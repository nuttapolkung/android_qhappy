package net.pirsquare.qhappy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

public class PromotionActivity extends ActionBarActivity {

	public PromotionFragment frag;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_promotion);
		
		final ActionBar actionBar = getSupportActionBar();
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("Promotion");
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		if (savedInstanceState == null) {
			 FragmentManager fm = getSupportFragmentManager();
			 FragmentTransaction fmt = fm.beginTransaction();
			 frag = new PromotionFragment();
			 fmt.add(R.id.container, frag);
			 fmt.commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

//		 Inflate the menu; 
//		 this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.promotion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			
			this.finish();
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
