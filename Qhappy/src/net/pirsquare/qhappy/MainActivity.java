package net.pirsquare.qhappy;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import net.pirsquare.qhappy.model.TwitterConstance;
import net.pirsquare.qhappy.service.QHappyService;
import net.pirsquare.qhappy.util.AlertDialogManager;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

class CreateUserCallBack extends Object {
	public void done() {

	}

	public void error() {

	}
}

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MainActivity extends ActionBarActivity {

	public ParseUser currentUser;
	private BluetoothAdapter mBluetoothAdapter;
	private ProgressDialog ringProgressDialog;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();

		QHApp app = (QHApp) getApplication();
		app.model.mainActivity = this;

		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("QHappy");

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		getWindow().setUiOptions(
				ActivityInfo.UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW);

		setContentView(R.layout.activity_main);

		// Check if twitter keys are set
		if (TwitterConstance.CONSUMER_KEY.trim().length() == 0
				|| TwitterConstance.CONSUMER_SECRET.trim().length() == 0) {
			// Check key for twitter
			alert.showAlertDialog(MainActivity.this, "Twitter oAuth tokens",
					"Please set your twitter oauth tokens first!", false);
			// stop executing code by return
			return;
		}

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		// Toast.makeText(this, "savedInstanceState:"+savedInstanceState,
		// Toast.LENGTH_SHORT).show();
		ParseInstallation.getCurrentInstallation().put("device_name",
				Build.MODEL);
		ParseInstallation.getCurrentInstallation().saveInBackground();

	}

	@Override
	protected void onDestroy() {
		Log.d("QHappy", "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
	}

	@Override
	protected void onStart() {
		super.onStart();

		SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
		final QHApp app = (QHApp) getApplication();
		app.model.mainActivity = this;

		Log.d("QHappy",
				"DeviceToken"
						+ ParseInstallation.getCurrentInstallation().getString(
								"deviceToken"));

		app.model.FBID = sharedPref.getString("fbid", "");
		app.model.login_name = sharedPref.getString("fb_name", "");
		app.model.islogin = !app.model.FBID.equals("");

		String queue_count = sharedPref.getString("haveQ", "");

		if (!queue_count.equals("") || app.model.islogin) {
			currentUser = ParseUser.getCurrentUser();

			if (currentUser != null) {
				currentUser.fetchInBackground(new GetCallback<ParseUser>() {
					@Override
					public void done(ParseUser arg0, ParseException arg1) {
						if (arg1 == null) {
							currentUser = arg0;
						} else {
							createUser(new CreateUserCallBack() {
								@Override
								public void done() {
									currentUser = ParseUser.getCurrentUser();
									super.done();
								}

								@Override
								public void error() {
									super.error();
								}
							});
						}
						Log.d("Login", "Fetch Complete");
					}
				});

				if (app.getInternetConnection()) {
					goHome();
				} else {
					Intent term_activity = new Intent(getBaseContext(),
							NotConnectInternetActivity.class);
					term_activity.putExtra("want_refresh", true);
					startActivity(term_activity);
				}
			} else {
				createUser(new CreateUserCallBack() {
					@Override
					public void done() {
						if (app.getInternetConnection()) {
							// Intent home_activity = new
							// Intent(getBaseContext(),
							// HomeActivity.class);
							//
							// startActivity(home_activity);
							goHome();
						} else {
							Intent term_activity = new Intent(getBaseContext(),
									NotConnectInternetActivity.class);
							term_activity.putExtra("want_refresh", true);
							startActivity(term_activity);
						}
						super.done();
					}

					@Override
					public void error() {
						super.error();
					}
				});
			}

			return;
		} else {
			checkTerm();
		}

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this);
		builder.setAutoCancel(true);

	}

	public void checkTerm() {

		// get local storage
		SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

		int readed_term = sharedPref.getInt("read_term", 0);

		if (readed_term == 0) {
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putInt("read_term", 1);
			editor.commit();

			Intent term_activity = new Intent(this, TermActivity.class);
			term_activity.putExtra("want_accept", true);

			startActivity(term_activity);
		}

		currentUser = ParseUser.getCurrentUser();

		if (currentUser == null) {
			createUser(new CreateUserCallBack() {
				@Override
				public void done() {
					super.done();
				}

				@Override
				public void error() {
					super.error();
				}
			});

		} else {
			currentUser.put("device_token", ParseInstallation
					.getCurrentInstallation().get("deviceToken"));
			currentUser.saveInBackground();
		}
	}

	public String getLocalBluetoothName() {
		if (mBluetoothAdapter == null) {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		}
		String name = mBluetoothAdapter.getName();
		if (name == null) {
			System.out.println("Name is null!");
			name = mBluetoothAdapter.getAddress();
		}
		return name;
	}

	public void createUser(CreateUserCallBack callBack) {

		final String device_token = ParseInstallation.getCurrentInstallation()
				.getString("deviceToken");

		final CreateUserCallBack _callBack = callBack;

		Log.d("Qhappy", "Device Token" + device_token);

		ParseUser.logInInBackground(device_token, "0000", new LogInCallback() {
			@Override
			public void done(ParseUser arg0, ParseException arg1) {
				if (arg1 == null) {
					currentUser = arg0;
					_callBack.done();
				} else {
					currentUser = new ParseUser();
					currentUser.setUsername(device_token);
					currentUser.setPassword("0000");
					currentUser.put("device_token", device_token);
					currentUser.put("title", "Guest");
					currentUser.put("device_name", getLocalBluetoothName());
					currentUser.signUpInBackground(new SignUpCallback() {
						public void done(ParseException e) {
							if (e == null) {
								_callBack.done();
							} else {
								_callBack.error();
							}
						}
					});
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	public void onTappedFBLogin(View view) {

		ringProgressDialog = ProgressDialog.show(this, "Please Wait..",
				"Loading...");

		final Activity mContext = this;

		createUser(new CreateUserCallBack() {
			@Override
			public void done() {

				if (currentUser != null)
					if (!ParseFacebookUtils.isLinked(currentUser)) {
						ParseFacebookUtils.link(currentUser, mContext,
								new SaveCallback() {
									@Override
									public void done(ParseException ex) {
										if (ex != null) {
											ParseUser.logOut();

											ParseFacebookUtils.logIn(mContext,
													new LogInCallback() {

														@Override
														public void done(
																ParseUser arg0,
																ParseException arg1) {
															if (arg1 == null) {
																try {
																	ParseFacebookUtils
																			.unlink(arg0);
																} catch (ParseException e) {
																	e.printStackTrace();
																}

																final String device_token = ParseInstallation
																		.getCurrentInstallation()
																		.getString(
																				"deviceToken");
																ParseUser
																		.logInInBackground(
																				device_token,
																				"0000",
																				new LogInCallback() {

																					@Override
																					public void done(
																							ParseUser arg0,
																							ParseException arg1) {
																						if (arg1 == null) {
																							ParseFacebookUtils
																									.link(arg0,
																											mContext,
																											new SaveCallback() {
																												@Override
																												public void done(
																														ParseException arg0) {
																													if (arg0 == null)
																														getFBID();
																													else {
																														callConnectionError();
																														ringProgressDialog
																																.dismiss();
																														ringProgressDialog = null;
																													}
																												}
																											});
																						} else {
																							callConnectionError();
																							ringProgressDialog
																									.dismiss();
																							ringProgressDialog = null;
																						}
																					}
																				});
															}

														}
													});

										} else
											getFBID();
									}
								});
					} else {
						Log.d("MyApp", "already linked with Facebook!");
						getFBID();
					}
				super.done();
			}

			@Override
			public void error() {
				super.error();
			}
		});
	}

	public void callConnectionError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Connection error try again later.");
		builder.create();
		builder.show();
	}

	public void getFBID() {
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						// If the response is successful
						if (ParseFacebookUtils.getSession() == Session
								.getActiveSession()) {
							if (user != null) {
								// Set the id for the ProfilePictureView
								// view that in turn displays the profile
								// picture.

								SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

								SharedPreferences.Editor editor = sharedPref
										.edit();
								editor.putString("fbid", user.getId());
								editor.putString("fb_name", user.getName());
								editor.commit();

								QHApp app = (QHApp) getApplication();
								app.model.FBID = sharedPref.getString("fbid",
										"");
								app.model.login_name = sharedPref.getString(
										"fb_name", "");

								ParseUser activeUser = ParseUser
										.getCurrentUser();
								activeUser.put("title", app.model.login_name);
								if (user.getProperty("email") != null)
									activeUser.put("conatct_email",
											user.getProperty("email"));
								if (user.getProperty("gender") != null)
									activeUser.put("gender",
											user.getProperty("gender"));
								if (user.getProperty("birthday") != null)
									activeUser.put("birthday",
											user.getProperty("birthday"));
								activeUser.saveInBackground();

								HashMap<String, Object> params = new HashMap<String, Object>();
								params.put("title", user.getName());
								params.put(
										"email",
										user.getProperty("email") != null ? user
												.getProperty("email") : "");
								params.put("facebook_id", user.getId());
								params.put(
										"birthday",
										user.getProperty("birthday") != null ? user
												.getProperty("birthday") : "");
								params.put(
										"gender",
										user.getProperty("gender") != null ? user
												.getProperty("gender") : "");
								params.put("device_token",
										ParseInstallation
												.getCurrentInstallation()
												.getString("deviceToken"));

								QHappyService.request("userUpdate", params,
										new TextHttpResponseHandler() {

											@Override
											public void onSuccess(int arg0,
													Header[] arg1, String arg2) {
												Log.d("QHappy",
														"update user complete.");
											}

											@Override
											public void onFailure(int arg0,
													Header[] arg1, String arg2,
													Throwable arg3) {
												Log.d("QHappy",
														"update user failed.");
											}
										});

							}

							goHome();
						} else if (response.getError() == null) {

							SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

							JSONObject json = null;
							try {
								json = new JSONObject(response.getRawResponse());
							} catch (JSONException e) {
								e.printStackTrace();
							}

							SharedPreferences.Editor editor = sharedPref.edit();

							try {
								editor.putString("fbid", json.getString("id"));
								editor.putString("fb_name",
										json.getString("name"));
							} catch (JSONException e) {
								e.printStackTrace();
							}

							editor.commit();

							QHApp app = (QHApp) getApplication();
							app.model.FBID = sharedPref.getString("fbid", "");
							app.model.login_name = sharedPref.getString(
									"fb_name", "");
							app.model.islogin = true;

							ParseUser activeUser = ParseUser.getCurrentUser();

							if (user != null)
								if (user.getProperty("email") != null)
									activeUser.put("contact_email",
											user.getProperty("email"));
							activeUser.put("title", app.model.login_name);
							if (user.getProperty("gender") != null)
								activeUser.put("gender",
										user.getProperty("gender"));
							if (user.getProperty("birthday") != null)
								activeUser.put("birthday",
										user.getProperty("birthday"));
							activeUser.saveInBackground();

							HashMap<String, Object> params = new HashMap<String, Object>();
							params.put("title", user.getName());
							params.put(
									"email",
									user.getProperty("email") != null ? user
											.getProperty("email") : "");
							params.put("facebook_id", user.getId());
							params.put(
									"birthday",
									user.getProperty("birthday") != null ? user
											.getProperty("birthday") : "");
							params.put(
									"gender",
									user.getProperty("gender") != null ? user
											.getProperty("gender") : "");
							params.put("device_token",
									ParseInstallation.getCurrentInstallation()
											.getString("deviceToken"));

							QHappyService.request("userUpdate", params,
									new TextHttpResponseHandler() {

										@Override
										public void onSuccess(int arg0,
												Header[] arg1, String arg2) {
											Log.d("QHappy",
													"update user complete.");
										}

										@Override
										public void onFailure(int arg0,
												Header[] arg1, String arg2,
												Throwable arg3) {
											Log.d("QHappy",
													"update user failed.");
										}
									});

							goHome();

						} else {

						}

						if (ringProgressDialog != null)
							ringProgressDialog.dismiss();
						ringProgressDialog = null;
					}
				});
		request.executeAsync();
	}

	public void goHome() {
		Intent home_intent = new Intent(this, HomeActivity.class);
		home_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(home_intent);

		this.finish();
	}

	public void onOnceTimeLogin(View view) {
		Log.d("MyApp", "once time login.!");

		createUser(new CreateUserCallBack() {
			@Override
			public void done() {
				if (currentUser != null) {
					currentUser.put("title", "Guest");
					currentUser.saveInBackground();
				}

				QHApp app = (QHApp) getApplication();
				SharedPreferences sharedPref = app.model.mainActivity
						.getPreferences(Context.MODE_PRIVATE);

				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString("fbid", "");
				editor.commit();

				app.model.islogin = false;

				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("title", "Guest");
				params.put("device_token", ParseInstallation
						.getCurrentInstallation().getString("deviceToken"));

				QHappyService.request("userUpdate", params,
						new TextHttpResponseHandler() {

							@Override
							public void onSuccess(int arg0, Header[] arg1,
									String arg2) {
								Log.d("QHappy", "update user complete.");
							}

							@Override
							public void onFailure(int arg0, Header[] arg1,
									String arg2, Throwable arg3) {
								Log.d("QHappy", "update user failed.");
							}
						});

				goHome();
				super.done();
			}

			@Override
			public void error() {
				super.error();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
