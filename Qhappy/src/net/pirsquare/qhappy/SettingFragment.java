package net.pirsquare.qhappy;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class SettingFragment extends Fragment {

	private QHApp app;
	private RelativeLayout view;
	private SharedPreferences sharedPref;
	private TextView login_name;
	private ToggleButton fb_toggle;
	private CircularImageView profile_image;
	
	private ParseUser current_user;
	private Boolean isRequestAutherizeFacebook  = false;
	private Handler timeoutAutherizerFacebook;
	

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		app = (QHApp) getActivity().getApplication();

		RelativeLayout rootView = (RelativeLayout)inflater.inflate(R.layout.fragment_setting, container,
				false);
		view =rootView;

		
		termHandle();

//		getActivity().getActionBar().setBackgroundDrawable(new ColorDrawable(0xffff0000));
		getActivity().getActionBar().setIcon(R.drawable.ic_tab_scan);
		getActivity().getActionBar().setTitle("QHappy");

		getActivity().setRequestedOrientation(
				ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		timeoutAutherizerFacebook = new Handler();

		return rootView;
	}
	
	public void onClickHelp(View v)
	{
		Intent intent   = new Intent(getActivity(),TutorialActivity.class);
		startActivity(intent);
	}

	@Override
	public void onStart() {

		checkSetting();
		super.onStart();
	}
	
	private ProgressDialog ringProgressDialog;

	public void checkSetting() {
		QHApp app = (QHApp) getActivity().getApplication();
		sharedPref = app.model.mainActivity
				.getPreferences(Context.MODE_PRIVATE);

		int set_notificat = sharedPref.getInt("set_notificat", 1);
		int set_sound = sharedPref.getInt("set_sound", 1);

		final ToggleButton notic_toggle = (ToggleButton) view
				.findViewById(R.id.notice_toggle);
		final ToggleButton sound_toggle = (ToggleButton) view
				.findViewById(R.id.sound_toggle);
		fb_toggle = (ToggleButton) view.findViewById(R.id.fbToggle);

		if (set_notificat == 0) {
			notic_toggle.setChecked(false);
		} else {
			notic_toggle.setChecked(true);
		}

		if (set_sound == 0) {
			sound_toggle.setChecked(false);
		} else {

			sound_toggle.setChecked(true);
		}

		notic_toggle.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (!notic_toggle.isChecked()) {
					notic_toggle.setChecked(true);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putInt("set_notificat", 1);
					editor.commit();
				}
			}
		});
		sound_toggle.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				SharedPreferences.Editor editor = sharedPref.edit();
				if (sound_toggle.isChecked()) {
					editor.putInt("set_sound", 1);
					editor.commit();
				} else {
					editor.putInt("set_sound", 0);
					editor.commit();
				}
			}
		});

		profile_image = (CircularImageView) view
				.findViewById(R.id.profile_image);
		
		
		// profile_image.addShadow();
		if (app.model.islogin)
			app.model.setProfileImage(profile_image);
		else
			profile_image.setImageDrawable(getResources().getDrawable(
					R.drawable.no_profile_man_medium));

		login_name = (TextView) view.findViewById(R.id.login_name);

		if (app.model.islogin) {

			login_name.setText(app.model.login_name);
			app.model.setProfileImage(profile_image);
			fb_toggle.setChecked(false);

		} else {
			fb_toggle.setChecked(true);
		}

		current_user =  ParseUser.getCurrentUser();
		
		fb_toggle.setChecked(app.model.islogin);

		fb_toggle.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				ringProgressDialog = ProgressDialog.show(getActivity(),"Please Wait..", "Loading...");
				
				if (fb_toggle.isChecked()) {
					
					isRequestAutherizeFacebook = true;
					timeoutAutherizerFacebook.postDelayed(timeOutAutherlize, 1000 * 120);
					
					ParseUser currentUser = ParseUser.getCurrentUser();
					
					if(currentUser!=null)
					if (ParseFacebookUtils.isLinked(currentUser) == false) {
						ParseFacebookUtils.link(currentUser, getActivity(),
								new SaveCallback() {
							
									@Override
									public void done(ParseException ex) {
										
										if(ex != null)
										if(ex.getCode() == 208)
										{
											ParseUser.logOut();
											ParseFacebookUtils.logIn(getActivity(), new LogInCallback() {
												
												@Override
												public void done(ParseUser arg0, ParseException arg1) {
													
													try {
														ParseFacebookUtils.unlink(arg0);
													} catch (ParseException e) {
														e.printStackTrace();
													}
													
													final String device_token = ParseInstallation
															.getCurrentInstallation().getString("deviceToken");
													
													Log.d("Qhappy","Device Token"+device_token);
													
													ParseUser.logInInBackground(device_token, "0000", new LogInCallback() {
														@Override
														public void done(ParseUser arg0, ParseException arg1) {
															if (!ParseFacebookUtils.isLinked(arg0)) {
															ParseFacebookUtils.link(arg0, getActivity(),new SaveCallback() {
																
																@Override
																public void done(ParseException arg0) {
																	getFBID();
																}
															});
															}
														}
													});
												}
											});
										}else
										getFBID();
									}
								});
						
					} else {
						Log.d("MyApp", "already linked with Facebook!");
						getFBID();
					}
				} else {

					QHApp app = (QHApp) getActivity().getApplication(); 
					SharedPreferences sharedPref = app.model.mainActivity
							.getPreferences(Context.MODE_PRIVATE);

					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putString("fbid", "");
					editor.commit();

					profile_image.setImageDrawable(getActivity().getResources()
							.getDrawable(R.drawable.no_profile_man_medium));
					login_name.setText("Guest");
					
					app.model.islogin = false;
					
					ParseUser user = ParseUser.getCurrentUser();
					user.put("title", "Guest");
					user.put("device_token", ParseInstallation.getCurrentInstallation().get("deviceToken"));
					user.saveInBackground();
					
					ringProgressDialog.dismiss();
					ringProgressDialog = null;
				}
			}
		});

		login_name.setTypeface(app.model.DB_OZONEX_BOLD);

		TextView textVersion = (TextView) view.findViewById(R.id.version_text);
		TextView text2 = (TextView) view.findViewById(R.id.textView2);
		TextView text3 = (TextView) view.findViewById(R.id.textView3);
		TextView text4 = (TextView) view.findViewById(R.id.textView4);
		TextView text5 = (TextView) view.findViewById(R.id.textView5);
		TextView text6 = (TextView) view.findViewById(R.id.textView6);
		
		text6.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				
				
				Intent intent   = new Intent(getActivity(),TutorialActivity.class);
				
				startActivity(intent);
				
				
			}
		});
		textVersion.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clearClickTimeHandle.removeCallbacks(clearClickRunable);
				clickVersionCount ++;
				if(clickVersionCount == 5)
				{
					clickVersionCount = 0;
					
					openDeveloperMode();
					
				}
				else
				clearClickTimeHandle.postDelayed(clearClickRunable, 1000);
			}
		});

		textVersion.setTypeface(app.model.DB_OZONEX_BOLD);
		text2.setTypeface(app.model.DB_OZONEX_BOLD);
		text3.setTypeface(app.model.DB_OZONEX_BOLD);
		text4.setTypeface(app.model.DB_OZONEX_BOLD);
		text5.setTypeface(app.model.DB_OZONEX_BOLD);
		text6.setTypeface(app.model.DB_OZONEX_BOLD);
	}
	
	private void openDeveloperMode()
	{
		Intent intent = new Intent(getActivity(),DeveloperModeActivity.class);
		startActivity(intent);
	}
	
	private Runnable clearClickRunable = new Runnable() {
		
		@Override
		public void run() {
			clickVersionCount = 0;
			
			Intent intent  = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("http://pirsquare.net/"));
			
			getActivity().startActivity(intent);
		}
	};
	
	private Handler clearClickTimeHandle = new Handler();
	private int clickVersionCount = 0;
	
	

	public Runnable timeOutAutherlize = new Runnable() {
		
		@Override
		public void run() {
			
			if(ringProgressDialog!=null)
			ringProgressDialog.dismiss();
			ringProgressDialog = null;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//			builder.setCancelable(false);
			builder.setTitle("QHappy");
			builder.setMessage("Connection error please try again later.");
			builder.setCancelable(true);
			builder.show();
			
		}
	};
	
	
	public void getFBID() {
		
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						// If the response is successful
						if (ParseFacebookUtils.getSession() == Session.getActiveSession()) {
							if (user != null) {

								QHApp app = (QHApp) getActivity()
										.getApplication();
								SharedPreferences sharedPref = app.model.mainActivity
										.getPreferences(Context.MODE_PRIVATE);

								SharedPreferences.Editor editor = sharedPref
										.edit();
								editor.putString("fbid", user.getId());
								editor.putString("fb_name", user.getName());
								editor.commit();
								
								app.model.islogin = true;
								
								if(user.getBirthday()!=null)
								app.model.birthday = user.getBirthday();
								
								if(user.getProperty("gender")!=null)
								app.model.gender = (String)user.getProperty("gender");

								app.model.FBID = sharedPref.getString("fbid",
										"");
								app.model.login_name = sharedPref.getString(
										"fb_name", "");

								login_name.setText(app.model.login_name);
								
								app.model.setProfileImage(profile_image);
								
								Log.d("login name", (String)login_name.getText());
								
								updateTitleInClond();
								

							}else{
								Log.d("QHAppy", "no user");
							}
						} else if (response.getError() == null) {

							QHApp app = (QHApp) getActivity().getApplication();
							SharedPreferences sharedPref = app.model.mainActivity
									.getPreferences(Context.MODE_PRIVATE);

							SharedPreferences.Editor editor = sharedPref.edit();
							editor.putString("fbid", (String) response
									.getGraphObject().getProperty("id"));
							editor.putString("fb_name", (String) response
									.getGraphObject().getProperty("name"));
							editor.commit();

							app.model.FBID = sharedPref.getString("fbid", "");
							app.model.login_name = sharedPref.getString(
									"fb_name", "");
							
							if(response.getGraphObject().getProperty("gender")!=null)
							app.model.gender = (String)response.getGraphObject().getProperty("gender");
							if(response.getGraphObject().getProperty("birthday")!=null)
							app.model.birthday = (String)response.getGraphObject().getProperty("birthday");
							
							login_name.setText(app.model.login_name);
							
							app.model.setProfileImage(profile_image);
							updateTitleInClond();
							
						}
						
//						remove timeout call back.
						timeoutAutherizerFacebook.removeCallbacks(timeOutAutherlize);
						
						app.model.islogin = true;
						
						if(ringProgressDialog!= null)
						ringProgressDialog.dismiss();
						ringProgressDialog = null;
					}
				});
		request.executeAsync();

	}
	
	public void updateTitleInClond()
	{
		final ParseUser saveUser =current_user;
		
		Log.d("login name", saveUser.getUsername());
		Log.d("device token", saveUser.getString("device_token"));
		String title =  (String)login_name.getText();
		saveUser.put("title",title);
		saveUser.put("gender", app.model.gender);
		saveUser.put("birthday", app.model.birthday);
		saveUser.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException arg0) {
				Log.d("QHappy", "Save Done. " );
				
//				ParseUser u = ParseUser.getCurrentUser();
//				u.put("title",(String)login_name.getText());
//				u.saveInBackground();
			}
		});
	}

	private void termHandle() {
		ImageButton term_button = (ImageButton) view
				.findViewById(R.id.term_button);

		term_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent term_activity = new Intent(getActivity(),
						TermActivity.class);

				startActivity(term_activity);
			}
		});
	}
}
