package net.pirsquare.qhappy.service;

import java.util.List;

import net.pirsquare.qhappy.model.QueueData;

import com.parse.ParseException;
import com.parse.ParseObject;

public interface ServiceCallBack {
		public void callBackGetBrandMetaData(List<ParseObject> result);
		public void callBackGetBranchMetaData(List<ParseObject> result);
		public void Error(ParseException e);
		public void callBackGetMyQData(List<ParseObject> result);
		public void callBackGetMyQDataQueue(List<QueueData> result);
		public void callBackGetFavData(List<ParseObject> result);
}
