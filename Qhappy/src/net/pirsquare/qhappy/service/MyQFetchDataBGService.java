package net.pirsquare.qhappy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.pirsquare.qhappy.Config;
import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.receiver.ResponseReceiver;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

public class MyQFetchDataBGService extends IntentService {

	public MyQFetchDataBGService() {
		super("update queue service");
	}

	public final String TAG = "MyQFetchDataBGService";

	private String device_token = "";

	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			device_token = intent.getDataString();
		} catch (Exception e) {
		}

		Log.d(TAG, "device token:" + device_token);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (intent != null) {
			try {
				device_token = intent.getDataString();
			} catch (Error e) {
				try {
					if (ParseInstallation.getCurrentInstallation() != null)
						device_token = ParseInstallation
								.getCurrentInstallation().getString(
										"device_token");
				} catch (Exception e2) {
					device_token = "";
				}

			}
			;

			if (device_token != "")
				fetchMyQ();
		}

		return START_STICKY;
	}

	private void fetchMyQ() {
		Log.d("QHappy", "fetch data");

		try {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("device_token", device_token);
		// params.put("queues", queue_data);
		Log.d("QHappy", "getMyQ paarams" + params.toString());

		if (Config.useParseServer == false)
			QHappyService.request(QHappyService.REQUEST_ACTION_GET_MYQ, params,
					new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {
							
							Log.d("QHappy", "get myq result >>" + arg2);
							try {
								JSONObject j = null;
								try {
									j = new JSONObject(arg2);
								} catch (JSONException e2) {
									e2.printStackTrace();
								}

								JSONArray response = null;
								if(j!=null)
								try {
									response = j.getJSONArray("result");
								} catch (JSONException e1) {
									e1.printStackTrace();
								}
								

								JSONArray json_list = new JSONArray();
								ArrayList<QueueData> myq_list = new ArrayList<QueueData>();

								if (response != null)
								{
									for (int i = 0; i < response.length(); i++) {

										QueueData q = new QueueData(null);
										q.setJSONObject(response
												.getJSONObject(i));
										myq_list.add(q);
										json_list.put(q.getJSONObject());
									}
									
//									Recheck
									if(response.length()==0)
									{
										Log.d("Qhappy", "is empty my queue.");
										Intent stop_intent = new Intent("net.pirsquare.stop_service");
										stop_intent.addCategory("net.pirsquare.qhappy");
										sendBroadcast(stop_intent);
									}
								}


								if (QHApp.instance != null) {
									try {
										QHApp.instance.model.myq_data = myq_list;
									} catch (Error error) {
									}
									
									String json_string = json_list.toString();
									Intent intent = new Intent(
											ResponseReceiver.ACTION_RESP);
									intent.putExtra("json", json_string);
									sendBroadcast(intent);

									Intent finish = new Intent(
											ResponseReceiver.ACTION_FINISH);
									sendBroadcast(finish);
								}
							} catch (Exception e2) {
								Log.d("QHappy",
										"Error when get myq in background.");
							}
						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {
							Log.d(TAG, "Fetch error");
							
							Intent finish = new Intent(
									ResponseReceiver.ACTION_FINISH);
							sendBroadcast(finish);
						}
					});
		else
			ParseCloud.callFunctionInBackground("getMyQ", params,
					new FunctionCallback<List<ParseObject>>() {
						public void done(List<ParseObject> result,
								ParseException e) {

							if (e == null) {

								try {
									JSONArray json_list = new JSONArray();
									ArrayList<QueueData> myq_list = new ArrayList<QueueData>();
									for (int i = 0; i < result.size(); i++) {

										QueueData q = new QueueData(result
												.get(i));
										myq_list.add(q);
										json_list.put(q.getJSONObject());
									}

									if (QHApp.instance != null) {
										try {
											QHApp.instance.model.myq_data = myq_list;
										} catch (Error error) {
										}
									}

									if (QHApp.instance != null) {
										String json_string = json_list
												.toString();
										Intent intent = new Intent(
												ResponseReceiver.ACTION_RESP);
										intent.putExtra("json", json_string);
										sendBroadcast(intent);

										Intent finish = new Intent(
												ResponseReceiver.ACTION_FINISH);
										sendBroadcast(finish);
									}
								} catch (Exception e2) {
									Log.d("QHappy",
											"Error when get myq in background.");
								}

								Log.d(TAG, "Size count " + result.size());
							} else {
								Log.d(TAG, "Fetch error");
							}
						}
					});
		} catch (Exception e) {
			Log.d("QHappy", "can't get my q");
		}
	}

}
