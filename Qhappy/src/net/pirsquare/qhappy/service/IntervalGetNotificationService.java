package net.pirsquare.qhappy.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.pirsquare.qhappy.receiver.ResponseReceiver;

import org.apache.http.Header;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.ParseInstallation;

public class IntervalGetNotificationService extends IntentService {

	public IntervalGetNotificationService() {
		super("interval get nootification service");
	}

	private String device_token = "";

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (intent != null) {
			try {
				device_token = intent.getDataString();
			} catch (Error e) {
				try {
					Log.d("QHappy", "In case no device token from intent.");
					if (ParseInstallation.getCurrentInstallation() != null)
						device_token = ParseInstallation
								.getCurrentInstallation().getString(
										"device_token");
				} catch (Error e2) {
					device_token = "";
				}

			}
			;

			if (device_token != "")
				fetchNotification();
		}

		return START_STICKY;
	}

	private String readTimeFromFile() {

		String ret = "0";

		try {
			InputStream inputStream = openFileInput("lasttime.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("login activity", "File not found: " + e.toString());

		} catch (IOException e) {
			Log.e("login activity", "Can not read file: " + e.toString());

		}

		return ret;
	}

	private String readFromFile() {

		String ret = "0";

		try {
			InputStream inputStream = openFileInput("lastid.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("login activity", "File not found: " + e.toString());

		} catch (IOException e) {
			Log.e("login activity", "Can not read file: " + e.toString());

		}

		return ret;
	}

	public void fetchNotification() {
		SharedPreferences s = getSharedPreferences("connection_failed",
				Context.MODE_PRIVATE);
		final Boolean connection_failed = s.getBoolean("value", true);
		final Editor ed = s.edit();

		int last_id = Integer.parseInt(readFromFile());
		String time = readTimeFromFile();
		try {
			QHappyService.intervalGetNotification(device_token, last_id, time,
					new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {
							Intent intent = new Intent(
									ResponseReceiver.ACTION_INTERVAL_NOTIFICATION);
							intent.putExtra("json", arg2);
							Log.d("QHappy", "intervalNotification result data "
									+ arg2);
							sendBroadcast(intent);

							ed.putBoolean("value", true);
							ed.commit();

							Intent success_intent = new Intent(
									ResponseReceiver.ACTION_CONNECTION_RESPONSE);
							success_intent.putExtra("connection", true);
							success_intent.addCategory("net.pirsquare.qhappy");
							sendBroadcast(success_intent);
						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {

							Log.d("QHappy",
									"intervalNotification result data failed  "
											+ arg2);

							if (connection_failed == false) {
								Log.d("QHappy", "connection after is failed");
								Intent success_intent = new Intent(
										ResponseReceiver.ACTION_CONNECTION_RESPONSE);
								success_intent.putExtra("connection", false);
								success_intent
										.addCategory("net.pirsquare.qhappy");

								sendBroadcast(success_intent);
							} else {
								Log.d("QHappy", "connection after is normal");
								ed.putBoolean("value", false);
								ed.commit();
							}
						}
					});
		} catch (Exception e) {
			Log.d("QHappy", "can't fetch notification.");
		}

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		device_token = intent.getDataString();
		Log.d("QHappy", "device token:" + device_token);
	}
}
