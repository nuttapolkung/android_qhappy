package net.pirsquare.qhappy.service;

import java.util.HashMap;
import java.util.Map;

import net.pirsquare.qhappy.Config;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class QHappyService {

	public static String REQUEST_ACTION_RESERVE = "reserveQueue";
	public static String REQUEST_ACTION_CANCEL = "cancelQueue";
	public static String REQUEST_ACTION_RESERV_TIMEOUT = "reserve_timeout";
	public static String REQUEST_ACTION_ACCEPT_QUEUE = "acceptQueue";
	public static String REQUEST_ACTION_GET_NOTIFICATION_WITH_ID = "getNotificationdataWithID";
	public static String REQUEST_ACTION_GET_MYQ = "getMyQ";
	public static String REQUEST_ACTION_CHECK_RESERVE_RESULT = "checkReserveResultAndroid";
	public static String REQUEST_ACTION_CHECK_KIOS_WITH_ID = "checkKiosStatusWithBranchIDs";
	public static String REQUEST_ACTION_SET_FAV = "setFav";
	public static String REQUEST_ACTION_UN_SET_FAV = "unSetFav";
	public static String REQUEST_ACTION_GET_BRANCH_WITH_ID = "getBranchWithID";
	public static String REQUEST_ACTION_GET_MYQ_REMAIN = "getMyQRemain";
	public static String REQUEST_ACTION_FAVORITE_EXISTS = "favoriteExists";
	public static String REQUEST_ACTION_REGISTER = "register";

	public static String REQUEST_DONE = "done";
	public static String REQUEST_FAILED = "failed";

	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void post(String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void intervalGetNotification(String device_token,
			int last_id, String time, AsyncHttpResponseHandler responseHandler) {
		RequestParams request_params = new RequestParams();
		request_params.put("device_token", device_token);
		request_params.put("last_id", last_id);
		request_params.put("time", time);
		request_params.put("action", "notice_mobile");

		Log.d("Qhappy",
				"intervalGetNotification params >>" + request_params.toString());

		client.post(Config.request_base_url, request_params, responseHandler);
	}

	// Request API function
	public static void request(String action, HashMap<String, Object> params,
			AsyncHttpResponseHandler responseHandler) {
		params.put("action", action);

		RequestParams request_params = new RequestParams();
		for (Map.Entry<String, Object> e : params.entrySet()) {
			request_params.put(e.getKey(), e.getValue());
		}

		Log.d("QHappy", "request params " + params.toString());

		client.post(Config.request_base_url, request_params, responseHandler);
	}

	// Combine url
	private static String getAbsoluteUrl(String relativeUrl) {
		return Config.request_base_url + relativeUrl;
	}
}
