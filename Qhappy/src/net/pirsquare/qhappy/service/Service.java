package net.pirsquare.qhappy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.pirsquare.qhappy.Config;
import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.model.QueueData;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;


public class Service {

	private ArrayList<ServiceCallBack> listeners = new ArrayList<ServiceCallBack>();
	
	public void getBranchMetaData(String bandID,String lat ,String lng)
	{
		ParseQuery<ParseObject> branch_query  = ParseQuery.getQuery("t_brand_metadata");
		branch_query.whereEqualTo("objectId", bandID);
		
		ParseGeoPoint userLocation =new ParseGeoPoint(Double.parseDouble(lat),Double.parseDouble(lng));
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("t_branch_metadata");
		
		query.whereMatchesQuery("brand", branch_query);
		
//		query.fromLocalDatastore();
		if(!(lat.equals("0.0") && lng.equals("0.0")))
			query.whereNear("location", userLocation);

		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> arg0, ParseException e) {
				if(e == null)
				{
					for (ServiceCallBack listen: listeners) {
			    		
			    		if(listen != null){
			    			listen.callBackGetBranchMetaData(arg0);
			    		}
					}
				}else{
					for (ServiceCallBack listen: listeners) {
			    		listen.Error(e);
					}
				}
			}
		});
	}
	
	public void addListener(ServiceCallBack callBack)
	{
		listeners.add(callBack);
	}
	
	public void removeListener(ServiceCallBack callBack)
	{
		listeners.remove(callBack);
	}
	
	public void getBrandMetaData()
	{
		ParseCloud.callFunctionInBackground("getBrandList", new HashMap<String, Object>(), new FunctionCallback<List<ParseObject>>() {
			  public void done(List<ParseObject>  result, ParseException e) {
			    if (e == null) {
			    	for (ServiceCallBack listen: listeners) {
			    		if(listen != null){
			    			listen.callBackGetBrandMetaData(result);
			    		}
					}
			    }else{
			    	for (ServiceCallBack listen: listeners) {
			    		listen.Error(e);
					}
			    }
			  }
			});
	}
	
	private Boolean isLoadMyQ = false;
	public void getMyQ()
	{
		if(isLoadMyQ)
			return;
		
		isLoadMyQ = true;
		
		String device_token = ParseInstallation
				.getCurrentInstallation().getString("deviceToken");
		HashMap<String, Object> params =  new HashMap<String, Object>();
		params.put("device_token", device_token);
		
		if(Config.useParseServer==false){
			
			QHappyService.request(QHappyService.REQUEST_ACTION_GET_MYQ, params, new TextHttpResponseHandler() {
				
				@Override
				public void onSuccess(int arg0, Header[] arg1, String arg2) {
					
					JSONObject j = null;
					try {
						j = new JSONObject(arg2);
					} catch (JSONException e2) {
						e2.printStackTrace();
					}
					
					JSONArray response = null;
					if(j!=null)
					try {
						response =j.getJSONArray("result");
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					
					List<QueueData> queues = new ArrayList<QueueData>();
					
					if(response!=null)
					for (int i = 0; i < response.length(); i++) {
						QueueData q = new QueueData(null);
						try {
							q.setJSONObject(response.getJSONObject(i));
						} catch (JSONException e) {
							Log.d("QHappy","error get json object.");
							e.printStackTrace();
						}
						queues.add(q);
					}
					    	for (ServiceCallBack listen: listeners) {
					    		if(listen != null){
					    			listen.callBackGetMyQDataQueue(queues);
					    		}
							}
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
					
					for (ServiceCallBack listen: listeners) {
			    		if(listen != null){
			    			listen.callBackGetMyQDataQueue(null);
			    		}
					}
				}
				
				@Override
				public void onFinish() {
					 isLoadMyQ = false;
					super.onFinish();
				}
			});
			
				
				
		}
		else{
		ParseCloud.callFunctionInBackground("getMyQ",params, new FunctionCallback<List<ParseObject>>() {
			  public void done(List<ParseObject>  result, ParseException e) {
			    if (e == null) {
			    	for (ServiceCallBack listen: listeners) {
			    		if(listen != null){
			    			listen.callBackGetMyQData(result);
			    		}
					}
			    }else{
			    	for (ServiceCallBack listen: listeners) {
			    		listen.Error(e);
					}
			    }
			    
			    isLoadMyQ = false;
			  }
			});
		}
	}
	
	private Boolean isLoadFav = false;
	public void getFav()
	{
		if(isLoadFav)
			return;
		
		isLoadFav = true;
		
		String device_token = ParseInstallation
				.getCurrentInstallation().getString("deviceToken");
		HashMap<String, Object> params =  new HashMap<String, Object>();
		params.put("device_token", device_token);
		
		ParseCloud.callFunctionInBackground("getFav",params, new FunctionCallback<List<ParseObject>>() {
			  public void done(List<ParseObject>  result, ParseException e) {
			    if (e == null) {
			    	for (ServiceCallBack listen: listeners) {
			    		if(listen != null){
			    			listen.callBackGetFavData(result);
			    		}
					}
			    }else{
			    	for (ServiceCallBack listen: listeners) {
			    		listen.Error(e);
					}
			    }
			    
			    isLoadFav = false;
			  }
			});
	}

	public void cancelCurrentQueue() {
		
		
		
	}	
}
