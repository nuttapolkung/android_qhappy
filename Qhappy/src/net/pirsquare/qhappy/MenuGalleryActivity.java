package net.pirsquare.qhappy;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.pirsquare.qhappy.dataAdepter.MenusFragmentAdapter;
import ru.truba.touchgallery.GalleryWidget.BasePagerAdapter.OnItemChangeListener;
import ru.truba.touchgallery.GalleryWidget.GalleryViewPager;
import ru.truba.touchgallery.GalleryWidget.UrlPagerAdapter;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.viewpagerindicator.PageIndicator;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MenuGalleryActivity extends FragmentActivity {

	MenusFragmentAdapter mAdapter;
	GalleryViewPager mPager;
    PageIndicator mIndicator;
    QHApp app;
    private List<Fragment> mListFragment;
    
    public static final String TAG = "detailsFragment";
	
    Boolean isShowActionBar = true;
    
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fragement_menu);
		//initialsie the pager
		
		final ActionBar actionBar = getActionBar();
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("QHappy");
		actionBar.hide();
         app = (QHApp) getApplication();
         app.model.menuGalleryActivity = this;
         
         List<ParseObject>  result = app.model.currentMenusParseObject;
         
         List<ParseFile> mListItems = new Vector<ParseFile>();
         mListFragment = new ArrayList<Fragment>();
         
//         
//         String[] urls = {
//                 "http://cs407831.userapi.com/v407831207/18f6/jBaVZFDhXRA.jpg",
//                 "http://cs407831.userapi.com/v4078f31207/18fe/4Tz8av5Hlvo.jpg",
//                 "http://cs407831.userapi.com/v407831207/1906/oxoP6URjFtA.jpg",
//                 "http://cs407831.userapi.com/v407831207/190e/2Sz9A774hUc.jpg",
//                 "http://cs407831.userapi.com/v407831207/1916/Ua52RjnKqjk.jpg",
//                 "http://cs407831.userapi.com/v407831207/191e/QEQE83Ok0lQ.jpg"
//         };
         
         List<String> items = new ArrayList<String>();
         
         int i=0;
         for (ParseObject parseObject : result) {
 			// Locate images in brand_image column
 			ParseFile image = (ParseFile) parseObject.get("image");
 			
 			items.add(image.getUrl());
 			i++;
 		}
         
        
//         Collections.addAll(items, urls);

         UrlPagerAdapter pagerAdapter = new UrlPagerAdapter(this, items);
         pagerAdapter.setOnItemChangeListener(new OnItemChangeListener()
 		{
 			@Override
 			public void onItemChange(int currentPosition)
 			{
 			}
 		});
         
         mPager = (GalleryViewPager)findViewById(R.id.gallerypager);
         mPager.setOffscreenPageLimit(3);
         mPager.setAdapter(pagerAdapter);
         
         mPager.getRootView().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(isShowActionBar)
				{
					getActionBar().hide();
					isShowActionBar = false;
				}else{
					getActionBar().show();
					isShowActionBar = true;
				}
			}
		});
         
         
//         
         
         
//         int i=0;
//         for (ParseObject parseObject : result) {
// 			// Locate images in brand_image column
// 			ParseFile image = (ParseFile) parseObject.get("image");
// 			
// 			mListItems.add(image);
// 			
// 			MenusFragment frag = new MenusFragment().setCurrentItem(i);
// 			mListFragment.add(frag);
// 			i++;
// 		}
//         
//         app.model.menu_image_file = new Vector<ParseFile>();
//         app.model.menu_image_file.addAll(mListItems);
//         
//         mAdapter = new MenusFragmentAdapter(getSupportFragmentManager(),MenuGalleryActivity.this,mListFragment);
//         
//         mPager = new ViewPager(MenuGalleryActivity.this);
//         mPager = (ViewPager) findViewById(R.id.gallerypager);
//         mPager.setAdapter(mAdapter);
//         mPager.setCurrentItem(0);
         
//         mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
//         mIndicator.setViewPager(mPager);
//         ((CirclePageIndicator) mIndicator).setSnap(true);
         
//         mPager.setOnPageChangeListener(new OnPageChangeListener() {
//
// 			@Override
// 			public void onPageSelected(int arg0) {
//// 				Toast.makeText(MenuGalleryActivity.this,
//// 						"Page Selected " + arg0, Toast.LENGTH_LONG).show();
// 			}
//
// 			@Override
// 			public void onPageScrolled(int arg0, float arg1, int arg2) {
// 			}
//
// 			@Override
// 			public void onPageScrollStateChanged(int arg0) {
// 			}
// 		});
	}
	
	
	
	
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override
	protected void onDestroy() {
		
		
		
		super.onDestroy();
	}



	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event)
	 {
		 if (keyCode == KeyEvent.KEYCODE_BACK)
		 {
//			 getSupportFragmentManager().beginTransaction().remove(frag).commit();
			 this.finish();
		 }

		 return super.onKeyDown(keyCode, event);
	 }

}
