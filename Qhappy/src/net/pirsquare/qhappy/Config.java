package net.pirsquare.qhappy;

import org.json.JSONObject;

public class Config {
	public static String held_message_accept = "ถึงคิวแล้วเราจะเก็บคิวไว้ 10 นาทีค่ะ";
	public static String held_message_reject = "คุณยกเลิกคิวไปแล้ว";
	public static String held_message_accept_timeout = "ขออนุญาตยกเลิกคิว เนื่องจากเกินเวลาที่กำหนด";
	public static String door_host_delete_message = "คิวถูกยกเลิก กรุณาติดต่อหน้าร้าน";
	public static String held_message_keep_queue = "ทางร้านจะรักษาคิวให้ท่าน 10 นาที";
	public static String held_message_accept_timeout_in_row = "เลยเวลาที่รักษาคิวไว้แล้ว";
	public static Integer held_timeout = 10;
	public static String introduce_allow_notification_message = "กรุณาเปิดการแจ้งเตือนในเมนู Settings -> Notification Center -> QHappy ->เลือกรูปแบบการแจ้งเตือนเป็น Alerts";

	public static String cencel_error_message = "การเชื่อมต่อผิดพลาด ไม่สามารถยกเลิดคิวได้ กรุณาลองใหม่ ภายหลัง";
	public static String kios_offline_message = "The kios is offline.";
	public static Integer request_reserve_timeout = 30;
	public static String app_version = "1.6.2";

	public static Integer claim_time_expired = 30;

	public static String QH_STAGE_ORDER = "order";
	public static String QH_STAGE_COMPLAIN = "complain";
	public static String QH_STAGE_DELIVERY = "delivery";
	// force update message
	public static String FORCE_UPDATE_MESSGAE = "The new version is available. Please update for the fully functional.";
	// SOL addition.
	public static String counter_number_message = "เชิญที่เคาน์เตอร์ ";
	public static String currect_stage_complain_messgae = "";
	public static String currect_stage_order_messgae = "กำลังรอสั่งของ";
	public static String currect_stage_derivery_messgae = "กำลังรอรับของ";
	// Distance near me shop.
	public static Double near_me_distance_meter = 3.0;

	public static String request_base_url = "http://www.q-happy.com/service/api";
	public static String request_refresh_url = "http://www.pirsquare.net/project/qhappy/service/notice/mobile";
	// TAG
	public static String TAG = "QHAPPY";
	//
	public static Boolean useParseServer = false;

	public static JSONObject current_stage_status_value;

}
