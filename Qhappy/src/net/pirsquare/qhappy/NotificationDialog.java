package net.pirsquare.qhappy;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class NotificationDialog extends Activity {

	@Override
	public void onAttachedToWindow() {
		 Window window = getWindow();

	        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
	                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
	                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
	                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
	        
		super.onAttachedToWindow();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(
                 WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                 WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		
		
		setContentView(R.layout.activity_notification_dialog);
		
		
	

				String mx = 	getIntent().getStringExtra("ms");
				
				TextView tw =(TextView)findViewById(R.id.textView1);
				
				tw.setText(mx);
				
				final Activity ctx = this;
				
				Button bt = (Button)findViewById(R.id.button1);
				bt.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(ctx,SplashScreen.class);
						ctx.startActivity(intent);
					}
				});
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }
}
