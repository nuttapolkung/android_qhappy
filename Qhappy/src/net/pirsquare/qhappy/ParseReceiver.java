package net.pirsquare.qhappy;

import net.pirsquare.qhappy.receiver.ResponseReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

public class ParseReceiver extends ParsePushBroadcastReceiver {

	@Override
	protected void onPushDismiss(Context context, Intent intent) {
		super.onPushDismiss(context, intent);
	}
	
	@Override
	protected void onPushReceive(Context arg0, Intent arg1) {
		super.onPushReceive(arg0, arg1);
	}
	
	@Override
	protected void onPushOpen(Context arg0, Intent arg1) {
		  Intent newIntent = arg0.getPackageManager().getLaunchIntentForPackage(arg0.getPackageName());
	        newIntent.putExtras(arg1.getExtras());
	        arg0.startActivity(newIntent);
	        
//		super.onPushOpen(arg0, arg1);
		
		
	}
	
	@Override
	protected Class<? extends Activity> getActivity(Context arg0, Intent arg1) {
		return MainActivity.class;
	}
	
	private static final String TAG = "Receiver";

	  @Override
	  public void onReceive(Context context, Intent intent) {
		  
		  super.onReceive(context, intent);
		  
//		  NotificationManager mNotificationManager = (NotificationManager) context
//					.getSystemService(Context.NOTIFICATION_SERVICE);
//		  mNotificationManager.cancel(1009);
////	      TODO Remove.
//	      return;
		  
		
		  
		  NotificationManager mNotificationManagerCancel = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		  mNotificationManagerCancel.cancelAll();
		  

		 String json_string = "";
		  
	      String action = intent.getAction();
	      String channel = intent.getExtras().getString("com.parse.Channel");
	      
	      json_string = intent.getExtras().getString("com.parse.Data");
	      
	     
	      
	      Log.d("QHappy", "Parse push "+ json_string);
	      
	      Intent brodcast_intent = new Intent(ResponseReceiver.ACTION_INTERVAL_NOTIFICATION);
	      brodcast_intent.putExtra("json", "["+json_string+"]");
		    Log.d("QHappy", "parse intervalNotification result data "+json_string);
		    context.sendBroadcast(brodcast_intent);
	      
	      if(true)
			  return;
	      
	      QHApp app = (QHApp)context.getApplicationContext();
	      
	      
	      if(app!=null)
	    	  Log.d("QHappy", "Have app context");
	      else
	    	  Log.d("QHappy", "No app context");
	     
			
	     JSONObject json = null;
		try {
			json  = new JSONObject(json_string);
			
			 SharedPreferences share = app.getSharedPreferences("noti_time",
						Context.MODE_PRIVATE);
				long old_time = share.getLong("noti_time", 0);
				
					long  noti_time = json.getLong("time");
		
					if (noti_time <= old_time) {
						Log.d("QHappy", "older notification.");
						return;
					}else{
						Editor editor = share.edit();
						editor.putLong("noti_time", noti_time);
						editor.commit();
					}
				
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	      String ms = "";
		try {
			ms = json.getString("alert");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	    Log.d(TAG, "got action " + action + " on channel " + channel + " with:");
	    
	    // Send the name of the connected device back to the UI Activity
      Message msg = app.mHandler.obtainMessage(0);
      Bundle bundle = new Bundle();
      bundle.putString("json", json_string);
      msg.setData(bundle);
      
      app.mHandler.sendMessage(msg);
      
      PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
      
      NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
      
      KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
      
     
     if(!pm.isScreenOn() || myKM.inKeyguardRestrictedInputMode())
     {
  	   PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "QHappy");
  	   wl.acquire();
  	   wl.release();
  	 
     }else{
  	   
//  	   mNotificationManager.cancel(1009);
     }
     
   
     
     try {
		
	
//   create voice 
   Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
   if(notification != null)
   {
	   MediaPlayer mp = MediaPlayer.create( context.getApplicationContext(), notification);
	   if(mp != null)
		   mp.start();
   }
     } catch (Exception e) {
 		Log.d("QHappy",e.toString());
 	}
  
//   create vibrate.
   Vibrator v = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
   if(v!=null)
	   v.vibrate(500);
//
     
	  }
}
