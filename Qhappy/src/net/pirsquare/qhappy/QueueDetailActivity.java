package net.pirsquare.qhappy;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.pirsquare.qhappy.model.BranchListData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.model.TwitterConstance;
import net.pirsquare.qhappy.receiver.ResponseReceiver;
import net.pirsquare.qhappy.service.QHappyService;
import net.pirsquare.qhappy.util.AlertDialogManager;
import net.pirsquare.qhappy.util.TwitterApp;
import net.pirsquare.qhappy.util.TwitterApp.TwDialogListener;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class QueueDetailActivity extends ActionBarActivity implements
		AppListener {

	private ImageView nextQueueImageView;
	private TextView unit_text;
	private TextView startDateText;
	private TextView brand_name;
	private TextView branch_name;
	private TextView branch_info;
	private TextView menu_title;

	private TextView queue_group;
	private TextView queue_reserve;
	private TextView queue_remain;

	private ParseImageView brand_image;
	private QueueData queueData;

	private ImageButton fav_btn;
	private String band_object_id;

	private Boolean isFav;

	private ProgressDialog ringProgressDialog;
	QHApp app;

	// Twitter
	private TwitterApp mTwitter;
	private ProgressDialog mProgressDlg;
	BranchListData branch;
	private ProgressBar progress_bar;

	private ParseImageView share_image;

	AlertDialogManager alert = new AlertDialogManager();

	// Login button
	ImageButton btnLoginTwitter;
	String msg;

	private Activity ctx;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_queue_detail);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (mNotificationManager != null)
			mNotificationManager.cancelAll();

		ActionBar actionBar = getActionBar();
		actionBar.setTitle("รายละเอียดคิว");
		// actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		QHApp app = (QHApp) getApplication();
		app.addListener(this);
		app.current_activity = this;

		ctx = this;

		Intent intent = getIntent();
		msg = intent.getStringExtra("msg");

		mTwitter = new TwitterApp(this, TwitterConstance.CONSUMER_KEY,
				TwitterConstance.CONSUMER_SECRET);

		mTwitter.setListener(mTwLoginDialogListener);

		mProgressDlg = new ProgressDialog(QueueDetailActivity.this);
		mProgressDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// add filter for receive data in babckground.

		receiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {
						refreshData();
					}
				});

	}

	// notify when operation in Service finished
	private ResponseReceiver receiver;

	@Override
	protected void onDestroy() {

		if (dialog != null) {
			try {
				dialog.cancel();
				dialog.dismiss();
			} catch (Exception error) {

			}
			dialog = null;
		}

		if (ringProgressDialog != null) {
			ringProgressDialog.cancel();
			ringProgressDialog.dismiss();
			ringProgressDialog = null;
		}
		QHApp app = (QHApp) getApplication();
		app.removeListener(this);

		// this.unregisterReceiver(receiver);

		super.onDestroy();
	}

	@Override
	protected void onStart() {

		if (msg != null) {
			this.showMsg(msg);
			msg = null;
		}

		QHApp app = (QHApp) getApplication();
		app.model.menuGalleryActivity = null;

		// Register
		IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_FINISH);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		this.registerReceiver(receiver, filter);

		refreshData();

		super.onStart();
	}

	@Override
	protected void onStop() {
		this.unregisterReceiver(receiver);
		super.onStop();
	}

	private Dialog dialog;

	private void showMsg(String msg) {

		QHApp app = (QHApp) getApplication();

		AlertDialog.Builder builder;

		builder = new AlertDialog.Builder(this);
		builder.setTitle("QHappy");
		builder.setMessage(msg).setPositiveButton("ตกลง",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog = null;
					}
				});

		dialog = builder.create();
		dialog.show();
	}

	public void refreshData() {
		if (queueData != null) {
			Log.d("QHappy", "Refresh data in QueueDatil Page.");
			app = (QHApp) getApplication();

			Boolean found_it = false;
			for (int i = 0; i < app.model.myq_data.size(); i++) {
				QueueData q = app.model.myq_data.get(i);
				if (q.queue_object_id.equals(queueData.queue_object_id)) {
					found_it = true;
					queueData = app.model.currentQueueData = q;
					break;
				}
			}

			if (found_it) {
				if (queueData.current_stage != null) {
					String value_s = "";
					try {
						value_s = Config.current_stage_status_value
								.getString(queueData.current_stage);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					queue_reserve.setText(value_s);
					// if(queueData.current_stage.equals(Config.QH_STAGE_ORDER))
					// queue_reserve.setText(Config.currect_stage_order_messgae);
					// else
					// if(queueData.current_stage.equals(Config.QH_STAGE_DELIVERY))
					// queue_reserve.setText(Config.currect_stage_derivery_messgae);
					// else
					// if(queueData.current_stage.equals(Config.QH_STAGE_COMPLAIN))
					// queue_reserve.setText(Config.currect_stage_complain_messgae);

				}
				progress_bar.setVisibility(View.INVISIBLE);
				try {
					if (queueData.queue_remain == -1) {
						progress_bar.setVisibility(View.VISIBLE);
						queue_remain.setText("");
					} else
						queue_remain.setText("" + queueData.queue_remain);

				} catch (Error e) {
				}
				;
				if (queueData.counter_number != 0) {
					queue_remain.setTextSize(50);
					queue_remain.setText(Config.counter_number_message
							+ queueData.counter_number);
					unit_text.setVisibility(View.INVISIBLE);
					if (nextQueueImageView != null)
						nextQueueImageView.setVisibility(View.INVISIBLE);
				} else {
					unit_text.setVisibility(View.VISIBLE);
					if (nextQueueImageView != null)
						nextQueueImageView.setVisibility(View.VISIBLE);
				}
			}
		}
	}

	public void initData() {
		app = (QHApp) getApplication();

		queueData = app.model.currentQueueData;
		if (queueData == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("No queue data please contact shop.")

			.setNegativeButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// User cancelled the dialog
				}
			});
			// Create the AlertDialog object and return it
			builder.create();
			return;
		}

		branch = app.model
				.getBranchDataWithID(app.model.currentQueueData.branch_id);

		startDateText = (TextView) findViewById(R.id.start_date);
		brand_image = (ParseImageView) findViewById(R.id.brand_image);

		brand_name = (TextView) findViewById(R.id.brand_name);
		branch_name = (TextView) findViewById(R.id.branch_name);
		branch_info = (TextView) findViewById(R.id.branch_info);
		queue_group = (TextView) findViewById(R.id.queue_group);
		queue_reserve = (TextView) findViewById(R.id.queue_reserve);
		queue_remain = (TextView) findViewById(R.id.queue_remain);
		fav_btn = (ImageButton) findViewById(R.id.fav_btn);
		btnLoginTwitter = (ImageButton) findViewById(R.id.tweetbutton);
		menu_title = (TextView) findViewById(R.id.textView2);

		Date startDate = new Date(queueData.time_start);
		startDateText.setText(startDate.toLocaleString());

		Button cancel_btn = (Button) findViewById(R.id.button1);
		cancel_btn.setTypeface(app.model.DB_OZONEX_BOLD);
		queue_remain.setTypeface(app.model.DB_OZONEX_BOLD);
		startDateText.setTypeface(app.model.DB_OZONEX_BOLD);
		queue_group.setTypeface(app.model.DB_OZONEX_BOLD);
		queue_reserve.setTypeface(app.model.DB_OZONEX_BOLD);

		brand_name.setTypeface(app.model.DB_OZONEX_BOLD);
		branch_name.setTypeface(app.model.DB_OZONEX);
		branch_info.setTypeface(app.model.DB_OZONEX);
		menu_title.setTypeface(app.model.DB_OZONEX);
		menu_title.setTextSize(22);

		unit_text = (TextView) findViewById(R.id.textView1);
		unit_text.setTypeface(app.model.DB_OZONEX_BOLD);

		progress_bar = (ProgressBar) findViewById(R.id.progressBar1);

		isFav = false;

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid", queueData.branch_id);
		params.put("device_token", ParseInstallation.getCurrentInstallation()
				.getString("deviceToken"));

		if (Config.useParseServer == false)
			QHappyService.request(QHappyService.REQUEST_ACTION_FAVORITE_EXISTS,
					params, new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {
							if (arg2.equals("true"))
								isFav = false;
							else
								isFav = true;

							if (isFav)
								fav_btn.setImageDrawable(getResources()
										.getDrawable(
												R.drawable.waiting_heart_red_image));
							else
								fav_btn.setImageDrawable(getResources()
										.getDrawable(
												R.drawable.waiting_heart_gray_image));

							fav_btn.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									if (!isFav)
										setFav();
									else
										unSetFav();
								}
							});
						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {
							Log.d("QHappy", "can't load favorite exists.");
						}
					});
		else
			ParseCloud.callFunctionInBackground("favoriteExists", params,
					new FunctionCallback<String>() {
						public void done(String result, ParseException e) {
							if (e == null) {
								if (result.equals("true"))
									isFav = false;
								else
									isFav = true;

								if (isFav)
									fav_btn.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.waiting_heart_red_image));
								else
									fav_btn.setImageDrawable(getResources()
											.getDrawable(
													R.drawable.waiting_heart_gray_image));

								fav_btn.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										if (!isFav)
											setFav();
										else
											unSetFav();
									}
								});
							} else {
							}

						}
					});

		BranchListData branch_data = app.model
				.getBranchDataWithID(queueData.branch_id);

		if (branch_data != null) {
			band_object_id = branch_data.brand().getObjectId();

			String brand_name_title = branch_data.brand().getString(
					"brand_name");
			branch_name.setText(branch_data.branch_name());
			branch_info.setText(branch_data.other_info());
			queue_group.setText(queueData.group_id + "" + queueData.queue_id);
			queue_reserve.setText("สำหรับ " + queueData.reserved_capacity
					+ " ท่าน");

			progress_bar.setVisibility(View.INVISIBLE);

			if (queueData.queue_remain == -1) {
				progress_bar.setVisibility(View.VISIBLE);
				queue_remain.setText("");
			} else
				queue_remain.setText("" + queueData.queue_remain);

			nextQueueImageView = (ImageView) findViewById(R.id.imageView4);

			if (queueData.counter_number != 0) {
				queue_remain.setTextSize(50);
				queue_remain.setText(Config.counter_number_message
						+ queueData.counter_number);
				unit_text.setVisibility(View.INVISIBLE);
				nextQueueImageView.setVisibility(View.INVISIBLE);
			} else {
				unit_text.setVisibility(View.VISIBLE);
				nextQueueImageView.setVisibility(View.VISIBLE);
			}

			Button cancel_queue_btn = (Button) findViewById(R.id.button1);
			cancel_queue_btn.setVisibility(View.VISIBLE);

			if (queueData.current_stage != null
					&& Config.current_stage_status_value != null) {
				String value_s = "";
				try {
					value_s = Config.current_stage_status_value
							.getString(queueData.current_stage);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (value_s != "")
					queue_reserve.setText(value_s);

				if (queueData.current_stage.equals("claimed")) {
					cancel_queue_btn.setVisibility(View.INVISIBLE);
					FrameLayout queue_layout = (FrameLayout) findViewById(R.id.frameLayout2);
					queue_layout.removeAllViews();

					FrameLayout queue_layout1 = (FrameLayout) findViewById(R.id.frameLayout1);
					queue_layout1.setBackgroundColor(Color.WHITE);
				}

			}

			if (brand_name_title.equals("SOL"))
				menu_title.setText("ดูรายการสินค้า");

			brand_image.setPlaceholder(getResources().getDrawable(
					R.drawable.logo_qhappy));
			ParseFile image_file = branch_data.branch_image_file();
			brand_image_uri = image_file.getUrl();
			brand_image.setParseFile(image_file);
			brand_image.loadInBackground();

			share_image = new ParseImageView(ctx);
			if (branch_data.share_image_file() != null) {
				share_image.setParseFile(branch_data.share_image_file());
				share_image.loadInBackground();
			}

			ParseObject brandObject = branch_data.brand();
			if (brandObject.isDataAvailable()) {
				brand_name.setText(brandObject.getString("brand_name"));
			} else {
				brandObject.fetchInBackground(new GetCallback<ParseObject>() {
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							brand_name.setText(object.getString("brand_name"));
						} else {
						}
					}
				});
			}
		}

	}

	protected void setFav() {
		// check internet connection.
		if (!app.getInternetConnection())
			return;

		isFav = true;
		fav_btn.setImageDrawable(getResources().getDrawable(
				R.drawable.waiting_heart_red_image));

		createProgressWithString("Please Wait..", "Loading...");

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid", queueData.branch_id);
		params.put("device_token", ParseInstallation.getCurrentInstallation()
				.getString("deviceToken"));

		if (Config.useParseServer == false)
			QHappyService.request(QHappyService.REQUEST_ACTION_SET_FAV, params,
					new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {

						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {
						}

						@Override
						public void onFinish() {
							removeProgressDialog();
							super.onFinish();
						}
					});
		else
			ParseCloud.callFunctionInBackground("setFav", params,
					new FunctionCallback<String>() {
						public void done(String result, ParseException e) {
							if (e == null) {

							} else {

							}
							removeProgressDialog();

						}
					});
	}

	private void removeProgressDialog() {
		if (ringProgressDialog != null) {
			try {
				ringProgressDialog.dismiss();
			} catch (Error e) {
				Log.d("QHappt", "can't dismiss progress dialog.");
			}
		}
		ringProgressDialog = null;
	}

	protected void unSetFav() {
		// check internet connection.
		if (!app.getInternetConnection())
			return;

		isFav = false;
		fav_btn.setImageDrawable(getResources().getDrawable(
				R.drawable.waiting_heart_gray_image));

		createProgressWithString("Please Wait..", "Loading...");

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid", queueData.branch_id);
		params.put("device_token", ParseInstallation.getCurrentInstallation()
				.getString("deviceToken"));

		if (Config.useParseServer == false)
			QHappyService.request(QHappyService.REQUEST_ACTION_UN_SET_FAV,
					params, new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {

						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {
						}

						@Override
						public void onFinish() {
							removeProgressDialog();
							super.onFinish();
						}
					});
		else
			ParseCloud.callFunctionInBackground("unSetFav", params,
					new FunctionCallback<String>() {
						public void done(String result, ParseException e) {
							if (e == null) {

							} else {

							}
							removeProgressDialog();
						}
					});
	}

	public void close() {
		setResult(1);
		this.finish();
	}

	private void createProgressWithString(String msg1, String msg2) {
		if (ringProgressDialog != null) {
			ringProgressDialog.cancel();
			ringProgressDialog = null;
		}

		ringProgressDialog = ProgressDialog.show(this, msg1, msg2);
	}

	public void confirmCancle() {
		// check internet connection.
		if (!app.getInternetConnection())
			return;

		createProgressWithString("Please Wait..", "Loading...");

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid", queueData.branch_id);
		params.put("queue_object_id", queueData.queue_object_id);
		params.put("time_stop", new Date().getTime() / 1000);

		for (int i = 0; i < app.model.myq_data.size(); i++) {

			QueueData queueDataModel = app.model.myq_data.get(i);
			if (queueDataModel.branch_id.equals(queueData.branch_id)) {
				app.model.myq_data.remove(i);

				try {
					app.model.homeActivity.saveMyQInTemp();
				} catch (Error e) {
				}
				;
				// remove queue data in temp.
				if (app.mListItemsTemp != null) {
					app.mListItemsTemp.remove(queueDataModel);
					if (app.mListItemsTemp.size() == 0)
						app.mListItemsTemp = null;
				}
				break;
			}
		}

		if (Config.useParseServer == false) {
			// Request to www.qhappy.com
			QHappyService.request(QHappyService.REQUEST_ACTION_CANCEL, params,
					new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {
							AlertDialog.Builder builder = new AlertDialog.Builder(
									app.model.mainActivity);
							builder.setMessage("คิวได้ถูกยกเลิกแล้วค่ะ")
									.setPositiveButton(
											"ตกลง",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {

												}
											});
							builder.create();
						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {
							AlertDialog.Builder builder = new AlertDialog.Builder(
									app.model.mainActivity);
							builder.setMessage(Config.cencel_error_message)
									.setPositiveButton(
											"ตกลง",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {

												}
											});
							builder.create();
						}

						@Override
						public void onFinish() {
							removeProgressDialog();
							close();
							super.onFinish();
						}
					});
		} else {
			// TODO Change

			ParseCloud.callFunctionInBackground("cancelQueue", params,
					new FunctionCallback<String>() {
						public void done(String result, ParseException e) {
							removeProgressDialog();
							if (e == null) {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										app.model.mainActivity);
								builder.setMessage("คิวได้ถูกยกเลิกแล้วค่ะ")
										.setPositiveButton(
												"ตกลง",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {

													}
												});
								builder.create();
							} else {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										app.model.mainActivity);
								builder.setMessage(Config.cencel_error_message)
										.setPositiveButton(
												"ตกลง",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {

													}
												});
								builder.create();
							}
							close();
						}
					});
		}
	}

	public void queueDetailClickCancle(View view) {

		// check internet connection.
		if (!app.getInternetConnection())
			return;
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("ต้องการยกเลิกคิว");
		builder.setMessage(
				"หากต้องการต่อคิวหลังจากยกเลิกแล้ว \nจะต้องเริ่มต่อจากคนล่าสุดนะคะ")
				.setPositiveButton("ยืนยัน ยกเลิก",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								confirmCancle();
							}
						})
				.setNegativeButton("ไม่ยกเลิก",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

							}
						});

		builder.create();
		builder.show();
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public void onStart() {

			QueueDetailActivity activity = (QueueDetailActivity) getActivity();
			activity.initData();

			super.onStart();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_queue_detail,
					container, false);

			return rootView;
		}
	}

	// event
	// Tweet
	public void share_twitter(View view) {
		// check internet connection.
		if (!app.getInternetConnection())
			return;

		if (mTwitter.hasAccessToken()) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setMessage("Want to post Tweet on Twitter?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									postToTwitter();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			final AlertDialog alert = builder.create();

			alert.show();
		} else {
			mTwitter.authorize();
		}
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			mProgressDlg.dismiss();
			String text = (msg.what == 0) ? "Posted to Twitter"
					: "Post to Twitter failed :";

			Toast.makeText(QueueDetailActivity.this, text, Toast.LENGTH_SHORT)
					.show();
		}
	};

	private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {
		@Override
		public void onComplete(String value) {
			String username = mTwitter.getUsername();
			username = (username.equals("")) ? "No Name" : username;

			Toast.makeText(QueueDetailActivity.this,
					"Connected to Twitter : " + username, Toast.LENGTH_LONG)
					.show();

			share_twitter(new View(QueueDetailActivity.this));
		}

		@Override
		public void onError(String value) {
			Toast.makeText(QueueDetailActivity.this,
					"Twitter connection failed by:" + value, Toast.LENGTH_LONG)
					.show();
		}
	};

	public void postToTwitter() {
		mProgressDlg.setMessage("Sending ...");
		mProgressDlg.show();

		new Thread() {
			@Override
			public void run() {
				int what = 0;

				try {
					String text = "คุณ " + mTwitter.getUsername() + " "
							+ branch.share_msg() + " อยู่ที่ "
							+ branch.brand().getString("brand_name") + " สาขา"
							+ branch.branch_name();
					mTwitter.updateStatus(text);
				} catch (Exception e) {
					what = 1;
				}

				mHandler.sendMessage(mHandler.obtainMessage(what));
			}
		}.start();
	}

	public void share_facebook(View view) {
		// check internet connection.
		if (!app.getInternetConnection())
			return;
		this.share("facebook");
	}

	void share(String nameApp) {
		try {
			share_image.setDrawingCacheEnabled(true);

			Bitmap bitmap = share_image.getDrawingCache();
			File root = Environment.getExternalStorageDirectory();
			final File cachePath = new File(root.getAbsolutePath()
					+ "/DCIM/Camera/image.jpg");
			try {
				cachePath.createNewFile();
				FileOutputStream ostream = new FileOutputStream(cachePath);
				bitmap.compress(CompressFormat.JPEG, 100, ostream);
				ostream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<Intent> targetedShareIntents = new ArrayList<Intent>();
			Intent share = new Intent(android.content.Intent.ACTION_SEND);
			share.setType("image/jpeg");
			List<ResolveInfo> resInfo = getPackageManager()
					.queryIntentActivities(share, 0);
			if (!resInfo.isEmpty()) {
				for (ResolveInfo info : resInfo) {

					Intent targetedShare = new Intent(
							android.content.Intent.ACTION_SEND);
					targetedShare.setType("image/jpeg"); // put here your mime
															// type
					if (info.activityInfo.packageName.toLowerCase().contains(
							nameApp)
							|| info.activityInfo.name.toLowerCase().contains(
									nameApp)) {
						targetedShare.putExtra(Intent.EXTRA_SUBJECT,
								"Sample Photo");
						targetedShare.putExtra(Intent.EXTRA_TEXT,
								"This photo is created by App Name");
						targetedShare.putExtra(Intent.EXTRA_STREAM,
								Uri.fromFile(cachePath));
						targetedShare.setPackage(info.activityInfo.packageName);
						targetedShareIntents.add(targetedShare);
					}
				}
				Intent chooserIntent = Intent.createChooser(
						targetedShareIntents.remove(0), "Select app to share");
				chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
						targetedShareIntents.toArray(new Parcelable[] {}));
				startActivity(chooserIntent);
			}
		} catch (Exception e) {
			Log.v("VM",
					"Exception while sending image on" + nameApp + " "
							+ e.getMessage());
		}
	}

	public void open_menu(View view) {
		// check internet connection.
		if (!app.getInternetConnection())
			return;

		app.model.currentMenusParseObject = new ArrayList<ParseObject>();

		mProgressDlg.setMessage("Loading ...");
		mProgressDlg.show();

		// [PFCloud callFunctionInBackground:@"getMenuWithBrandID"
		// withParameters:@{@"brand_id":band_object_id}
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("brand_id", band_object_id);
		ParseCloud.callFunctionInBackground("getMenuWithBrandID", params,
				new FunctionCallback<List<ParseObject>>() {
					public void done(List<ParseObject> result, ParseException e) {
						if (e == null) {
							mProgressDlg.dismiss();
							app.model.currentMenusParseObject = result;

							Intent menuGalleryActivity = new Intent(
									QueueDetailActivity.this,
									MenuGalleryActivity.class);

							startActivity(menuGalleryActivity);
						}
					}
				});

		// ParseQuery<ParseObject> query = branch.menus().getQuery();
		// query.addAscendingOrder("order");
		// query.findInBackground(new FindCallback<ParseObject>() {
		// @Override
		// public void done(List<ParseObject> result, ParseException arg1) {
		// mProgressDlg.dismiss();
		// app.model.currentMenusParseObject = result;
		//
		// Intent term_activity = new Intent(QueueDetailActivity.this,
		// MenuGalleryActivity.class);
		//
		// startActivity(term_activity);
		// }
		// });
		// ParseRelation relation = branch.
	}

	private static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;
	private String brand_image_uri;

	// post facebook
	private void publishStory() {
		Session session = Session.getActiveSession();

		Toast.makeText(getApplicationContext(), "session" + session,
				Toast.LENGTH_LONG).show();

		if (session != null) {

			// Check for publish permissions
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(PERMISSIONS, permissions)) {
				pendingPublishReauthorization = true;
				Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
						this, PERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				return;
			}

			Bundle postParams = new Bundle();
			postParams.putString("name", "Facebook SDK for Android");
			postParams.putString("caption",
					"Build great social apps and get more installs.");
			postParams
					.putString(
							"description",
							"The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
			postParams.putString("link",
					"https://developers.facebook.com/android");
			postParams
					.putString("picture",
							"https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

			Request.Callback callback = new Request.Callback() {

				@Override
				public void onCompleted(Response response) {
					JSONObject graphResponse = response.getGraphObject()
							.getInnerJSONObject();
					String postId = null;
					try {
						postId = graphResponse.getString("id");
					} catch (JSONException e) {
						Log.i(Session.TAG, "JSON error " + e.getMessage());
					}
					FacebookRequestError error = response.getError();
					if (error != null) {
						Toast.makeText(getApplicationContext(),
								error.getErrorMessage(), Toast.LENGTH_SHORT)
								.show();
					} else {
						Toast.makeText(getApplicationContext(), postId,
								Toast.LENGTH_LONG).show();
					}
				}
			};

			Request request = new Request(session, "me/feed", postParams,
					HttpMethod.POST, callback);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}
	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void appCallBack(JSONObject jsonObj) {

	}

}
