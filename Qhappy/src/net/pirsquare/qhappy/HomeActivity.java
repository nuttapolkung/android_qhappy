package net.pirsquare.qhappy;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import net.pirsquare.qhappy.model.BranchListData;
import net.pirsquare.qhappy.model.CancelQueueData;
import net.pirsquare.qhappy.model.PromotionData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.model.WaitQueueData;
import net.pirsquare.qhappy.receiver.ResponseReceiver;
import net.pirsquare.qhappy.service.IntervalGetNotificationService;
import net.pirsquare.qhappy.service.MyQFetchDataBGService;
import net.pirsquare.qhappy.service.QHappyService;
import net.pirsquare.qhappy.service.ServiceCallBack;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class HomeActivity extends ActionBarActivity implements
		ActionBar.TabListener, AppListener, ServiceCallBack {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	Activity ctx;

	private ProgressDialog ringProgressDialog;

	private void dismissProgressDialog() {
		if (ringProgressDialog != null) {
			ringProgressDialog.cancel();
			ringProgressDialog.dismiss();
		}
		ringProgressDialog = null;
	}

	public Boolean isStart = false;

	Boolean isTempNotification = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		// getActionBar().hide();

		ctx = this;

		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// actionBar.setBackgroundDrawable(new ColorDrawable(0xffE33124));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("QHappy");
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowTitleEnabled(true);
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.

			actionBar.addTab(actionBar.newTab()

			// .setCustomView(mSectionsPagerAdapter.getIcon(i))
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					// .setIcon(mSectionsPagerAdapter.getIcon(i))
					.setTabListener(this));

		}

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();

		final QHApp app = (QHApp) getApplication();
		app.addListener(this);
		app.model.homeActivity = this;
		app.service.addListener(this);

		app.model.loadLocalData();
		app.model.readPromotion();

		getWindow().setUiOptions(
				ActivityInfo.UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW);

		try {
			SharedPreferences sharedPref = app.model.mainActivity
					.getPreferences(Context.MODE_PRIVATE);

			String queue_count = sharedPref.getString("haveQ", "");
			if (!queue_count.equals(""))
				mViewPager.setCurrentItem(2);
			else {
				app.service.getMyQ();
				mViewPager.setCurrentItem(0);
			}
		} catch (Exception error) {
			mViewPager.setCurrentItem(2);
		}

		// add filter for receive data in background.
		receiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {

						String json_string = "";
						try {
							Log.d("QHappy", "BroadCast Receive >> "
									+ intent.getExtras().getString("json"));
							json_string = intent.getExtras().getString("json");

							QHApp app = (QHApp) context.getApplicationContext();

							Editor editor = app.getSharedPreferences("temp_q",
									Context.MODE_PRIVATE).edit();
							editor.putString("temp", json_string);
							editor.commit();
						} catch (Exception e) {
							Log.d("QHappy", "Can't save queue in temp.");
						}

						if (!json_string.equals(""))
							try {
								ArrayList<QueueData> list = new ArrayList<QueueData>();
								JSONArray json_list = new JSONArray(json_string);
								for (int i = 0; i < json_list.length(); i++) {
									QueueData q = new QueueData(null);
									q.setJSONObject(json_list.getJSONObject(i));
									list.add(q);
								}

								// check expire claim queueu evenry 20 sec.
								app.model.checkExpireclaimQueue();

								app.model.myq_data = list;

							} catch (JSONException e) {
								e.printStackTrace();
							}
					}
				});

		// Register filter broadcast
		IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);

		this.registerReceiver(receiver, filter);

		// Register filter broadcast interval notification
		receiver_notification = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {

						if (checkIsLock())
							return;

						String json_string = "";
						try {
							json_string = intent.getExtras().getString("json");
						} catch (Exception e) {
							Log.d("QHappy",
									"Can't get json from fetch notification in background.");
						}

						if (!json_string.equals(""))
							try {
								JSONArray noti_list = new JSONArray(json_string);

								int noti_length = noti_list.length();
								if (noti_length > 0) {
									// Check notification if last notification
									// is update queue then before notification
									// is update_status do fire notification
									// update_status instead.
									if (noti_length >= 2) {
										JSONObject j1 = noti_list
												.getJSONObject(noti_length - 1);
										String qs1 = j1.getString("qs");
										if (qs1.equals("update")) {
											JSONObject j2 = noti_list
													.getJSONObject(noti_length - 2);
											String qs2 = j2.getString("qs");

											if (qs2.equals("update_status"))
												appCallBack(noti_list
														.getJSONObject(noti_length - 2));
											else
												appCallBack(noti_list
														.getJSONObject(noti_length - 1));
										} else
											appCallBack(noti_list
													.getJSONObject(noti_length - 1));
									} else
										appCallBack(noti_list
												.getJSONObject(noti_length - 1));
								}

								// Old Function fire any notification.
								// for (int i = 0; i < noti_list.length(); i++)
								// {
								// Log.d("QHappy",
								// "Fire from manual get noti.");
								//
								// final JSONObject j =
								// noti_list.getJSONObject(i);
								// new Handler().postDelayed(new Runnable() {
								//
								// @Override
								// public void run() {
								// appCallBack(j);
								// }
								// }, 10);
								// }

								JSONObject noti = new JSONObject();
								if (noti_list.length() > 0) {
									noti = noti_list.getJSONObject(noti_list
											.length() - 1);

									writeToFile(String.valueOf(noti.get("id")),
											"lastid.txt");
									writeToFile(
											String.valueOf(noti.get("time")),
											"lasttime.txt");
								}

							} catch (JSONException e) {
								e.printStackTrace();
							}

						Log.d("QHappy", "Get notification result >> "
								+ json_string);
					}
				});

		filter = new IntentFilter(
				ResponseReceiver.ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND);
		filter.addCategory(Intent.CATEGORY_DEFAULT);

		this.registerReceiver(receiver_notification, filter);

		// get localstorage
		SharedPreferences s = app.getSharedPreferences("temp_q",
				Context.MODE_PRIVATE);
		String resultsss = s.getString("temp", "");

		ArrayList<QueueData> test_read = new ArrayList<QueueData>();
		try {
			JSONArray temps_read = new JSONArray(resultsss);
			for (int i = 0; i < temps_read.length(); i++) {
				QueueData q = new QueueData(null);
				q.setJSONObject(temps_read.getJSONObject(i));
				test_read.add(q);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		app.model.myq_data = test_read;

		receiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {
						if (app.model.notConnectInternetActivity == null) {
							Intent nonet = new Intent(context,
									NotConnectInternetActivity.class);
							nonet.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							nonet.putExtra("want_refresh", true);
							context.startActivity(nonet);
						}

					}
				});

		this.registerReceiver(receiver, new IntentFilter("net.pirsquare.noNet"));

		// create stop start service receiver
		start_stop_receiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {

						Boolean is_start = intent.getBooleanExtra("start",
								false);

						if (is_start)
							startBackgroundService();
						else if (!app.model.isReserve)
							stopBackgroundService();
					}
				});

		IntentFilter start_stop_intent_filter = new IntentFilter(
				"net.pirsquare.service_order");
		start_stop_intent_filter.addCategory("net.pirsquare.qhappy");
		this.registerReceiver(start_stop_receiver, start_stop_intent_filter);

		ResponseReceiver quickReserveReceiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {
						String branchID = intent.getStringExtra("branchID");
						Log.d("QHappy", "branch" + branchID);
						// call reserve by code.
						checkQRCode(branchID);
					}
				});

		IntentFilter quickReserveIntentFilter = new IntentFilter(
				StandbyFragment.QUICK_RESERVE_ACTION);
		quickReserveIntentFilter.addCategory("net.pirsquare.qhappy");
		this.registerReceiver(quickReserveReceiver, quickReserveIntentFilter);

		startGetMYQService();
		startGetNotificationService();
		// reset
		// writeToFile("0");
	}

	Handler delaySetCancelReserveHandle = new Handler();
	Runnable delaySetCancelReserveFlag = new Runnable() {

		@Override
		public void run() {
			QHApp app = (QHApp) ctx.getApplicationContext();
			if (app != null) {
				app.model.isReserve = false;
			}
		}
	};

	public void cancelDelaySetCancelReserve() {
		try {
			delaySetCancelReserveHandle
					.removeCallbacks(delaySetCancelReserveFlag);
		} catch (Exception e) {
		}

	}

	public void delaySetCancelReserve() {
		delaySetCancelReserveHandle.postDelayed(delaySetCancelReserveFlag,
				(60 * 5) * 1000);
	}

	private void startBackgroundService() {
		Log.d("QHappy", "startBackgroundService");
		startGetMYQService();
		startGetNotificationService();
	}

	private void stopBackgroundService() {
		Log.d("QHappy", "stopBackgroundService");
		Intent interval_intent = new Intent(getApplicationContext(),
				IntervalGetNotificationService.class);
		interval_intent.setData(Uri.parse(ParseInstallation
				.getCurrentInstallation().getString("deviceToken")));
		PendingIntent pintent_interval = PendingIntent.getService(
				getApplicationContext(), 0, interval_intent, 0);

		AlarmManager alarm_interval = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		// Start every 30 seconds
		try {
			alarm_interval.cancel(pintent_interval);
		} catch (Exception e) {
		}

		Intent intent = new Intent(getApplicationContext(),
				MyQFetchDataBGService.class);
		intent.setData(Uri.parse(ParseInstallation.getCurrentInstallation()
				.getString("deviceToken")));

		PendingIntent pintent = PendingIntent.getService(
				getApplicationContext(), 0, intent, 0);

		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		// Start every 20 seconds
		try {
			alarm.cancel(pintent);
		} catch (Exception e) {
			Log.d("QHappy", "Can't stop get myq service.");
		}
	}

	public void startGetNotificationService() {
		Calendar cal = Calendar.getInstance();
		forceStopService(getPackageName() + ":fetch_notification");
		// if (!checkRuningService(getPackageName() + ":fetch_notification")) {
		// start interval get notification service
		Intent interval_intent = new Intent(getApplicationContext(),
				IntervalGetNotificationService.class);
		interval_intent.setData(Uri.parse(ParseInstallation
				.getCurrentInstallation().getString("deviceToken")));
		PendingIntent pintent_interval = PendingIntent.getService(
				getApplicationContext(), 0, interval_intent, 0);

		AlarmManager alarm_interval = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		// Start every 30 seconds
		try {
			alarm_interval.cancel(pintent_interval);
		} catch (Exception e) {
			Log.d("QHappy", "Can't stop get notification service.");
		}

		alarm_interval.setRepeating(AlarmManager.RTC_WAKEUP,
				cal.getTimeInMillis(), 18 * 1000, pintent_interval);

		startService(interval_intent);
		// }
	}

	public void startGetMYQService() {
		Calendar cal = Calendar.getInstance();
		forceStopService(getPackageName() + ":fetch_data");

		Intent intent = new Intent(getApplicationContext(),
				MyQFetchDataBGService.class);
		intent.setData(Uri.parse(ParseInstallation.getCurrentInstallation()
				.getString("deviceToken")));

		PendingIntent pintent = PendingIntent.getService(
				getApplicationContext(), 0, intent, 0);

		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		// Start every 20 seconds
		try {
			alarm.cancel(pintent);
		} catch (Exception e) {
			Log.d("QHappy", "Can't stop get myq service.");
		}

		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
				20 * 1000, pintent);

		startService(intent);
		// }
	}

	private Boolean checkIsLock() {
		PowerManager pm = (PowerManager) this
				.getSystemService(Context.POWER_SERVICE);

		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		KeyguardManager myKM = (KeyguardManager) this
				.getSystemService(Context.KEYGUARD_SERVICE);

		if (myKM.inKeyguardRestrictedInputMode()) {
			return true;
			// }else if (!pm.isScreenOn() ||
			// myKM.inKeyguardRestrictedInputMode()) {
			// // PowerManager.WakeLock wl = pm.newWakeLock(
			// // PowerManager.SCREEN_BRIGHT_WAKE_LOCK
			// // | PowerManager.ACQUIRE_CAUSES_WAKEUP, "QHappy");
			// // wl.acquire();
			// // wl.release();
			// Log.d("QHappy", "App is Lock.");
			// return true;
		} else {
			Log.d("QHappy", "App is UnLock.");
			return false;
			// mNotificationManager.cancelAll();
		}
	}

	private Boolean checkRuningService(String service_name) {
		Boolean runing = false;
		ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> runningAppProcesses = am
				.getRunningAppProcesses();

		Iterator<RunningAppProcessInfo> iter = runningAppProcesses.iterator();

		while (iter.hasNext()) {
			RunningAppProcessInfo next = iter.next();

			Log.d("PROCESS", next.processName);

			if (next.processName.equals(service_name)) {
				Log.d("QHappy", "running process name >> " + next.processName);
				runing = true;
				break;
			}
		}

		return runing;
	}

	public void writeToFile(String data, String filename) {

		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					openFileOutput(filename, Context.MODE_PRIVATE));
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public void alert() {
		PowerManager pm = (PowerManager) this
				.getSystemService(Context.POWER_SERVICE);

		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		KeyguardManager myKM = (KeyguardManager) this
				.getSystemService(Context.KEYGUARD_SERVICE);

		if (!pm.isScreenOn() || myKM.inKeyguardRestrictedInputMode()) {
			PowerManager.WakeLock wl = pm.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP, "QHappy");
			wl.acquire();
			wl.release();

		} else {

			// mNotificationManager.cancelAll();
		}

		// create voice
		Uri notification = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		if (notification != null) {
			MediaPlayer mp = MediaPlayer.create(this.getApplicationContext(),
					notification);
			if (mp != null)
				mp.start();
		}

		// create vibrate.
		Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		if (v != null)
			v.vibrate(500);
		//

	}

	private ResponseReceiver start_stop_receiver;
	// notify when operation in Service finished
	private ResponseReceiver receiver;
	// notify when operation in Service finished
	private ResponseReceiver receiver_notification;

	@Override
	protected void onDestroy() {

		QHApp app = (QHApp) getApplication();
		app.removeListener(this);
		app.service.removeListener(this);

		this.unregisterReceiver(receiver);
		this.unregisterReceiver(receiver_notification);
		this.unregisterReceiver(start_stop_receiver);

		// forceStopService(getPackageName() + ":fetch_data");

		super.onDestroy();
	}

	private void forceStopService(String service_name) {
		ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> runningAppProcesses = am
				.getRunningAppProcesses();

		Iterator<RunningAppProcessInfo> iter = runningAppProcesses.iterator();

		while (iter.hasNext()) {
			RunningAppProcessInfo next = iter.next();

			Log.d("PROCESS", next.processName);

			if (next.processName.equals(service_name)) {
				Log.d("QHappy", "Kill process name >> " + next.processName);
				android.os.Process.killProcess(next.pid);
			}
		}
	}

	@Override
	public void callBackGetBranchMetaData(List<ParseObject> result) {
	}

	@Override
	public void callBackGetBrandMetaData(List<ParseObject> result) {
	}

	@Override
	public void callBackGetFavData(List<ParseObject> result) {
	}

	@Override
	public void callBackGetMyQData(List<ParseObject> result) {
		QHApp app = (QHApp) getApplication();

		ArrayList<QueueData> myQ_data = new ArrayList<QueueData>();

		for (ParseObject obj : result) {
			QueueData queue_data = new QueueData(obj);
			myQ_data.add(queue_data);
		}

		app.model.myq_data = myQ_data;
	}

	@Override
	public void Error(ParseException e) {

	}

	private Boolean exit = false;

	@Override
	public void onBackPressed() {
		if (exit)
			this.finish();
		else {
			Toast.makeText(this, "Press Back again to Exit.",
					Toast.LENGTH_SHORT).show();
			exit = true;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					exit = false;
				}
			}, 3 * 1000);

		}

	}

	FrameLayout badge_container;
	TextView badge_text;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		QHApp app = (QHApp) getApplication();
		// Inflate the menu;
		// this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);

		MenuItem item = menu.findItem(R.id.action_promotion);
		MenuItemCompat.setActionView(item, R.layout.promotion_badge_layout);

		RelativeLayout badge_layout = (RelativeLayout) MenuItemCompat
				.getActionView(item);

		badge_container = (FrameLayout) badge_layout
				.findViewById(R.id.badge_container);

		badge_text = (TextView) badge_layout.findViewById(R.id.textView1);
		badge_text.setTypeface(app.model.DB_OZONEX);

		final Activity ctx = this;

		badge_layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent promotion = new Intent(ctx, PromotionActivity.class);
				startActivity(promotion);
			}
		});

		updateBadge();

		return true;
	}

	private void updateBadge() {
		QHApp app = (QHApp) getApplication();
		if (app.model.unread_promotion == 0) {
			if (badge_container != null)
				badge_container.setVisibility(View.INVISIBLE);
		} else {
			if (badge_container != null)
				badge_container.setVisibility(View.VISIBLE);
			if (badge_text != null)
				badge_text.setText("" + app.model.unread_promotion);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {

			Intent setting = new Intent(this, PreloadActivity.class);
			startActivity(setting);

			return true;
		} else if (id == R.id.action_promotion) {
			Intent promotion = new Intent(this, PromotionActivity.class);
			startActivity(promotion);

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStop() {

		isStart = false;
		// if(ring_progress_dialog!=null)
		// {
		// ring_progress_dialog.cancel();
		// ring_progress_dialog.dismiss();
		// ring_progress_dialog = null;
		// }
		// this.unregisterReceiver(receiver);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("status", "off");
		params.put("device_token", ParseInstallation.getCurrentInstallation()
				.getString("deviceToken"));

		QHappyService.request("SetMobileStatus", params,
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						Log.d("QHappy", "SetMobileStatus complete.");
					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						Log.d("QHappy", "SetMobileStatus failed.");
					}
				});

		super.onStop();
	}

	@Override
	protected void onStart() {

		// NotificationManager notificationManager =
		// (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		// notificationManager.cancelAll();

		// startGetMYQService();
		// startGetNotificationService();

		isStart = true;
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("status", "on");
		params.put("device_token", ParseInstallation.getCurrentInstallation()
				.getString("deviceToken"));

		QHappyService.request("SetMobileStatus", params,
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						Log.d("QHappy", "SetMobileStatus complete.");
					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						Log.d("QHappy", "SetMobileStatus failed.");
					}
				});

		QHApp app = (QHApp) getApplication();
		app.model.checkUnreadpromotion();
		updateBadge();

		if (app.model.branch_metadata == null)
			loadMetaData();
		else if (app.model.branch_metadata.size() == 0)
			loadMetaData();

		SharedPreferences share = this.getSharedPreferences("help",
				Context.MODE_PRIVATE);

		Boolean isReadedHelp = share.getBoolean("readed_help", false);

		if (isReadedHelp == false) {
			Editor edit = share.edit();
			edit.putBoolean("readed_help", true);
			edit.commit();

			Intent intent = new Intent(this, Tutorial_main.class);
			startActivity(intent);
		}

		if (app.model.myq_data.size() != 0) {
			if (!checkRuningService(getPackageName() + ":fetch_notification")) {
				startBackgroundService();
			}
		}

		if (app.model.notification_temp != "") {
			try {
				JSONArray noti_list = new JSONArray(app.model.notification_temp);

				isTempNotification = true;
				if (noti_list.length() > 0) {
					appCallBack(noti_list.getJSONObject(noti_list.length() - 1));
				}

				JSONObject noti = noti_list
						.getJSONObject(noti_list.length() - 1);

				writeToFile(String.valueOf(noti.get("id")), "lastid.txt");
				writeToFile(String.valueOf(noti.get("time")), "lasttime.txt");

			} catch (JSONException e2) {
				e2.printStackTrace();
			}

			app.model.notification_temp = "";
		}
		super.onStart();
	}

	private void loadMetaData() {

		final ProgressDialog progress = new ProgressDialog(this);
		progress.setTitle("QHappy");
		progress.setMessage("Initalize data.");
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.show();

		ParseCloud.callFunctionInBackground("getMetaData",
				new HashMap<String, Object>(),
				new FunctionCallback<List<ParseObject>>() {
					public void done(List<ParseObject> result, ParseException e) {
						if (e == null) {

							if (result.size() > 0) {
								progress.dismiss();

								QHApp app = (QHApp) getApplication();

								app.model.branch_metadata = new ArrayList<BranchListData>();

								for (ParseObject parseObject : result) {
									BranchListData data = new BranchListData(
											parseObject);
									app.model.branch_metadata.add(data);
								}

								Log.d("Qhappy",
										"Load BranchMetaData Complete...");

							} else {
								progress.dismiss();
								loadMetaData();
								Log.d("Qhappy",
										"Load BranchMetaData No data...");
							}
						} else {

							progress.dismiss();
							loadMetaData();
							Log.d("Qhappy", "Load BranchMetaData Error...");
						}
					}
				});
	}

	private Handler timer_handle;

	public void stopCheckReserve() {
		if (timer_handle != null)
			timer_handle.removeCallbacks(recheckReserve);
		timer_handle = null;
	}

	public void startCheckReserve() {
		timer_handle = new Handler();
		timer_handle.postDelayed(recheckReserve, 3000);
	}

	public Runnable recheckReserve = new Runnable() {

		@Override
		public void run() {

			QHApp app = (QHApp) getApplication();
			HashMap<String, Object> params = new HashMap<String, Object>();

			params.put("qoid", app.model.request_reserve_queue_id);
			params.put("branch_id", app.model.branch_detact.branch_id());
			params.put("device_token", ParseInstallation
					.getCurrentInstallation().getString("deviceToken"));

			if (Config.useParseServer == false)

				QHappyService.request(
						QHappyService.REQUEST_ACTION_CHECK_RESERVE_RESULT,
						params, new TextHttpResponseHandler() {

							@Override
							public void onSuccess(int arg0, Header[] arg1,
									String arg2) {

								Log.d("QHappy", "onCheck Reserve data : "
										+ arg2);

								JSONObject json = null;
								try {
									json = new JSONObject(arg2);
								} catch (JSONException e1) {
									e1.printStackTrace();
								}

								String result = "";
								try {
									result = (String) json.get("result");
								} catch (JSONException e1) {
									e1.printStackTrace();
								}

								if (json != null) {
									if (result.equals("empty")
											|| result.equals("not_found")) {
										if (timer_handle != null)
											timer_handle.postDelayed(
													recheckReserve, 1000);
									} else if (result.equals("rejected")) {
										QHApp app = (QHApp) getApplication();

										if (app.current_activity != null)
											app.current_activity.finish();

										app.current_activity = null;
										app.model.request_reserve_queue_id = null;

										Log.d("QHappy",
												"onCheckReserve founded Reject.");
										if (app.model.request_activity != null) {
											try {
												app.model.request_activity
														.removeTimer();
											} catch (Exception error) {
												Log.d("QHappy",
														error.getMessage());
											}

											app.model.request_activity = null;

											app.model.isReserve = false;

											Intent contactShop = new Intent(
													ctx,
													ContactShopActivity.class);
											contactShop
													.putExtra("msg",
															"คิวถูกยกเลิก กรุณาติดต่อหน้าร้าน");
											startActivity(contactShop);

										}
									} else {
										JSONObject qd = null;
										try {
											qd = new JSONObject(arg2);
										} catch (JSONException e) {
											e.printStackTrace();
										}

										if (qd != null) {
											timer_handle = null;

											String msg = "";
											try {
												msg = qd.getString("alert");
											} catch (JSONException e) {
												e.printStackTrace();
											}

											Intent intent = new Intent(
													"net.pirsquare.accepted");
											sendBroadcast(intent);

											mViewPager.setCurrentItem(1);
											QueueData queueData = new QueueData(
													null);
											try {
												queueData.branch_id = qd
														.getString("bid");
												queueData.group_id = qd
														.getString("gid");
												queueData.queue_id = qd
														.getString("qid");
												queueData.queue_object_id = qd
														.getString("qoid");
												queueData.queue_remain = qd
														.getInt("qr");
												queueData.reserved_capacity = qd
														.getInt("rc");
												queueData.time_estimate = qd
														.getString("te");
												queueData.time_start = qd
														.getLong("ts");
											} catch (JSONException er) {
												er.printStackTrace();
											}

											QHApp app = (QHApp) getApplication();
											if (app.current_activity != null)
												app.current_activity.finish();

											// jsonObj.get
											if (queueData != null)
												app.model.currentQueueData = queueData;

											if (app.model.myq_data == null)
												app.model.myq_data = new ArrayList<QueueData>();

											app.model.myq_data.add(queueData);

											app.model.isReserve = false;

											mViewPager.setCurrentItem(2);
											Intent queue_detail = new Intent(
													ctx,
													QueueDetailActivity.class);
											queue_detail.putExtra("msg", msg);
											startActivity(queue_detail);

											app.model.request_reserve_queue_id = null;

										} else {
											if (timer_handle != null)
												timer_handle.postDelayed(
														recheckReserve, 1000);
										}
									}
								}
							}

							@Override
							public void onFailure(int arg0, Header[] arg1,
									String arg2, Throwable arg3) {

							}
						});
			else

				ParseCloud.callFunctionInBackground(
						"checkReserveResultAndroid", params,
						new FunctionCallback<String>() {
							public void done(String result, ParseException e) {
								if (e == null) {

									JSONObject jsonObj = null;
									try {
										jsonObj = new JSONObject(result);
									} catch (JSONException e1) {
										e1.printStackTrace();
									}

									if (jsonObj != null) {
										timer_handle = null;

										mViewPager.setCurrentItem(1);
										QueueData queueData = new QueueData(
												null);
										try {
											queueData.branch_id = jsonObj
													.getString("bid");
											queueData.group_id = jsonObj
													.getString("gid");
											queueData.queue_id = jsonObj
													.getString("qid");
											queueData.queue_object_id = jsonObj
													.getString("qoid");
											queueData.queue_remain = jsonObj
													.getInt("qr");
											queueData.reserved_capacity = jsonObj
													.getInt("rc");
											queueData.time_estimate = jsonObj
													.getString("te");
											queueData.time_start = jsonObj
													.getLong("ts");
										} catch (JSONException er) {
											er.printStackTrace();
										}

										QHApp app = (QHApp) getApplication();
										if (app.current_activity != null)
											app.current_activity.finish();

										// jsonObj.get
										if (queueData != null)
											app.model.currentQueueData = queueData;

										if (app.model.myq_data == null)
											app.model.myq_data = new ArrayList<QueueData>();

										app.model.myq_data.add(queueData);

										mViewPager.setCurrentItem(2);
										Intent queue_detail = new Intent(ctx,
												QueueDetailActivity.class);
										startActivity(queue_detail);
									} else {
										if (timer_handle != null)
											timer_handle.postDelayed(
													recheckReserve, 1000);
									}
								} else {

								}
							}
						});
		}
	};

	public void saveMyQInTemp() {
		QHApp app = (QHApp) getApplication();
		JSONArray temps = new JSONArray();

		for (int i = 0; i < app.model.myq_data.size(); i++) {
			temps.put(app.model.myq_data.get(i).getJSONObject());
		}

		String resultsss = temps.toString();

		Editor e = app.getSharedPreferences("temp_q", Context.MODE_PRIVATE)
				.edit();
		e.putString("temp", resultsss);
		e.commit();
	}

	public void showNotificationWithData(JSONObject jsonObj) {
		int showAlert = 1;
		final QHApp app = (QHApp) getApplication();
		String queue_status = "";
		try {
			queue_status = jsonObj.getString("qs");

		} catch (JSONException e) {
			e.printStackTrace();
		}

		saveMyQInTemp();

		if (queue_status.equals(QHApp.QUEUE_STATUS_REJECT)
				&& app.model.request_activity == null)
			return;

		if (current_dialog != null) {
			current_dialog.dismiss();
			current_dialog = null;
		}

		// check internet connection.
		if (!app.getInternetConnection())
			return;

		if (app.model.menuGalleryActivity != null) {
			app.model.menuGalleryActivity.finish();
			app.model.menuGalleryActivity = null;
		}

		app.model.menuGalleryActivity = null;

		if (app.current_activity != null)
			if (app.current_activity.equals(this)) {
				Log.d("QHAppy", "this home page");
			} else {

				Log.d("QHAppy", "this other page");
				app.current_activity.finish();
				app.current_activity = null;
			}

		String msg = "";
		try {
			msg = jsonObj.getString("alert");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Log.d("Qhappy", queue_status);

		if (queue_status.equals("waiting_approve")) {
			String url = "";
			try {
				url = (String) jsonObj.get("url");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			final String approve_url = url;

			AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
			builder.setTitle("QHappy");
			builder.setMessage(msg)
					.setPositiveButton("Approve",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Intent i = new Intent(Intent.ACTION_VIEW);
									i.setData(Uri.parse(approve_url));
									startActivity(i);
								}
							})
					.setNegativeButton("Close",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});

			builder.setCancelable(false);
			builder.create();
			builder.show();

			return;
		} else if (queue_status.equals("promotion")) {
			final PromotionData promotion_data = new PromotionData();
			promotion_data.start_date = new Date();
			try {
				promotion_data.pid = jsonObj.getString("pid");
				promotion_data.bid = jsonObj.getString("bid");
				promotion_data.msg = msg;
			} catch (JSONException e) {
				e.printStackTrace();
			}

			app.model.promotion_lists.add(promotion_data);
			app.model.writePromotion();

			AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
			builder.setTitle("QHappy");
			builder.setMessage(msg)
					.setPositiveButton("ปิด",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									promotion_data.isRead = false;
									app.model.writePromotion();

									app.model.checkUnreadpromotion();
									updateBadge();
								}
							})
					.setNegativeButton("อ่านรายละเอียด",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									promotion_data.isRead = true;
									app.model.writePromotion();

									app.model.checkUnreadpromotion();
									updateBadge();
								}
							});

			builder.setCancelable(false);
			builder.create();
			builder.show();

			return;
		}

		if (queue_status.equals(QHApp.QUEUE_STATUS_ACCEPT)) {

			QueueData queueData = new QueueData(null);

			try {
				queueData.branch_id = jsonObj.getString("bid");
				queueData.group_id = jsonObj.getString("gid");
				queueData.queue_id = jsonObj.getString("qid");
				queueData.queue_object_id = jsonObj.getString("qoid");
				queueData.queue_remain = jsonObj.getInt("qr");
				queueData.reserved_capacity = jsonObj.getInt("rc");
				queueData.time_estimate = jsonObj.getString("te");
				queueData.time_start = jsonObj.getLong("ts");

				Toast.makeText(ctx, "read queue data" + queueData.branch_id
						+ " " + queueData.queue_remain + " "
						+ queueData.group_id + " " + queueData.queue_id,
						Toast.LENGTH_LONG);

			} catch (JSONException e) {

				e.printStackTrace();
				Toast.makeText(ctx, "Receive Queue read object error",
						Toast.LENGTH_SHORT);
			}

			if (app.model.request_reserve_queue_id != null)
				if (queueData != null) {
					// Check received queue is equal reserver queue. if not
					// match ignore it.

					if (app.model.request_reserve_queue_id
							.equals(queueData.queue_object_id)) {
						app.model.currentQueueData = queueData;

						if (app.model.myq_data == null)
							app.model.myq_data = new ArrayList<QueueData>();

						app.model.myq_data.add(queueData);

						mViewPager.setCurrentItem(2);

						saveMyQInTemp();

						if (queueData.queue_object_id != null) {
							app.model
									.removeTempDataWithBrachID(queueData.branch_id);
							app.model
									.removeTempDataWithBrachID(queueData.queue_object_id);
							app.model.saveFile();

							if (app.model.request_reserve_queue_id != null)
								if (!app.model.request_reserve_queue_id
										.equals(queueData.queue_object_id)) {
									Log.d("appCallBack", "Queue mismatch.");
									return;
								}
						}

						app.model.isReserve = false;

						showAlert = 0;

						Intent queue_detail = new Intent(ctx,
								QueueDetailActivity.class);
						queue_detail.putExtra("msg", msg);
						startActivity(queue_detail);
					} else
						return;
				}

		} else if (queue_status.equals(QHApp.QUEUE_STATUS_REJECT)) {

			showAlert = 0;

			if (app.model.request_reserve_queue_id != null) {
				if (app.model.request_activity != null)
					try {
						app.model.request_activity.removeTimer();
					} catch (Exception error) {
						Log.d("QHappy", error.getMessage());
					}

				app.model.request_reserve_queue_id = null;
				app.model.isReserve = false;

				Intent contactShop = new Intent(ctx, ContactShopActivity.class);
				contactShop.putExtra("msg", msg);
				startActivity(contactShop);
			}

		} else if (queue_status.equals(QHApp.QUEUE_STATUS_CALLED)) {
			QueueData queueData = new QueueData(null);
			try {
				queueData = app.model.getQueueDataWithID(jsonObj
						.getString("qoid"));
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (queueData != null) {
				try {

					queueData.current_stage = jsonObj.getString("cs");
					queueData.counter_number = jsonObj.getInt("cn");
					queueData.queue_remain = jsonObj.getInt("qr");
				} catch (Exception error) {
					Log.d("QHappy", "can't get cs or cn or qr in JSON Object.");
				}

				app.model.removeWaitDataWithBrachID(queueData.queue_object_id);

				app.model.currentQueueData = queueData;

				mViewPager.setCurrentItem(2);

				showAlert = 0;

				Intent queue_detail = new Intent(ctx, QueueDetailActivity.class);
				queue_detail.putExtra("msg", msg);
				startActivity(queue_detail);
			} else {
				Log.d("QHappy", "no queue.");
				showAlert = 0;
			}

		} else if (queue_status.equals(QHApp.QUEUE_STATUS_CLAIMED)) {
			QueueData queueData = new QueueData(null);
			showAlert = 0;
			String qoid = "";
			try {
				qoid = jsonObj.getString("qoid");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (!qoid.equals("")) {

				if (mViewPager.getCurrentItem() != 0) {

					mViewPager.setCurrentItem(0);
					mViewPager.setCurrentItem(1);
					mViewPager.setCurrentItem(0);
				}

				queueData = app.model.getQueueDataWithID(qoid);
				if (queueData != null) {
					app.model.currentQueueData = queueData;

					// if(mViewPager.getCurrentItem()!=0)
					// mViewPager.setCurrentItem(0);

					showAlert = 1;

					if (queueData != null) {
						app.model.myq_data.remove(queueData);

						saveMyQInTemp();
					}
				} else if (app.model.currentQueueData != null) {
					if (app.model.currentQueueData.queue_object_id.equals(qoid)) {
						// mViewPager.setCurrentItem(0);
						showAlert = 1;
					} else
						showAlert = 0;
				} else
					showAlert = 0;

				// force load myQ
				Intent interval_intent = new Intent(getApplicationContext(),
						MyQFetchDataBGService.class);
				interval_intent.setData(Uri.parse(ParseInstallation
						.getCurrentInstallation().getString("deviceToken")));

				startService(interval_intent);

			}

			Intent in = new Intent(ctx, EmptyActivity.class);
			startActivity(in);
		} else if (queue_status.equals(QHApp.QUEUE_STATUS_DELETE)) {

			QueueData queueData = new QueueData(null);

			String qoid = "";

			try {
				qoid = jsonObj.getString("qoid");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (!qoid.equals("")) {

				queueData = app.model.getQueueDataWithID(qoid);

				if (queueData != null) {
					WaitQueueData wait_queue_data = app.model
							.getWaitQueueData(qoid);
					if (wait_queue_data != null) {
						wait_queue_data.message = Config.held_message_accept_timeout;
						wait_queue_data.isTimeOut = true;

						app.model.saveFile();
					}

					CancelQueueData cancel_queue_data = new CancelQueueData(
							null);
					cancel_queue_data.readObject(queueData);
					cancel_queue_data.message = Config.door_host_delete_message;

					if (app.model
							.getCancelQueueData(cancel_queue_data.queue_object_id) == null)
						app.model.cancel_queues.add(cancel_queue_data);
					app.model.saveFile();

					if (queueData != null) {
						app.model.myq_data.remove(queueData);
						mViewPager.setCurrentItem(0);

						saveMyQInTemp();
					}
				} else
					showAlert = 0;
			} else
				showAlert = 0;

		} else if (queue_status.equals(QHApp.QUEUE_STATUS_TIMEOUT_DELETE)) {

			QueueData queueData = new QueueData(null);

			String qoid = "";

			try {
				qoid = jsonObj.getString("qoid");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (!qoid.equals("")) {

				queueData = app.model.getQueueDataWithID(qoid);

				WaitQueueData wait_queue_data = app.model
						.getWaitQueueData(qoid);
				if (wait_queue_data != null) {
					wait_queue_data.message = Config.held_message_accept_timeout;
					wait_queue_data.isTimeOut = true;

					app.model.saveFile();
				}

				if (queueData != null) {

					CancelQueueData cancel_queue_data = new CancelQueueData(
							null);
					cancel_queue_data.readObject(queueData);
					cancel_queue_data.message = Config.held_message_accept_timeout_in_row;

					if (app.model
							.getCancelQueueData(cancel_queue_data.queue_object_id) == null)

						app.model.cancel_queues.add(cancel_queue_data);
					app.model.saveFile();

					app.model.myq_data.remove(queueData);
					mViewPager.setCurrentItem(2);

					Intent intent = new Intent(ResponseReceiver.ACTION_FINISH);
					sendBroadcast(intent);

					saveMyQInTemp();
				} else
					showAlert = 0;
			}

			Intent in = new Intent(ctx, EmptyActivity.class);
			startActivity(in);
		} else if (queue_status.equals(QHApp.QUEUE_STATUS_HELD)) {
			showAlert = 0;
			QueueData queueData = new QueueData(null);

			String qoid = "";

			try {
				qoid = jsonObj.getString("qoid");
				msg = jsonObj.getString("alert");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (!qoid.equals("")) {
				queueData = app.model.getQueueDataWithID(qoid);

				if (queueData != null) {
					app.model.currentQueueData = queueData;
					final Activity mContext = ctx;

					final String target_obid = qoid;
					final QueueData target_queue_data = queueData;
					// Dialog

					AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
					builder.setCancelable(false);
					builder.setTitle("QHappy");
					builder.setMessage(msg)
							.setPositiveButton("กดรับบริการ",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											comfirm_wait();

											if (app.model
													.getWaitQueueData(target_obid) == null) {
												// make wait queue data
												WaitQueueData wait_queue_data = new WaitQueueData();
												wait_queue_data.queue_object_id = target_obid;
												wait_queue_data.isTimeOut = false;
												// store in model
												app.model.wait_queues
														.add(wait_queue_data);
												// save file
												app.model.saveFile();
											}

											if (mViewPager.getCurrentItem() != 2)
												mViewPager.setCurrentItem(2);
											else
												app.service.getMyQ();

											current_dialog = null;
										}
									})
							.setNegativeButton("ไม่ใช้บริการแล้ว",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											if (app.model
													.getCancelQueueData(target_queue_data.queue_object_id) == null) {
												CancelQueueData cancel_queue_data = new CancelQueueData(
														null);
												cancel_queue_data
														.readObject(target_queue_data);
												cancel_queue_data.message = Config.held_message_reject;
												if (app.model
														.getCancelQueueData(cancel_queue_data.queue_object_id) == null)
													app.model.cancel_queues
															.add(cancel_queue_data);

												app.model.saveFile();
											}

											ringProgressDialog = ProgressDialog
													.show(mContext,
															"Please Wait..",
															"Loading...");

											HashMap<String, Object> params = new HashMap<String, Object>();
											params.put("bid",
													target_queue_data.branch_id);
											params.put(
													"queue_object_id",
													target_queue_data.queue_object_id);
											params.put("time_stop",
													new Date().getTime() / 1000);

											app.model.myq_data
													.remove(target_queue_data);

											saveMyQInTemp();
											current_dialog = null;

											if (Config.useParseServer == false)
												QHappyService
														.request(
																"cancelQueue",
																params,
																new TextHttpResponseHandler() {

																	@Override
																	public void onSuccess(
																			int arg0,
																			Header[] arg1,
																			String arg2) {

																	}

																	@Override
																	public void onFailure(
																			int arg0,
																			Header[] arg1,
																			String arg2,
																			Throwable arg3) {

																	}

																	@Override
																	public void onFinish() {
																		dismissProgressDialog();

																		app.service
																				.getMyQ();
																		super.onFinish();
																	}
																});
											else
												ParseCloud
														.callFunctionInBackground(
																"cancelQueue",
																params,
																new FunctionCallback<String>() {
																	public void done(
																			String result,
																			ParseException e) {
																		if (e == null) {

																		} else {

																		}
																		dismissProgressDialog();

																		app.service
																				.getMyQ();

																	}
																});

										}
									});

					current_dialog = builder.create();
					current_dialog.show();
				}

			}

			Intent in = new Intent(ctx, EmptyActivity.class);
			startActivity(in);

		} else if (queue_status.equals(QHApp.QUEUE_STATUS_UPDATE_STATUS)) {
			showAlert = 0;
			QueueData queueData = new QueueData(null);
			String qoid = "";

			try {
				qoid = jsonObj.getString("qoid");
				msg = jsonObj.getString("alert");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (!qoid.equals("")) {
				queueData = app.model.getQueueDataWithID(qoid);
				if (queueData != null) {
					try {
						queueData.queue_remain = jsonObj.getInt("qr");
						queueData.current_stage = jsonObj.getString("cs");
						queueData.counter_number = jsonObj.getInt("cn");

					} catch (Exception error) {

					}

					app.model.removeTempDataWithBrachID(qoid);
					app.model.saveFile();

					app.model.currentQueueData = queueData;

					if (msg.equals("")) {
						mViewPager.setCurrentItem(2);
						Intent queue_detail = new Intent(ctx,
								QueueDetailActivity.class);
						startActivity(queue_detail);

						current_dialog = null;
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								ctx);
						builder.setTitle("QHappy");
						builder.setMessage(msg).setPositiveButton("ตกลง",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										mViewPager.setCurrentItem(2);
										Intent queue_detail = new Intent(ctx,
												QueueDetailActivity.class);
										startActivity(queue_detail);

										current_dialog = null;
									}
								});

						current_dialog = builder.create();
						current_dialog.show();
					}
				}
			}

		} else if (queue_status.equals(QHApp.QUEUE_STATUS_QUEUED)) {
			showAlert = 0;
			QueueData queueData = new QueueData(null);
			String qoid = "";

			try {
				qoid = jsonObj.getString("qoid");
				msg = jsonObj.getString("alert");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (!qoid.equals("")) {
				queueData = app.model.getQueueDataWithID(qoid);
				if (queueData != null) {
					try {
						queueData.counter_number = jsonObj.getInt("cn");
						queueData.current_stage = jsonObj.getString("cs");
					} catch (Exception error) {

					}

					app.model.removeTempDataWithBrachID(queueData.branch_id);
					app.model
							.removeWaitDataWithBrachID(queueData.queue_object_id);

					app.model.currentQueueData = queueData;

					Intent intent = new Intent(ResponseReceiver.ACTION_FINISH);
					sendBroadcast(intent);

					AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
					builder.setTitle("QHappy");
					builder.setMessage(msg).setPositiveButton("ตกลง",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									mViewPager.setCurrentItem(2);
									Intent queue_detail = new Intent(ctx,
											QueueDetailActivity.class);
									startActivity(queue_detail);

									current_dialog = null;
								}
							});

					current_dialog = builder.create();
					current_dialog.show();
				}
			}
			Intent in = new Intent(ctx, EmptyActivity.class);
			startActivity(in);
		} else if (queue_status.equals(QHApp.QUEUE_STATUS_UPDATE)) {
			String bid = "";
			String gid = "";
			String qoid = "";
			int qr = 1;

			try {
				bid = jsonObj.getString("bid");
				gid = jsonObj.getString("gid");
				qr = jsonObj.getInt("qr");
				qoid = jsonObj.getString("qoid");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			QueueData queueData = new QueueData(null);
			queueData = app.model.getQueueDataWithID(qoid);

			if (queueData != null) {
				queueData.queue_remain = qr;
				app.model.currentQueueData = queueData;
			} else if (app.model.currentQueueData != null) {
				app.model.currentQueueData.queue_remain = qr;
			}

			mViewPager.setCurrentItem(2);

			Intent queue_detail = new Intent(ctx, QueueDetailActivity.class);
			queue_detail.putExtra("msg", msg);
			startActivity(queue_detail);

		}

		if (showAlert == 1) {
			if (!msg.equals("")) {
				// Inserting delay here
				Log.d("QHappy", "show alert");
				this.showAlert(msg);

			}

		}
	}

	private void continuesCallNotification() {
		if (notification_lists.size() > 0) {
			String notification_string = notification_lists.get(0);
			// Remove notification when receive data instead.
			notification_lists.remove(0);
			JSONObject json = null;

			try {
				json = new JSONObject(notification_string);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (json != null)
				appCallBack(json);
		}
	}

	private void keepInQueue(String notification_string) {
		notification_lists.add(notification_string);
	}

	Boolean is_notification_free = true;
	ArrayList<String> notification_lists = new ArrayList<String>();

	@Override
	public void appCallBack(JSONObject jsonObj) {

		if (!is_notification_free) {
			keepInQueue(jsonObj.toString());
			return;
		}

		Log.d("QHappy", "appCallBack " + jsonObj.toString());

		final JSONObject json_temp = jsonObj;

		// Check is new receive notification id
		String nid = "";
		String bid = "";
		String qs = "";
		String pos = "";
		int cn = 0;
		int qr = 0;
		String cs = "";

		try {
			nid = jsonObj.getString("nid");
			bid = jsonObj.getString("bid");
			qs = jsonObj.getString("qs");
			cs = jsonObj.getString("cs");
			cn = jsonObj.getInt("cn");
			qr = jsonObj.getInt("qr");

		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		try {
			pos = jsonObj.getString("pos");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		// check repate notification
		// try {
		// long noti_time = jsonObj.getLong("time");
		// SharedPreferences share = this.getSharedPreferences("noti_time",
		// Context.MODE_PRIVATE);
		// long old_time = share.getLong("noti_time", 0);
		//
		// if (noti_time <= old_time) {
		// Log.d("QHappy", "older notification.");
		// return;
		// }
		//
		// Editor edit = share.edit();
		// edit.putLong("noti_time", noti_time);
		// edit.commit();
		//
		// } catch (JSONException e1) {
		// e1.printStackTrace();
		// }

		if (!nid.equals("")) {
			final HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("nid", nid);
			params.put("bid", bid);
			params.put("qr", qr);
			params.put("qs", qs);
			params.put("cn", cn);
			params.put("cs", cs);

			if (qs.equals(QHApp.QUEUE_STATUS_ACCEPT)
					|| qs.equals(QHApp.QUEUE_STATUS_REJECT)) {
				Intent intent = new Intent("net.pirsquare.accepted");
				sendBroadcast(intent);
			}

			if (qs.equals("accepted")) {

				// Check reserve queue already accepted yet.
				QHApp app = (QHApp) getApplication();
				if (app.model.request_reserve_queue_id == null)
					return;
			}

			try {
				String type_push = jsonObj.getString("type");
				if (type_push.equals("manual") && isTempNotification == false)
					alert();

				isTempNotification = false;
			} catch (Exception e) {
			}

			// change state
			is_notification_free = false;

			if (!pos.equals(""))
				params.put("pos", pos);

			ring_progress_dialog = ProgressDialog.show(this, "Please Wait..",
					"Loading...");
			Log.d("QHappy", "Get notificationDataWithNID " + params.toString());

			if (Config.useParseServer == false) {
				QHappyService.request(
						QHappyService.REQUEST_ACTION_GET_NOTIFICATION_WITH_ID,
						params, new TextHttpResponseHandler() {

							@Override
							public void onSuccess(int arg0, Header[] arg1,
									String arg2) {
								JSONObject jsonData = null;

								Log.d("QHappy",
										"get notification with id result :"
												+ arg2);
								try {
									jsonData = new JSONObject(arg2);
								} catch (JSONException e) {
									e.printStackTrace();
								}

								if (jsonData != null)
									showNotificationWithData(jsonData);
								else {
									Log.d("QHappy",
											"request notification error "
													+ arg2.toString());
								}
							}

							@Override
							public void onFailure(int arg0, Header[] arg1,
									String arg2, Throwable arg3) {

								JSONObject temp;
								try {
									temp = new JSONObject(json_temp.toString());
									temp.put("time", System.currentTimeMillis());

									// Retry call notification in queue.
									notification_lists.add(0,
											json_temp.toString());
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}

							@Override
							public void onFinish() {
								if (ring_progress_dialog != null) {
									try {
										ring_progress_dialog.dismiss();
									} catch (Error e) {
										Log.d("QHappy",
												"can't dismiss ringg progress");
									}
									ring_progress_dialog = null;
								}

								is_notification_free = true;

								continuesCallNotification();
								super.onFinish();
							}
						});
			} else {

				ParseCloud.callFunctionInBackground(
						"getNotificationdataWithID", params,
						new FunctionCallback<String>() {
							@Override
							public void done(String arg0, ParseException arg1) {

								if (ring_progress_dialog != null) {
									try {
										ring_progress_dialog.dismiss();
									} catch (Error e) {
										Log.d("QHappy",
												"can't dismiss ringg progress");
									}
									ring_progress_dialog = null;
								}

								if (arg1 == null) {

									JSONObject jsonData = null;
									try {
										jsonData = new JSONObject(arg0);
									} catch (JSONException e) {
										e.printStackTrace();
									}

									if (jsonData != null)
										showNotificationWithData(jsonData);
									else {
										Log.d("QHappy",
												"request notification error "
														+ arg0.toString());
									}
								} else {

									Log.d("QHappy",
											"Error getNotificationdataWithID "
													+ arg1.toString());
									// Retry call notification in queue.
									notification_lists.add(0,
											json_temp.toString());
									Log.d("QHappy",
											"Retry get notificationWithID in queue "
													+ json_temp.toString());
								}

								is_notification_free = true;

								continuesCallNotification();
							}
						});
			}
			return;
		} else {
			// enter old check
			showNotificationWithData(jsonObj);
			is_notification_free = true;
		}
	}

	ProgressDialog ring_progress_dialog;
	AlertDialog current_dialog;

	public void showAlert(String msg) {
		if (msg == null)
			return;

		QHApp app = (QHApp) getApplication();

		AlertDialog.Builder builder;
		builder = new AlertDialog.Builder(ctx);
		builder.setTitle("QHappy");
		builder.setMessage(msg).setPositiveButton("ตกลง",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						current_dialog = null;
					}
				});

		current_dialog = builder.create();
		current_dialog.show();

	}

	public void comfirm_wait() {
		QHApp app = (QHApp) getApplication();

		ringProgressDialog = ProgressDialog.show(ctx, "Please Wait..",
				"Loading...");

		final Activity ctx = this;

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid", app.model.currentQueueData.branch_id);
		params.put("qoid", app.model.currentQueueData.queue_object_id);

		if (Config.useParseServer == false)
			QHappyService.request("confirmWait", params,
					new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {

						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {

						}

						@Override
						public void onFinish() {
							dismissProgressDialog();

							mViewPager.setCurrentItem(2);

							Intent queue_detail = new Intent(ctx,
									QueueDetailActivity.class);
							startActivity(queue_detail);
							super.onFinish();
						}
					});
		else
			ParseCloud.callFunctionInBackground("confirmWait", params,
					new FunctionCallback<String>() {
						public void done(String result, ParseException e) {
							if (e == null) {
							} else {
							}
							dismissProgressDialog();

							mViewPager.setCurrentItem(2);

							Intent queue_detail = new Intent(ctx,
									QueueDetailActivity.class);
							startActivity(queue_detail);

						}
					});

	}

	Intent scanQRActivity = null;

	public void tapScan(View view) {
		// check internet connection.
		QHApp app = (QHApp) getApplication();
		if (!app.getInternetConnection())
			return;

		if (scanQRActivity != null)
			return;

		scanQRActivity = new Intent(ctx, ScanQRActivity.class);
		startActivity(scanQRActivity);
	}

	@Override
	protected void onResume() {

		QHApp app = (QHApp) getApplication();

		if (app.model.detatcQRCode != "") {
			checkQRCode(app.model.detatcQRCode);
			app.model.detatcQRCode = "";
		}

		scanQRActivity = null;

		super.onResume();
	}

	private void callRequestActivity() {
		Intent requestIntent = new Intent(ctx, RequestActivity.class);
		startActivity(requestIntent);
	}

	public void checkQRCode(String code) {
		ringProgressDialog = ProgressDialog.show(ctx, "Please Wait..",
				"Loading...");

		final String branch_id = code;

		int result = code.indexOf("http://q-happy.com/?q");

		if (result != -1) {
			final QHApp app = (QHApp) getApplication();
			// get code
			String code_base_64 = code.split("q=")[1];
			String decode = new String(Base64.decode(code_base_64,
					Base64.NO_PADDING | Base64.NO_WRAP)).trim();
			// decript code
			String[] codes = decode.split("\\|");

			String branch_id_string = codes[3];
			// check my q

			QueueData checkReserveQueue = null;

			for (QueueData queue : app.model.myq_data) {

				if (queue.branch_id.equals(branch_id_string)) {
					checkReserveQueue = queue;
					break;
				}
			}

			if (checkReserveQueue == null) {
				String current_stage = "order";
				if (codes[0].equals("B"))
					current_stage = "complain";

				if (Config.current_stage_status_value != null) {
					try {
						current_stage = Config.current_stage_status_value
								.getString(codes[0]);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				final QueueData queue_data = new QueueData(null);
				queue_data.branch_id = codes[3];
				queue_data.group_id = codes[0];
				queue_data.queue_id = codes[1];
				// ignore queue remain use -1 instead queue remain for show
				queue_data.queue_remain = -1;// Integer.parseInt(codes[4]);
				queue_data.current_stage = "";
				queue_data.counter_number = 0;
				queue_data.time_start = Long.parseLong(codes[2]);

				app.model.currentQueueData = queue_data;

				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("current_stage", current_stage);
				params.put("device_type", "android");
				params.put("device_token", ParseInstallation
						.getCurrentInstallation().getString("deviceToken"));
				params.put("branch_id", codes[3]);
				params.put("queue_id", Integer.parseInt(codes[1]));
				params.put("group_id", codes[0]);
				params.put("time_start", Long.parseLong(codes[2]));
				params.put("queue_remain", Integer.parseInt(codes[4]));

				final Activity ctx = this;
				final Runnable timeout_accept_queue = new Runnable() {

					@Override
					public void run() {
						Intent contactShop = new Intent(ctx,
								ContactShopActivity.class);
						startActivity(contactShop);
					}
				};

				final Handler timeout_handle = new Handler();
				timeout_handle.postDelayed(timeout_accept_queue, 5000);

				// check active server.
				if (Config.useParseServer == false) {
					// Request to www.qhappy.com
					QHappyService.request(
							QHappyService.REQUEST_ACTION_ACCEPT_QUEUE, params,
							new TextHttpResponseHandler() {

								@Override
								public void onSuccess(int arg0, Header[] arg1,
										String arg2) {

									if (arg2.equals("error")) {
										app.model.myq_data.remove(queue_data);
										app.model.currentQueueData = null;
										app.current_activity.finish();

										mViewPager.setCurrentItem(0);

										AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
												ctx);
										// set title
										alertDialogBuilder.setTitle("QHappy");
										// set dialog message
										alertDialogBuilder
												.setMessage(
														"คิวนี้หมดอายุแล้ว ค่ะ กรุณาขอคิวใหม่อีกครั้ง")
												.setCancelable(false)
												.setPositiveButton(
														"ปิด",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {
															}
														});

										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder
												.create();

										// show it
										alertDialog.show();

									} else if (arg2.equals("canceled")) {
										app.model.myq_data.remove(queue_data);
										app.model.currentQueueData = null;
										app.current_activity.finish();

										mViewPager.setCurrentItem(0);

										AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
												ctx);
										// set title
										alertDialogBuilder.setTitle("QHappy");
										// set dialog message
										alertDialogBuilder
												.setMessage(
														"คุณได้กดยกเลิกคิวนี้ไปก่อนแล้ว")
												.setCancelable(false)
												.setPositiveButton(
														"ปิด",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {
															}
														});

										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder
												.create();

										// show it
										alertDialog.show();

									} else {

										if (arg2.indexOf('"') != -1) {
											arg2 = arg2.substring(1,
													arg2.length() - 1);
										}

										queue_data.queue_object_id = arg2;

										app.model.myq_data.add(queue_data);

										// start background service.
										Intent stop_intent = new Intent(
												"net.pirsquare.service_order");
										stop_intent
												.addCategory("net.pirsquare.qhappy");
										stop_intent.putExtra("start", true);
										sendBroadcast(stop_intent);

										Intent queue_detail = new Intent(ctx,
												QueueDetailActivity.class);
										startActivity(queue_detail);

									}
								}

								@Override
								public void onFailure(int arg0, Header[] arg1,
										String arg2, Throwable arg3) {
									AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
											ctx);
									// set title
									alertDialogBuilder.setTitle("QHappy");
									// set dialog message
									alertDialogBuilder
											.setMessage(
													"Server time out please try again later.")
											.setCancelable(false)
											.setPositiveButton(
													"ปิด",
													new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int id) {
														}
													});

									// create alert dialog
									AlertDialog alertDialog = alertDialogBuilder
											.create();

									// show it
									alertDialog.show();
								}

								@Override
								public void onFinish() {
									// cancle timeout delay.
									timeout_handle
											.removeCallbacks(timeout_accept_queue);
									// dispose progress
									dismissProgressDialog();
									super.onFinish();
								}
							});

				} else {
					// TODO change
					ParseCloud.callFunctionInBackground("acceptQueue", params,
							new FunctionCallback<String>() {
								public void done(String arg0,
										ParseException arg1) {

									// cancle timeout delay.
									timeout_handle
											.removeCallbacks(timeout_accept_queue);
									// dispose progress
									dismissProgressDialog();

									if (arg1 == null) {
										if (arg0.equals("error")) {
											app.model.myq_data
													.remove(queue_data);
											app.model.currentQueueData = null;
											app.current_activity.finish();

											mViewPager.setCurrentItem(0);

											AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													ctx);
											// set title
											alertDialogBuilder
													.setTitle("QHappy");
											// set dialog message
											alertDialogBuilder
													.setMessage(
															"คิวนี้หมดอายุแล้ว ค่ะ กรุณาขอคิวใหม่อีกครั้ง")
													.setCancelable(false)
													.setPositiveButton(
															"ปิด",
															new DialogInterface.OnClickListener() {
																public void onClick(
																		DialogInterface dialog,
																		int id) {
																	// if this
																	// button is
																	// clicked,
																	// close
																	// current
																	// activity
																}
															});

											// create alert dialog
											AlertDialog alertDialog = alertDialogBuilder
													.create();

											// show it
											alertDialog.show();

										} else {
											queue_data.queue_object_id = arg0;
											app.model.myq_data.add(queue_data);

											Intent queue_detail = new Intent(
													ctx,
													QueueDetailActivity.class);
											startActivity(queue_detail);
										}
										// acceptQueue error from parse.
									} else {
										AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
												ctx);
										// set title
										alertDialogBuilder.setTitle("QHappy");
										// set dialog message
										alertDialogBuilder
												.setMessage(
														"Server time out please try again later.")
												.setCancelable(false)
												.setPositiveButton(
														"ปิด",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {
																// if this
																// button is
																// clicked,
																// close
																// current
																// activity
															}
														});

										// create alert dialog
										AlertDialog alertDialog = alertDialogBuilder
												.create();

										// show it
										alertDialog.show();
									}
								};
							});
				}

			} else {

				// dispose progress
				dismissProgressDialog();

				app.model.currentQueueData = checkReserveQueue;

				Intent queue_detail = new Intent(ctx, QueueDetailActivity.class);
				startActivity(queue_detail);
			}

		} else {

			final Activity ctx = this;

			if (Config.useParseServer == false && false) {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("branch_id", code);
				QHappyService.request(
						QHappyService.REQUEST_ACTION_GET_BRANCH_WITH_ID,
						params, new TextHttpResponseHandler() {

							@Override
							public void onSuccess(int arg0, Header[] arg1,
									String arg2) {
								if (arg2.equals("")) {
									AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
											ctx);
									// set title
									alertDialogBuilder.setTitle("QHappy");
									// set dialog message
									alertDialogBuilder
											.setMessage(
													"Code mismatch please try again later.")
											.setCancelable(false)
											.setPositiveButton(
													"ปิด",
													new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int id) {
														}
													});

									// create alert dialog
									AlertDialog alertDialog = alertDialogBuilder
											.create();

									// show it
									alertDialog.show();

								} else {

									JSONObject json = null;
									try {
										json = new JSONObject(arg2);
									} catch (JSONException e) {
										e.printStackTrace();
									}

									QHApp app = (QHApp) getApplication();

									QueueData checkReserveQueue = null;

									for (QueueData queue : app.model.myq_data) {

										if (queue.branch_id.equals(branch_id)) {
											checkReserveQueue = queue;
											break;
										}
									}

									if (checkReserveQueue == null) {

										BranchListData b = new BranchListData(
												null);
										b.setJson(json);
										app.model.branch_detact = b;

										callRequestActivity();
									} else {
										app.model.currentQueueData = checkReserveQueue;

										Intent queue_detail = new Intent(ctx,
												QueueDetailActivity.class);
										startActivity(queue_detail);
									}
								}
							}

							@Override
							public void onFailure(int arg0, Header[] arg1,
									String arg2, Throwable arg3) {

							}

							@Override
							public void onFinish() {
								dismissProgressDialog();
								super.onFinish();
							}
						});
			} else {
				ParseQuery<ParseObject> query = ParseQuery
						.getQuery("t_branch_metadata");
				query.whereEqualTo("branch_id", code);

				query.findInBackground(new FindCallback<ParseObject>() {
					public void done(List<ParseObject> branchList,
							ParseException e) {
						if (e == null) {

							if (branchList.size() == 0) {
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
										ctx);
								// set title
								alertDialogBuilder.setTitle("QHappy");
								// set dialog message
								alertDialogBuilder
										.setMessage(
												"Code mismatch please try again later.")
										.setCancelable(false)
										.setPositiveButton(
												"ปิด",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// if this button is
														// clicked, close
														// current activity
													}
												});

								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder
										.create();

								// show it
								alertDialog.show();

							} else {
								ParseObject branch_data = branchList.get(0);

								QHApp app = (QHApp) getApplication();

								QueueData checkReserveQueue = null;

								for (QueueData queue : app.model.myq_data) {

									if (queue.branch_id.equals(branch_id)
											&& queue.claim_time == 0) {
										checkReserveQueue = queue;
										break;
									}
								}

								if (checkReserveQueue == null) {
									app.model.branch_detact = new BranchListData(
											branch_data);

									callRequestActivity();
								} else {
									app.model.currentQueueData = checkReserveQueue;

									Intent queue_detail = new Intent(ctx,
											QueueDetailActivity.class);
									startActivity(queue_detail);
								}
							}

						} else {
							Log.d("score", "Error: " + e.getMessage());
						}
						// dispose progress
						dismissProgressDialog();
					}
				});
			}
		}

	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {

			switch (position) {
			case 0:
				return new StandbyFragment();
			case 1:
				return new ListFragment();
			case 2:
				return new MyQFragment();
			case 3:
				return new FavoriteFragment();
			case 4:
				return new SettingFragment();
			}
			return null;
		}

		@Override
		public int getCount() {
			return 4;
		}

		public int getIcon(int position) {
			int icon;
			switch (position) {
			case 0:
				icon = R.drawable.ic_tab_scan;
				break;
			case 1:
				icon = R.drawable.ic_tab_list;
				break;
			case 2:
				icon = R.drawable.ic_tab_myq;
				break;
			case 3:
				icon = R.drawable.ic_tab_fav;
				break;
			default:
				icon = R.drawable.nav_my_q_icon;
			}

			return icon;

		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return getString(R.string.title_section4).toUpperCase(l);
			case 4:
				return getString(R.string.title_section5).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_home, container,
					false);

			ImageView imageView = (ImageView) rootView
					.findViewById(R.id.imageView1);
			// TextView textView = (TextView) rootView
			// .findViewById(R.id.section_label);
			// textView.setText(Integer.toString(getArguments().getInt(
			// ARG_SECTION_NUMBER)));
			return rootView;
		}
	}

	@Override
	public void callBackGetMyQDataQueue(List<QueueData> result) {

	}

}
