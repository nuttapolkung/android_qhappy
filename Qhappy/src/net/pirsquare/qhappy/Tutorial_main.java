package net.pirsquare.qhappy;

import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Tutorial_main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial_main);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		final Activity ctx = this;
		
		findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ctx.finish();
			}
		});
	}
}
