package net.pirsquare.qhappy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ContactShopActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_contact_shop);
		
		this.setTheme(R.style.AppBaseTheme);
		
		QHApp app = (QHApp)getApplication();
		Button ok_button  = (Button)findViewById(R.id.contact_shop_ok_btn);
		ok_button.setTypeface(app.model.DB_OZONEX_BOLD);
		TextView msg_text  = (TextView)findViewById(R.id.textView1);
		msg_text.setTypeface(app.model.DB_OZONEX_BOLD);
		
		 app.current_activity = this;
		 
		 String msg = this.getIntent().getStringExtra("msg");
		 
		 if(msg!=null)
		 if(msg.equals("") == false)
		 {
			 AlertDialog.Builder builder;
			builder = new AlertDialog.Builder(this);
			builder.setTitle("QHappy");
			builder.setMessage(msg).setPositiveButton("ตกลง",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							
						}
					});

			 builder.create();
			 builder.show();
			 
		 }
		 
		
	}
	
	public void onClickContatcShopOk(View v)
	{
		this.finish();
	}
}
