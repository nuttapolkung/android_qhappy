package net.pirsquare.qhappy.dataAdepter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;

import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.R;
import net.pirsquare.qhappy.model.BrandData;
import net.pirsquare.qhappy.util.ImageLoader;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseImageView;

public class BrandAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ImageLoader imageLoader;
	QHApp app;
	private LinkedList<BrandData> brandList = null;
	private ArrayList<BrandData> arraylist;

	public BrandAdapter(Context context, QHApp app, LinkedList<BrandData> brand) {
		this.context = context;
		this.app = (QHApp) app;
		this.brandList = brand;
		inflater = LayoutInflater.from(context);
		this.arraylist = new ArrayList<BrandData>();
		this.arraylist.addAll(brand);
		imageLoader = new ImageLoader(context);
		// super(context, R.layout.list_row, brand);
	}

	public class ViewHolder {
		TextView name;
		TextView info;
		ParseImageView logo;
		TextView distance;
	}

	@Override
	public int getCount() {
		return brandList.size();
	}

	@Override
	public Object getItem(int position) {
		return brandList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.list_row, null);
			// Locate the TextViews in list_row.xml
			holder.name = (TextView) convertView.findViewById(R.id.brandName);
			holder.info = (TextView) convertView.findViewById(R.id.brandInfo);
			// Locate the ImageView in list_row.xml
			holder.logo = (ParseImageView) convertView
					.findViewById(R.id.brandImage);
			holder.distance = (TextView) convertView
					.findViewById(R.id.textView1);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		// Set the results into TextViews
		holder.name.setText(brandList.get(position).getBrandName());
		holder.info.setText(brandList.get(position).getBrandInfo());
		// Set the results into ImageView
		holder.logo.setParseFile(brandList.get(position).image_file);
		holder.logo.loadInBackground();

		if (brandList.get(position).distance != 0.0) {

			Double value_doble = brandList.get(position).distance * 1000;
			NumberFormat formatter = new DecimalFormat("####");
			String f = formatter.format(Math.floor(value_doble));

			holder.distance.setText(f + " m.");
		}
		// imageLoader.DisplayImage(brandList.get(position).getBrandLogo(),
		// holder.logo);
		// Listen for ListView Item Click

		holder.name.setTypeface(app.model.DB_OZONEX_BOLD);
		holder.info.setTypeface(app.model.DB_OZONEX);

		return convertView;
	}

	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
}
