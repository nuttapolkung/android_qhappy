package net.pirsquare.qhappy.dataAdepter;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.R;
import net.pirsquare.qhappy.dataAdepter.BranchAdapter.ViewHolder;
import net.pirsquare.qhappy.model.PromotionData;
import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PromotionAdepter extends BaseAdapter {

	private ArrayList<PromotionData> mPromotionList;
	  LayoutInflater inflater;
	  QHApp app;
	  List<ParseObject> brand_object;
	  
	
	public PromotionAdepter(Context context,QHApp _app,ArrayList<PromotionData> promotion_data)
	{
		app = _app;
		mPromotionList = promotion_data;
		inflater = LayoutInflater.from(context);
		
		brand_object = new ArrayList<ParseObject>();
	}
	
	public void setData(ArrayList<PromotionData> promotion_data)
	{
		mPromotionList = promotion_data;
	}
	
	@Override
	public int getCount() {
		
		return mPromotionList.size();
	}

	@Override
	public Object getItem(int index) {
		return mPromotionList.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public class ViewHolder {
        TextView title;
        TextView dsc;
        ParseImageView brandImage;
        TextView date;
    }

	private ParseObject getBrandObjectWithID(String id)
	{
		ParseObject returnObject = null;
		for (ParseObject brand : brand_object) {
			if(brand.getObjectId().equals(id))
			{
				returnObject = brand;
				break;
			}
		}
		
		return returnObject;
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.promotion_row, null);
//            Locate the TextViews in list_row.xml
            holder.title = (TextView) convertView.findViewById(R.id.title_text);
            holder.dsc = (TextView) convertView.findViewById(R.id.dsc_text);
            holder.date = (TextView) convertView.findViewById(R.id.date_text);
            holder.brandImage = (ParseImageView)convertView.findViewById(R.id.imageView1);
            
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
		
		PromotionData promotion_data = mPromotionList.get(position);
		
		ParseObject brand_data = this.getBrandObjectWithID(promotion_data.bid);
		
		if(brand_data == null)
		{
		ParseQuery<ParseObject> query = ParseQuery.getQuery("t_brand_metadata");
		query.whereEqualTo("objectId", promotion_data.bid);
		
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> results, ParseException e) {
		        if (e == null) {
		        	
		        	ParseObject brand_data = results.get(0);
		        	
		        	 holder.title.setText((String)brand_data.get("brand_name"));
		        	 holder.brandImage.setParseFile((ParseFile)brand_data.get("brand_image"));
		        	 holder.brandImage.loadInBackground();
		        	 
		        	 brand_object.add(brand_data);
		        } else {
		           
		        }
		    }
		});
		}else{
			 holder.title.setText((String)brand_data.get("brand_name"));
        	 holder.brandImage.setParseFile((ParseFile)brand_data.get("brand_image"));
        	 holder.brandImage.loadInBackground();
		}
		
		holder.date.setText(DateUtils.getRelativeTimeSpanString(promotion_data.start_date.getTime()));
		
		app.model.getBranchDataWithID(promotion_data.bid);
		
		holder.dsc.setText(promotion_data.msg);
		
        holder.dsc.setTypeface(app.model.DB_OZONEX);
        holder.date.setTypeface(app.model.DB_OZONEX);
        holder.title.setTypeface(app.model.DB_OZONEX);
        return convertView;
	}
	

}
