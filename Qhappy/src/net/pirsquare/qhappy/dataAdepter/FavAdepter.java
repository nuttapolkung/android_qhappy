package net.pirsquare.qhappy.dataAdepter;

import java.util.ArrayList;

import net.pirsquare.qhappy.FavAdepterRemoveFav;
import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.R;
import net.pirsquare.qhappy.model.BranchListData;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;


public class FavAdepter extends BaseAdapter {

	private ArrayList<BranchListData> data;
	private Activity activity;
	private FavAdepterRemoveFav listener;
	
	public FavAdepter(Activity activity, ArrayList<BranchListData> data,FavAdepterRemoveFav listener)
	{
		this.activity = activity;
		this.data = data;
		this.listener = listener;
	}
	
	
	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		
		return this.data.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		 if(convertView==null)
         {
             LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             convertView = inflater.inflate(R.layout.fav_row_detail, parent,false);
         }
		 
		 
		 BranchListData branchData = (BranchListData)this.data.get(position);
		 
		 ParseImageView bandImage = (ParseImageView)convertView.findViewById(R.id.imageView1);
		 
        final  TextView band_name = (TextView)convertView.findViewById(R.id.textView1);
         TextView branch_name = (TextView)convertView.findViewById(R.id.textView2);
         TextView other_info = (TextView)convertView.findViewById(R.id.textView3);
       
         QHApp app = (QHApp) this.activity.getApplication();
         band_name.setTypeface(app.model.DB_OZONEX_BOLD);
         branch_name.setTypeface(app.model.DB_OZONEX);
         other_info.setTypeface(app.model.DB_OZONEX);
         
         ImageButton fav_btn = (ImageButton)convertView.findViewById(R.id.imageButton1);
         fav_btn.setTag(branchData.branch_id());
         fav_btn.setOnClickListener(new OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				Log.d("QHappy", "Click Fav"+v.getTag());
 				if(listener!=null)
 					listener.removeFav((String)v.getTag());
 			}

 		});

        
         
         final  ParseObject bandData = (ParseObject)branchData.brand();
         if(bandData.isDataAvailable())
         {
        	 band_name.setText(bandData.getString("brand_name")); 
         }else{
        	 bandData.fetchInBackground(new GetCallback<ParseObject>() {
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							 band_name.setText(bandData.getString("brand_name")); 
						} else {
						}
					}
				});
         }
         
         bandImage.setParseFile(branchData.branch_image_file());
         bandImage.loadInBackground();
         
         branch_name.setText(branchData.branch_name());
         other_info.setText(branchData.other_info());
         
		return convertView;
	}

}
