package net.pirsquare.qhappy.dataAdepter;

import java.util.ArrayList;
import java.util.LinkedList;

import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.R;
import net.pirsquare.qhappy.dataAdepter.BrandAdapter.ViewHolder;
import net.pirsquare.qhappy.model.BranchData;
import net.pirsquare.qhappy.model.BrandData;
import net.pirsquare.qhappy.util.ImageLoader;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BranchAdapter extends BaseAdapter {
	// Declare Variables
    Context context;
    QHApp app;
    LayoutInflater inflater;
    ImageLoader imageLoader;
    private LinkedList<BranchData> branchList = null;
    private ArrayList<BranchData> arraylist;
    private BrandData brand;
    
	public BranchAdapter(Context context,QHApp app,BrandData brand, LinkedList<BranchData> branch) {
		this.context = context;
		this.app = (QHApp)app;
        this.branchList = branch;
        this.brand = brand;
        inflater = LayoutInflater.from(context);
        this.arraylist = new ArrayList<BranchData>();
        this.arraylist.addAll(branch);
        imageLoader = new ImageLoader(context);
	    }
	
	public class ViewHolder {
        TextView locate;
        TextView time;
        TextView info;
    }
 
    @Override
    public int getCount() {
        return branchList.size();
    }
 
    @Override
    public Object getItem(int position) {
        return branchList.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    public void setData(LinkedList<BranchData> newData,BrandData brand) {
    	  this.branchList = newData;
    	  this.brand = brand;
    }

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.place_row, null);
            // Locate the TextViews in list_row.xml
            holder.locate = (TextView) convertView.findViewById(R.id.branchplace);
//            holder.time = (TextView) convertView.findViewById(R.id.waittime);
            holder.info = (TextView) convertView.findViewById(R.id.branchInfo);
            // Locate the ImageView in list_row.xml
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
		// Set the results into TextViews
        holder.locate.setText(branchList.get(position).branch_name());
//        holder.time.setText(branchList.get(position).time_estimate()+" H.");
        // Set the results into ImageView
        // Listen for ListView Item Click
        holder.info.setText(branchList.get(position).other_info());
        holder.info.setTypeface(app.model.DB_OZONEX);
        holder.locate.setTypeface(app.model.DB_OZONEX);
        
        return convertView;
	}
	
	
	
}
