package net.pirsquare.qhappy.dataAdepter;

import java.util.ArrayList;

import net.pirsquare.qhappy.Config;
import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.R;
import net.pirsquare.qhappy.model.BranchListData;
import net.pirsquare.qhappy.model.CancelQueueData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.model.WaitQueueData;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;

public class MyQAdepter extends BaseAdapter {

	private ArrayList<QueueData> data;
	private Activity activity;
	private JSONObject kios_status;
	private ArrayList<String> status_list;

	public MyQAdepter(Activity activity, ArrayList<QueueData> data,
			JSONObject status) {
		this.kios_status = status;
		this.activity = activity;
		this.data = data;

		status_list = new ArrayList<String>();
	}

	public Boolean kiosIsOffline(int pos) {
		return status_list.get(pos).equals("offline");
	}

	@Override
	public int getCount() {
		return this.data.size();
	}

	@Override
	public Object getItem(int position) {

		if (this.data.size() == 0)
			return null;
		else
			return this.data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.myq_row_detail, parent,
					false);
		}

		QueueData myQData = (QueueData) this.data.get(position);

		ParseImageView bandImage = (ParseImageView) convertView
				.findViewById(R.id.imageView1);

		final TextView band_name = (TextView) convertView
				.findViewById(R.id.textView1);
		TextView branch_name = (TextView) convertView
				.findViewById(R.id.textView2);
		TextView other_info = (TextView) convertView
				.findViewById(R.id.textView3);

		TextView queue_start = (TextView) convertView
				.findViewById(R.id.textView4);
		TextView queue_remain = (TextView) convertView
				.findViewById(R.id.textView5);
		TextView queue_end = (TextView) convertView
				.findViewById(R.id.textView6);

		QHApp app = (QHApp) activity.getApplication();

		String branchID = "";
		if (myQData.getClass() == CancelQueueData.class)
			branchID = ((CancelQueueData) myQData).branch_id;
		else
			branchID = myQData.branch_id;

		BranchListData branchData = app.model.getBranchDataWithID(branchID);

		band_name.setTypeface(app.model.DB_OZONEX_BOLD);
		branch_name.setTypeface(app.model.DB_OZONEX);
		other_info.setTypeface(app.model.DB_OZONEX);

		TextView Q_text = (TextView) convertView.findViewById(R.id.textView4);
		Q_text.setTypeface(app.model.DB_OZONEX_BOLD);

		if (queue_remain != null)
			queue_remain.setTypeface(app.model.DB_OZONEX_BOLD);

		TextView unit_text = (TextView) convertView
				.findViewById(R.id.textView6);
		if (unit_text != null)
			unit_text.setTypeface(app.model.DB_OZONEX_BOLD);

		if (branchData != null) {
			final ParseObject bandData = (ParseObject) branchData.brand();
			if (bandData.isDataAvailable()) {
				band_name.setText(bandData.getString("brand_name"));
			} else {
				bandData.fetchInBackground(new GetCallback<ParseObject>() {
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							band_name.setText(bandData.getString("brand_name"));
						} else {
						}
					}
				});
			}

			// check kios status
			String status = "";
			try {
				if (kios_status != null)
					status = (String) kios_status.get(branchID);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			status_list.add(status);

			if (!status.equals(""))
				Log.d("QHappy", "Kios status : " + status);
			//

			bandImage.setParseFile(branchData.branch_image_file());
			bandImage.loadInBackground();

			branch_name.setText(branchData.branch_name());
			if (queue_remain != null)
				queue_remain.setText("" + myQData.queue_remain);
			other_info.setText(branchData.other_info());
			// check cancel queue in temp
			if (myQData.getClass() == CancelQueueData.class) {
				CancelQueueData cancelQueueData = (CancelQueueData) myQData;

				ImageView arrow = (ImageView) convertView
						.findViewById(R.id.imageView2);
				arrow.setVisibility(View.INVISIBLE);

				Log.d("QHappy", "this data is cancel yet.");
				if (queue_remain != null) {
					try {
						LinearLayout container = (LinearLayout) queue_remain
								.getParent();
						container.removeView(queue_remain);
						container.removeView(queue_end);
					} catch (Exception e) {
						Log.d("QHappy", "Can't get container in MYQAdepter.");
					}
				}

				queue_start.setText(cancelQueueData.message);

				// convert to grayScale
				LinearLayout drawView = (LinearLayout) convertView
						.findViewById(R.id.linearLayout1);
				drawView.setAlpha(50);

				ColorMatrix cm = new ColorMatrix();
				cm.setSaturation(0);
				ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
				bandImage.setColorFilter(f);

				convertView.setAlpha(75);
				convertView.setBackgroundColor(Color.GRAY);
			} else if (myQData.claim_time > 0) {
				ImageView arrow = (ImageView) convertView
						.findViewById(R.id.imageView2);
				arrow.setVisibility(View.INVISIBLE);

				Log.d("QHappy", "this data is cancel yet.");
				if (queue_remain != null) {
					try {
						LinearLayout container = (LinearLayout) queue_remain
								.getParent();
						container.removeView(queue_remain);
						container.removeView(queue_end);
					} catch (Exception e) {
						Log.d("QHappy", "Can't get container in MYQAdepter.");
					}
				}

				if (myQData.current_stage != null
						&& Config.current_stage_status_value != null) {
					String value_s = "";
					try {
						value_s = Config.current_stage_status_value
								.getString(myQData.current_stage);
					} catch (JSONException e) {
						e.printStackTrace();
					}

					if (value_s != "")
						queue_start.setText(value_s);
					else
						queue_start.setText("Claimed.");

				}

				// convert to grayScale
				LinearLayout drawView = (LinearLayout) convertView
						.findViewById(R.id.linearLayout1);
				drawView.setAlpha(50);

				ColorMatrix cm = new ColorMatrix();
				cm.setSaturation(0);
				ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
				bandImage.setColorFilter(f);

				convertView.setAlpha(75);
				convertView.setBackgroundColor(Color.GRAY);
			}
			// get wait queue.
			WaitQueueData waitQueueData = app.model
					.getWaitQueueData(myQData.queue_object_id);
			if (waitQueueData != null) {
				if (waitQueueData.isTimeOut) {
					LinearLayout drawView = (LinearLayout) convertView
							.findViewById(R.id.linearLayout1);
					drawView.setAlpha(50);

					convertView.setAlpha(75);
					convertView.setBackgroundColor(Color.GRAY);
				}
				if (queue_remain != null) {
					try {
						LinearLayout container = (LinearLayout) queue_remain
								.getParent();

						container.removeView(queue_remain);
						container.removeView(queue_end);
					} catch (Exception e) {
						Log.d("QHappy", "Can't get container in MYQAdepter.");
					}
				}

				queue_start.setText(Config.held_message_accept);
			}

			if (status.equals("offline")) {

				ImageView arrow = (ImageView) convertView
						.findViewById(R.id.imageView2);
				arrow.setVisibility(View.INVISIBLE);

				Log.d("QHappy", "this data is cancel yet.");
				if (queue_remain != null) {
					try {
						LinearLayout container = (LinearLayout) queue_remain
								.getParent();

						container.removeView(queue_remain);
						container.removeView(queue_end);

						container.setBackgroundColor(0xFFffcc00);
					} catch (Exception e) {
						Log.d("QHappy", "Can't get container in MYQAdepter.");
					}

				}

				queue_start.setText(Config.kios_offline_message);
			}

		}

		return convertView;
	}

	public Bitmap toGrayscale(Bitmap bmpOriginal) {
		if (bmpOriginal != null && !bmpOriginal.isRecycled()) {
			bmpOriginal.recycle();
			bmpOriginal = null;
		}

		int width, height;
		height = bmpOriginal.getHeight();
		width = bmpOriginal.getWidth();

		Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
				Bitmap.Config.RGB_565);
		Canvas c = new Canvas(bmpGrayscale);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmpOriginal, 0, 0, paint);
		return bmpGrayscale;
	}
}
