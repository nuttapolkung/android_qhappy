package net.pirsquare.qhappy.dataAdepter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.pirsquare.qhappy.R;

import com.parse.ParseFile;
import com.viewpagerindicator.IconPagerAdapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MenusFragmentAdapter extends FragmentPagerAdapter  {

    Context context;
    LayoutInflater inflater;
    private List<Fragment> fragment;

    private int mCount;
    public MenusFragmentAdapter(FragmentManager fm,Context context,List<Fragment> fragment) {
		super(fm);
		this.context = context;
        inflater = LayoutInflater.from(context);
        mCount = fragment.size();
        this.fragment = new Vector<Fragment>();
        this.fragment.addAll(fragment);
//        Toast.makeText(context, "menu.size:"+menu.size(), Toast.LENGTH_LONG).show();
        
	}

    @Override
    public Fragment getItem(int position) {
        return fragment.get(position);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
    
    @Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		this.notifyDataSetChanged();
	}
    
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		return super.instantiateItem(container, position);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return super.isViewFromObject(arg0, arg1);
	}

	@Override
	public Parcelable saveState() {
		return super.saveState();
	}
}