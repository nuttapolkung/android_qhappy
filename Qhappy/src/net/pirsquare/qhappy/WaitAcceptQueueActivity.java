package net.pirsquare.qhappy;

import java.util.Date;
import java.util.HashMap;

import net.pirsquare.qhappy.service.QHappyService;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WaitAcceptQueueActivity extends Activity implements AppListener{
	
	Button cancle_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_wait_accept_queue);
		
		QHApp app = (QHApp)getApplication();
		app.addListener(this);
		
		app.model.homeActivity.startCheckReserve();
		app.current_activity = this;
		
		getActionBar().hide();
		
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		 
		 ImageView img = (ImageView)findViewById(R.id.profile_image);
		 img.setBackgroundResource(R.drawable.preload_animation);

		 // Get the background, which has been compiled to an AnimationDrawable object.
		 AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();

		 frameAnimation.start();
		 
		 TextView textV = (TextView)findViewById(R.id.textView1);
		 textV.setTypeface(app.model.DB_OZONEX);
		 textV.setTextSize(36);
		 
		 app.current_activity = this;
	}
	
	private void close() {
		this.finish();
	}
	
	@Override
	protected void onStop() {
		QHApp app = (QHApp)getApplication();
		app.removeListener(this);
		
		app.model.homeActivity.stopCheckReserve();
		
		super.onStop();
	}
	
	@Override
	public void appCallBack(JSONObject jsonObj) {
		this.finish();
	}
	
	@Override
	public void onBackPressed() {
	}
	
	public void onClickCancleReserved(View view)
	{
		final QHApp app = (QHApp)getApplication();
		app.removeListener(this);
		
		app.model.homeActivity.stopCheckReserve();
		app.model.isReserve = false;
		
		final Activity ctx  = this; 
		
		if(app.model.request_activity!=null)
		{
			app.model.request_activity.removeTimer();
		}
		
		final ProgressDialog ringProgressDialog = ProgressDialog.show(ctx, "Please Wait..", "Loading...");
		
		HashMap<String,Object> params = new HashMap<String, Object>();
		params.put("bid", app.model.branch_detact.branch_id());
		params.put("queue_object_id",app.model.request_reserve_queue_id);			
		params.put("time_stop", new Date().getTime() / 1000);
		
//		Request to www.qhappy.com
		QHappyService.request(QHappyService.REQUEST_ACTION_CANCEL,params, new TextHttpResponseHandler() {
			
			@Override
			public void onSuccess(int arg0, Header[] arg1, String arg2) {
				app.model.request_reserve_queue_id = null;
			}
			
			@Override
			public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
				
		    	 AlertDialog.Builder builder = new AlertDialog.Builder(app.model.mainActivity);
			        builder.setMessage(Config.cencel_error_message)
			               .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
			                   public void onClick(DialogInterface dialog, int id) {
			                       
			                   }
			               });
			         builder.create();
			}
			
			@Override
			public void onFinish() {
				 ringProgressDialog.dismiss();
				 close();
				super.onFinish();
			}
		});
//		TODO chnage
		ParseCloud.callFunctionInBackground("cancelQueue",params, new FunctionCallback<String>() {
			  public void done(String  result, ParseException e) {
			    if (e == null) {
			    	app.model.request_reserve_queue_id = null;
			    }else{
			    	
			    	 AlertDialog.Builder builder = new AlertDialog.Builder(app.model.mainActivity);
				        builder.setMessage(Config.cencel_error_message)
				               .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
				                   public void onClick(DialogInterface dialog, int id) {
				                       
				                   }
				               });
				         builder.create();
			    }
			    try{
			    ringProgressDialog.dismiss();
			    close();
			    }catch(Exception error)
			    {
			    	
			    }
			  }
			});
		
		app.model.request_reserve_queue_id = null;
	}
}
