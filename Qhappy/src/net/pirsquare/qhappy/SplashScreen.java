package net.pirsquare.qhappy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.pirsquare.qhappy.model.BranchListData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.service.QHappyService;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;

import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class SplashScreen extends Activity {

	String device_token;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		// Track app opens.
		ParseAnalytics.trackAppOpenedInBackground(getIntent());

		if (this.getIntent().getExtras() != null) {
			String json = this.getIntent().getStringExtra("json");
			if (json != null) {
				QHApp app = (QHApp) getApplication();

				app.model.notification_temp = readFromFile();

				Log.d("QHappy", "Notification temp"
						+ app.model.notification_temp);
			}
		}

		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();

	}

	private String readFromFile() {

		String ret = "";

		try {
			InputStream inputStream = openFileInput("temp_queue.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("login activity", "File not found: " + e.toString());

		} catch (IOException e) {
			Log.e("login activity", "Can not read file: " + e.toString());

		}

		return ret;
	}

	private void forceStopService(String service_name) {
		try {

			ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
			List<RunningAppProcessInfo> runningAppProcesses = am
					.getRunningAppProcesses();

			Iterator<RunningAppProcessInfo> iter = runningAppProcesses
					.iterator();

			while (iter.hasNext()) {
				RunningAppProcessInfo next = iter.next();

				Log.d("PROCESS", next.processName);

				if (next.processName.equals(service_name)) {
					Log.d("QHappy", "Kill process name >> " + next.processName);
					android.os.Process.killProcess(next.pid);
				}
			}
		} catch (Exception e) {
			Log.d("QHappy", "can't stop background service.");
		}
	}

	@Override
	protected void onStart() {

		forceStopService(getPackageName() + ":fetch_notification");

		getMobileConfig();
		getConfigDataFromParse();
		// create localStorge.
		createLocalDataStorge();

		// loadMyQ();
		super.onStart();
	}

	private void getMobileConfig() {
		QHappyService.request("GetMobileConfig", new HashMap<String, Object>(),
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						try {
							JSONObject json = new JSONObject(arg2);
							if (json.get("result") != null) {
								Config.current_stage_status_value = json
										.getJSONObject("result");
							} else {
								Log.d("QHappy", "Get mobile config error :"
										+ json.get("error"));
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						getMobileConfig();
					}
				});
	}

	private void getConfigDataFromParse() {
		final Activity ctx = this;
		ParseCloud.callFunctionInBackground("getConfigData",
				new HashMap<String, Object>(),
				new FunctionCallback<HashMap<String, Object>>() {
					public void done(HashMap<String, Object> result,
							ParseException e) {
						if (e == null) {

							Config.claim_time_expired = Integer
									.parseInt((String) result
											.get("claim_time_expired"));
							Config.held_message_accept = (String) result
									.get("held_message_accept");
							Config.held_message_accept_timeout = (String) result
									.get("held_message_accept_timeout");
							Config.held_message_keep_queue = (String) result
									.get("held_message_keep_queue");
							Config.held_message_accept_timeout_in_row = (String) result
									.get("held_message_accept_timeout_in_row");
							Config.held_message_reject = (String) result
									.get("held_message_reject");
							Config.held_timeout = Integer
									.parseInt((String) result
											.get("held_timeout"));
							Config.cencel_error_message = (String) result
									.get("cencel_error_message");
							Config.request_reserve_timeout = Integer
									.parseInt((String) result
											.get("request_reserve_timeout"));
							Config.kios_offline_message = (String) result
									.get("kios_offline_message");
							Config.FORCE_UPDATE_MESSGAE = (String) result
									.get("force_update_message");
							Config.near_me_distance_meter = Double
									.parseDouble((String) result
											.get("near_me_distance_meter"));
							// Check app version.
							String server_app_version = (String) result
									.get("android_version");
							// Request url
							Config.request_base_url = (String) result
									.get("request_base_url");
							Config.request_refresh_url = (String) result
									.get("request_refresh_url");

							if (!server_app_version.equals(Config.app_version)) {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										ctx);
								builder.setCancelable(false);
								builder.setTitle("QHappy");
								builder.setMessage(Config.FORCE_UPDATE_MESSGAE)
										// builder.setMessage("กรุณาอัพเดท QHappy เป็นเวอร์ชั่นล่าสุดเพื่อการทำงานที่ถูกต้อง")
										.setPositiveButton(
												"Update now",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														final String appPackageName = getPackageName(); // getPackageName()
																										// from
																										// Context
																										// or
																										// Activity
																										// object
														try {
															startActivity(new Intent(
																	Intent.ACTION_VIEW,
																	Uri.parse("market://details?id="
																			+ appPackageName)));
														} catch (android.content.ActivityNotFoundException anfe) {
															startActivity(new Intent(
																	Intent.ACTION_VIEW,
																	Uri.parse("http://play.google.com/store/apps/details?id="
																			+ appPackageName)));
														}
													}
												});

								// Create the AlertDialog object and return it
								// try for lose context.
								try {
									builder.create().show();
								} catch (Exception error) {
								}
								;

							} else {
								initializeData();
							}
						} else {
							Log.d("QHappy", "get config data error.");
							try_toget_config--;
							if (try_toget_config <= 0)
								initializeData();
							else
								getConfigDataFromParse();
						}
					}
				});
	}

	int try_toget_config = 5;

	private void initializeData() {
		ParseCloud.callFunctionInBackground("getMetaData",
				new HashMap<String, Object>(),
				new FunctionCallback<List<ParseObject>>() {
					public void done(List<ParseObject> result, ParseException e) {
						if (e == null) {

							if (result.size() > 0) {
								QHApp app = (QHApp) getApplication();

								app.model.branch_metadata = new ArrayList<BranchListData>();

								for (ParseObject parseObject : result) {
									BranchListData data = new BranchListData(
											parseObject);
									app.model.branch_metadata.add(data);
								}

								Log.d("Qhappy",
										"Load BranchMetaData Complete...");

								getDeviceToken();
							} else {
								initializeData();
								Log.d("Qhappy",
										"Load BranchMetaData No data...");
							}
						} else {
							initializeData();
							Log.d("Qhappy", "Load BranchMetaData Error...");
						}
					}
				});

		// getDeviceToken();

	}

	private void getDeviceToken() {
		String android_id = Secure.getString(getApplicationContext()
				.getContentResolver(), Secure.ANDROID_ID);

		ParseInstallation installation = ParseInstallation
				.getCurrentInstallation();
		installation.put("UniqueId", android_id);

		device_token = (String) installation.get("deviceToken");

		if (device_token == null)
			installation.saveInBackground(new SaveCallback() {

				@Override
				public void done(ParseException arg0) {
					Log.d("getDeviceToken", "recheck devicetoken.");
					getDeviceToken();
				}
			});
		else {
			WifiManager wimanager = (WifiManager) getApplicationContext()
					.getSystemService(Context.WIFI_SERVICE);
			String macAddress = wimanager.getConnectionInfo().getMacAddress();
			if (macAddress == null) {
				macAddress = "Device don't have mac address or wi-fi is disabled";
			} else {
				installation.put("mac_address", macAddress);
				installation.saveInBackground();
			}

			SharedPreferences share = getSharedPreferences("register",
					Context.MODE_PRIVATE);
			String d = share.getString("device_token", "");
			if (d.equals("")) {

				Editor e = share.edit();
				e.putString("device_token", device_token);
				e.commit();
			}

			registerDeviceAtQHappyDotCom();

			Log.d("addMainActivity", "done.");

			loadMyQ();

			addMainActivity();
		}
	}

	private int register_count = 4;

	private void registerDeviceAtQHappyDotCom() {
		if (register_count <= 0)
			return;

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("device_token", ParseInstallation.getCurrentInstallation()
				.get("deviceToken"));
		params.put("objectId", ParseInstallation.getCurrentInstallation()
				.getObjectId());
		params.put("device_name", android.os.Build.MODEL);

		QHappyService.request(QHappyService.REQUEST_ACTION_REGISTER, params,
				new TextHttpResponseHandler() {

					@Override
					public void onSuccess(int arg0, Header[] arg1, String arg2) {
						Log.d("QHappy", "Register device complete.");
					}

					@Override
					public void onFailure(int arg0, Header[] arg1, String arg2,
							Throwable arg3) {
						Log.d("QHappy", "Register device Failed.");
						register_count--;
						registerDeviceAtQHappyDotCom();
					}
				});
	}

	public void createLocalDataStorge() {
		ParseQuery<ParseObject> branch_query = ParseQuery
				.getQuery("t_brand_metadata");
		branch_query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> arg0, ParseException arg1) {
				if (arg1 == null) {
					final List<ParseObject> obj = arg0;
					new Thread(new Runnable() {
						public void run() {
							try {
								ParseObject.pinAll(obj);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}).start();

				}
			}
		});

		ParseQuery<ParseObject> query = ParseQuery
				.getQuery("t_branch_metadata");
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> arg0, ParseException arg1) {

				if (arg1 == null) {
					final List<ParseObject> obj = arg0;
					new Thread(new Runnable() {
						public void run() {
							try {
								ParseObject.pinAll(obj);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}).start();
				}
			}
		});

	}

	public void loadMyQ() {
		String device_token = ParseInstallation.getCurrentInstallation()
				.getString("deviceToken");
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("device_token", device_token);

		ParseCloud.callFunctionInBackground("getMyQ", params,
				new FunctionCallback<List<ParseObject>>() {
					public void done(List<ParseObject> result, ParseException e) {
						if (e == null) {
							final QHApp app = (QHApp) getApplication();

							ArrayList<QueueData> myQ_data = new ArrayList<QueueData>();

							myQ_data.addAll(app.model.cancel_queues);

							app.model.myq_data = new ArrayList<QueueData>();
							for (ParseObject obj : result) {
								QueueData queue_data = new QueueData(obj);
								myQ_data.add(queue_data);
								app.model.myq_data.add(queue_data);
							}
						}
					}
				});
	}

	private void addMainActivity() {
		Intent i = new Intent(SplashScreen.this, MainActivity.class);
		startActivity(i);

		Intent intent = getIntent();
		String json_string = null;
		if (intent != null) {
			Bundle extra = intent.getExtras();
			if (extra != null)
				json_string = extra.getString("com.parse.Data");
		}

		final String json_final = json_string;

		if (json_string != null) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					JSONObject json = null;
					try {
						json = new JSONObject(json_final);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					String ms = "";
					try {
						ms = json.getString("alert");
					} catch (JSONException e) {
						e.printStackTrace();
					}

					QHApp app = (QHApp) getApplicationContext();

					// Send the name of the connected device back to the UI
					// Activity
					Message msg = app.mHandler.obtainMessage(0);
					Bundle bundle = new Bundle();
					bundle.putString("json", json_final);
					msg.setData(bundle);

					app.mHandler.sendMessage(msg);

					finish();
				}
			}, 3000);

			Log.d("QHappy", "JSON String : " + json_string);

		} else
			// close this activity
			finish();
	}
}
