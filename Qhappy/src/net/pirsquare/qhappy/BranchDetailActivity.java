package net.pirsquare.qhappy;

import net.pirsquare.qhappy.QueueDetailActivity.PlaceholderFragment;
import net.pirsquare.qhappy.model.BrandData;
import net.pirsquare.qhappy.model.QueueData;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class BranchDetailActivity extends ActionBarActivity{
	
	BranchFragment frag;
	private BrandData brand;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_queue_detail);

		ActionBar actionBar = getActionBar();
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("QHappy");

		QHApp app = (QHApp) getApplication();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		app.current_activity = this;

		brand = app.model.currentBrandData;
		
		if (savedInstanceState == null) {
			FragmentManager fm = getSupportFragmentManager();
			 FragmentTransaction fmt = fm.beginTransaction();
			 frag = new BranchFragment();
			 frag.init(brand);
			 fmt.add(R.id.container, frag);
			 fmt.commit();
		}
	}
	
	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event)
	 {
		 if (keyCode == KeyEvent.KEYCODE_BACK)
		 {
			 getSupportFragmentManager().beginTransaction().remove(frag).commit();
			 
			 this.finish();
		 }

		 return super.onKeyDown(keyCode, event);
	 }

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_queue_detail,
					container, false);
			return rootView;
		}
	}

	
}
