package net.pirsquare.qhappy;

import java.util.LinkedList;
import java.util.List;

import net.pirsquare.qhappy.dataAdepter.BrandAdapter;
import net.pirsquare.qhappy.model.BrandData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.service.ServiceCallBack;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.handmark.pulltorefresh.extras.listfragment.PullToRefreshListFragment;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

public class ListFragment extends PullToRefreshListFragment implements ServiceCallBack,OnRefreshListener<ListView> {
	
	private QHApp app;
	Context context;
	private  BranchFragment fragment2;
	private int state = 0;
	private Activity act;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		act  = getActivity();
		app = (QHApp)getActivity().getApplication();
		app.service.addListener(this);
		
		 LocationListener locationListener = new LocationListener() {
			
			public void onLocationChanged(Location location) {
				app.model.lng = ""+location.getLongitude();
				app.model.lat = ""+location.getLatitude();
			}
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				
			}
			
			@Override
			public void onProviderEnabled(String provider) {
			
			}
			
			@Override
			public void onProviderDisabled(String provider) {
				
			}
		};
		
		LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE); 
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 3000, locationListener);
		Location location =lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if(location!=null)
		{
			app.model.lng = ""+location.getLongitude();
			app.model.lat = ""+location.getLatitude();
		}
	}

	@Override
	protected PullToRefreshListView onCreatePullToRefreshListView(
			LayoutInflater inflater, Bundle savedInstanceState) {

		PullToRefreshListView view = new PullToRefreshListView(getActivity());

		
		return view;
	}
	
	@Override
	public void onDestroy() {
		
		app.service.removeListener(this);
		super.onDestroy();
	}

	@Override
	public void onStart() {
		
		super.onStart();
		
		state=1;
		
		mPullRefreshListView = this.getPullToRefreshListView();
		mPullRefreshListView.setOnRefreshListener(this);
		
		
		loadData();
		
	}
	@Override
	public void onPause() {
		state = 0;
		super.onPause();
	}

	public void loadData() {
		
		this.setListShown(false);
		if(!app.getInternetConnection())
		{
			Intent term_activity = new Intent(getActivity(), NotConnectInternetActivity.class);
			term_activity.putExtra("want_refresh", true);
			startActivity(term_activity);
			return;
		}
		app.service.getBrandMetaData();
		
	}
//	Listener
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		
		loadData();
		
	}
	
	@Override
	public void callBackGetFavData(List<ParseObject> result) {
		
	}
	
	private LinkedList<BrandData> mListItems;
	private BrandAdapter mAdapter;
	private PullToRefreshListView mPullRefreshListView;
	private ListView actualListView;
	
	@Override
	public void callBackGetBrandMetaData(List<ParseObject> result) {
		
		if(state==0)return;
		
		mListItems = new LinkedList<BrandData>();
		
		for (ParseObject parseObject : result) {
			
			// Locate images in brand_image column
			ParseFile image = (ParseFile) parseObject.get("brand_image");

			BrandData brand = new BrandData();
			brand.setBrandName((String) parseObject.getString("brand_name"));
			brand.setBrandInfo((String) parseObject.getString("brand_info"));
			brand.setBrandLogo(image.getUrl());
			brand.setOBID((String)parseObject.getObjectId());
			brand.image_file = image;
			mListItems.push(brand);
		}
		
		actualListView = mPullRefreshListView.getRefreshableView();
		mAdapter = new BrandAdapter(getActivity(),app, mListItems);
		
		 actualListView.setAdapter(mAdapter);
		 this.setListShown(true);
		 
		actualListView.setOnItemClickListener(new OnItemClickListener() {
		
             @Override
             public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
               
              // ListView Clicked item index
//              int itemPosition     = position;
              
              // ListView Clicked item value
              BrandData  itemData  = mListItems.get(position-1);
                 
               // Show Alert 
              
//               Toast.makeText(getActivity().getApplicationContext(),
//                 "Position :"+itemPosition+"  ListItem : " +itemData.getBrandName() , Toast.LENGTH_LONG)
//                 .show();
               
//              getActivity().set
              
//               FragmentManager fm =  getActivity().getSupportFragmentManager();
//               FragmentTransaction fmt = fm.beginTransaction();
//               if(fragment2 == null){
//            	   fragment2 = new BranchFragment();
//                   fragment2.init(itemData);
//                   BranchFragment frag = new BranchFragment();
//	      			frag.init(itemData);
//	      			 fmt.add(R.id.container, frag);
//	      			 fmt.commit();
//               }else{
//            	   fmt.addToBackStack(null);
//            	   fmt.hide(ListFragment.this);
//            	   fmt.replace(android.R.id.content, fragment2);
//            	   fmt.commit();
//               }
            	 
            	 int pos = position-1;
	           	  BrandData  brand  = mListItems.get(pos);
	           	 
	           	  QHApp app = (QHApp)getActivity().getApplication();
	           	  app.model.currentBrandData = brand;
	           	  
	           	  Intent branch_detail = new Intent(getActivity(),BranchDetailActivity.class);
	           	  startActivity(branch_detail);
             }
     });    
		
		mPullRefreshListView.onRefreshComplete();
		
	}
	
	@Override
	public void callBackGetMyQData(List<ParseObject> result) {
	}
	
	@Override
	public void Error(ParseException e) {
		
//		 this.setListShown(true);
//		 AlertDialog.Builder builder = new AlertDialog.Builder(act);
//		 builder.setTitle("QHappy");
//	        builder.setMessage("Connection error please try again later.")
//	               .setPositiveButton("ปิด", new DialogInterface.OnClickListener() {
//	                   public void onClick(DialogInterface dialog, int id) {
//	                      
//	                   }
//	               })  ;
//	  
//	        builder.create().show();
	        
	       
	}
	@Override
	public void callBackGetBranchMetaData(List<ParseObject> result) {
		
	}
	
	 public static class PlaceholderFragment extends Fragment {
		 	
		 	
	        public PlaceholderFragment() {
	        }

	        @Override
	        public View onCreateView(LayoutInflater inflater, ViewGroup container,
	          Bundle savedInstanceState) {
	         View myFragmentView = inflater.inflate(R.layout.fragment_brand_detail, container, false);
	        
	         return myFragmentView;
	        }
	        
	    }

	@Override
	public void callBackGetMyQDataQueue(List<QueueData> result) {
		// TODO Auto-generated method stub
		
	}
}
