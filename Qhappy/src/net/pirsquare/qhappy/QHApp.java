package net.pirsquare.qhappy;

import java.util.ArrayList;

import net.pirsquare.qhappy.model.Model;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.service.Service;
import net.pirsquare.qhappy.util.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseTwitterUtils;
import com.parse.PushService;

interface AppListener
{
	public void appCallBack(JSONObject jsonObj);
}
public class QHApp extends Application {

	public static String QUEUE_STATUS_ACCEPT  = "accepted";
	public static String QUEUE_STATUS_REJECT  = "rejected";
	public static String QUEUE_STATUS_CLAIMED  = "claimed";
	public static String QUEUE_STATUS_CALLED  = "called";
	public static String QUEUE_STATUS_HELD  = "held";
	public static String QUEUE_STATUS_QUEUED  = "queued";
	public static String QUEUE_STATUS_UPDATE_STATUS  = "update_status";
	public static String QUEUE_STATUS_CANCEL = "canceled";
	public static String QUEUE_STATUS_UPDATE  = "update";
	public static String QUEUE_STATUS_UPDATE_REMAIN  = "update_remain";
	public static String QUEUE_STATUS_DELETE  = "delete";
	public static String QUEUE_STATUS_TIMEOUT_DELETE  = "timeout_delete";
	
	public ArrayList<QueueData> mListItemsTemp;
	public String appName = "QHappy";

	public String device_token = "";
	
	public Bitmap screen_shot;

	public Model model = new Model();
	public Service service = new Service();
	
	public ArrayList<AppListener> listeners = new ArrayList<AppListener>();
	
	// Internet Connection detector
    private ConnectionDetector cd;
    
    public Activity current_activity;
    
    public static QHApp instance;
	
	@Override
	public void onCreate() {
		
		super.onCreate();
		
		QHApp.instance = this;
		
		SharedPreferences share =  this.getSharedPreferences("app_1",Context.MODE_PRIVATE);
		
		String appID = share.getString("appID", "F0gukxOuWRODhhk40P1m4ya70rPxhoQYMv908WNz");
		String client = share.getString("client", "nsOBZEkPYxYajiSSgq5bvwxb0tMKxAl9KuEVQzvl");
		
//		comment key below if you use real server.
//		appID ="JQU11HKdq2IzVME0M9kpinzroSM4Fyzx0JL2fwwe";
//		client ="glw3uWghdV0yIbbW80jExIHbYHj70AEGppAOYSTF";
		

//		enabled localDataStore.
		try {
			Parse.enableLocalDatastore(this);
		} catch (Exception e) {
			Log.d("QHappy", "can't enable offline storage.");
		}
		
		
		Parse.initialize(this, appID,client);
		

		ParseFacebookUtils.initialize("508147449297255");
		ParseTwitterUtils.initialize("QX1YvSkKABJhgCvqCu4DPjZJv", "4YeX06FDAhnm4gmT16lpaqYH7XoPq12WFGODKJjec4KjiBtUKH");
		
//		PushService.setDefaultPushCallback(this, MainActivity.class);
//		PushService.setDefaultPushCallback(this, SplashScreen.class);
		
//		PushService.subscribe(this, "test", MainActivity.class);
//		ParseInstallation.getCurrentInstallation().saveInBackground();
		
//		PushService.setDefaultPushCallback(this, MainActivity.class);
		
		initializeFont();
	}
	
	
	public void reInitialzeParse(String appID , String client)
	{
		Parse.initialize(this, appID,client);
		
		SharedPreferences share =  this.getSharedPreferences("app_1",Context.MODE_PRIVATE);
		Editor edit = share.edit();
		edit.putString("appID", appID);
		edit.putString("client", client);
		edit.commit();
	}
	
	private void initializeFont() {
		model.DB_OZONEX =Typeface.createFromAsset(getAssets(),"fonts/DB Ozone X.ttf");
		model.DB_OZONEX_BOLD =Typeface.createFromAsset(getAssets(),"fonts/DB Ozone X Bd.ttf");
	}
	
	public void addListener(AppListener listener)
	{
		if(listeners.indexOf(listener)  == -1)
		listeners.add(listener);
	}
	
	public void removeListener(AppListener listener)
	{
		listeners.remove(listener);
	}
	
	public void callListener(JSONObject jsonObj)
	{
		
		ArrayList<AppListener> call_backs = (ArrayList<AppListener>) listeners.clone();
		
		for (AppListener listener : call_backs) {
			try{
			listener.appCallBack(jsonObj);
			}catch(Exception e)
			{
				Log.d("QHAppy","callListener can't call");
			}
		}
		
		call_backs = null;
	}

	public void updateToken() {
		device_token = (String) ParseInstallation.getCurrentInstallation().get(
				"deviceToken");
	}
	
	public Boolean getInternetConnection(){
		cd = new ConnectionDetector(getApplicationContext());
		 
        if (!cd.isConnectingToInternet()) {

            return false;
        }else{
        	return true;
        }
	}

	public final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			
				Bundle bundle  = (Bundle)msg.getData();
				
				JSONObject json_obj = new JSONObject();
				try {
					json_obj = new JSONObject(bundle.getString("json"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				if(json_obj!=null)
				callListener(json_obj);
		}
	};
	
}
