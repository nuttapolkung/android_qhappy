package net.pirsquare.qhappy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import net.pirsquare.qhappy.dataAdepter.BranchAdapter;
import net.pirsquare.qhappy.dataAdepter.PromotionAdepter;
import net.pirsquare.qhappy.model.BranchData;
import net.pirsquare.qhappy.model.BrandData;
import net.pirsquare.qhappy.model.PromotionData;
import net.pirsquare.qhappy.service.ServiceCallBack;

import com.handmark.pulltorefresh.extras.listfragment.PullToRefreshListFragment;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class PromotionFragment extends PullToRefreshListFragment  implements OnRefreshListener<ListView>{

	private QHApp app;
	private PromotionData promotion_data;
	
	private ArrayList<PromotionData> mListItems;
	
	private PromotionAdepter mAdapter;
	
	private PullToRefreshListView mPullRefreshListView;
	private ListView actualListView;
	private View staticView;
	private Boolean isStart = false;
	private Activity mContext;
	
	private ImageView nopromotion_image;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		app = (QHApp)getActivity().getApplication();
		mContext = getActivity();
		isStart = true;
		
	}
	
	@Override
	public void onStart() {
		
		super.onStart();
		
		isStart = false;
		mPullRefreshListView = this.getPullToRefreshListView();
		mPullRefreshListView.setOnRefreshListener(this);
		mPullRefreshListView.setPadding(0, 0, 0, 50);
		
		nopromotion_image = new ImageView(mContext);
		nopromotion_image.setImageResource(R.drawable.nopromotion);
		
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
		        LayoutParams.WRAP_CONTENT,      
		        LayoutParams.WRAP_CONTENT,
		        Gravity.CENTER
		);
		params.setMargins(20, 20,30, 20);
		
		nopromotion_image.setVisibility(View.INVISIBLE);
		
		ViewGroup parent = (ViewGroup)mPullRefreshListView.getParent().getParent();
		parent.addView(nopromotion_image,params);
		
		loadData();
		
	}
	
	public void loadData() {
		
		this.setListShown(false); 
		
		
		mListItems = app.model.promotion_lists;
		
		if(mListItems.size()==0)
			nopromotion_image.setVisibility(View.VISIBLE);
		else
			nopromotion_image.setVisibility(View.INVISIBLE);
		
		
		actualListView = mPullRefreshListView.getRefreshableView();
		
		
		
		Collections.reverse(mListItems);
		
		if(mAdapter == null){
			mAdapter = new PromotionAdepter(getActivity(),app,mListItems);
		}else{
			mAdapter.setData(mListItems);
			mAdapter.notifyDataSetChanged();
		}
		
		if(mAdapter!=null){
			actualListView.setAdapter(mAdapter);    
		}
		
		this.setListShown(true);
		 
		 
		 actualListView.setOnItemClickListener(new OnItemClickListener() {
			 
             @Override
             public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
               
              // ListView Clicked item value
              PromotionData  itemData = mListItems.get(position-1);
               // Show Alert 
              
              Intent promotionDetail = new Intent(mContext	, PromotionDetail.class);
              promotionDetail.putExtra("index", position);
              startActivity(promotionDetail);
              
               
             }
		 });
		 
		 this.actualListView.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				 mPullRefreshListView.onRefreshComplete();
				
			}
		}, 1000);
		
		
		
	}
//	Listener
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		loadData();
		
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
