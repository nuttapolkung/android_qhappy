package net.pirsquare.qhappy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.pirsquare.qhappy.dataAdepter.FavAdepter;
import net.pirsquare.qhappy.model.BranchListData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.service.ServiceCallBack;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.handmark.pulltorefresh.extras.listfragment.PullToRefreshListFragment;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

public class FavoriteFragment extends PullToRefreshListFragment implements FavAdepterRemoveFav,
		ServiceCallBack, OnRefreshListener<ListView> {

	private QHApp app;
	Context context;

	public Boolean isFristLoad;
	public int state = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (QHApp) getActivity().getApplication();
		app.service.addListener(this);
		
		Log.d("Fav", "onCreate");
	}

	@Override
	public void onStart() {

		Log.d("Fav", "onStart");
//		if (mPullRefreshListView == null) {
			mPullRefreshListView = this.getPullToRefreshListView();
			mPullRefreshListView.setOnRefreshListener(this);
			
			no_fav = new ImageView(getActivity());
			no_fav.setScaleType(ScaleType.CENTER_INSIDE);
			no_fav.setImageDrawable(getResources().getDrawable(R.drawable.empty_no_fav));
			
			FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
			        LayoutParams.WRAP_CONTENT,      
			        LayoutParams.WRAP_CONTENT,
			        Gravity.CENTER
			);
			
			params.setMargins(20, 20, 20, 20);
			
			no_fav.setVisibility(View.INVISIBLE);
			
			ViewGroup parent = (ViewGroup)mPullRefreshListView.getParent().getParent();
			parent.addView(no_fav,params);
			
		state = 1;
		loadData();
		
//		mPullRefreshListView.setBackgroundColor(Color.GREEN);
		Button test = new Button(getActivity());
	
//		this.addView(test);
		
		
//		View v =getView()actualListView;
//		
//		ImageView no_data =new ImageView(getActivity());
//		no_data.setImageDrawable(getResources().getDrawable(R.drawable.empty_no_fav));
//		
//		
//		RelativeLayout layout = (RelativeLayout)getActivity().findViewById(R.id.fav_layout);
//		layout.addView(no_data);

		super.onStart();
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
	
//		ImageView no_fav_image_view = new ImageView(getActivity());
//		no_fav_image_view.setImageDrawable(getResources().getDrawable(R.drawable.empty_no_fav));
//		LayoutParams lauout_params = new LayoutParams(300,300);
//		
//		getActivity().getWindow().addContentView(no_fav_image_view, lauout_params);
		
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onResume() {
		Log.d("Fav", "onResume");
		super.onResume();
	}

	@Override
	public void onPause() {
		state = 0;
		Log.d("FAv", "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.d("Fav", "onStop");
		super.onStop();
	}

	public void loadData() {
		
		
//		check internet connection.
		QHApp app = (QHApp)getActivity().getApplication();
		if(!app.getInternetConnection())
		{
			Intent term_activity = new Intent(getActivity(), NotConnectInternetActivity.class);
			term_activity.putExtra("want_refresh", true);
			startActivity(term_activity);
			return;
		}
		
		no_fav.setVisibility(View.INVISIBLE);
		
		this.setListShown(false);
		
		
		app.service.getFav();
	}

	@Override
	public void onDestroy() {

		app.service.removeListener(this);
		Log.d("Fav", "onDestroy");
		super.onDestroy();
	}

	@Override
	protected PullToRefreshListView onCreatePullToRefreshListView(
			LayoutInflater inflater, Bundle savedInstanceState) {
		PullToRefreshListView view = new PullToRefreshListView(getActivity());
		
		return view;
	}
	
	// Listener
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		loadData();
	}

	private FavAdepter mAdapter;
	private PullToRefreshListView mPullRefreshListView;
	private ListView actualListView;
	private ImageView no_fav;


	
	@Override
	public void callBackGetBrandMetaData(List<ParseObject> result) {

	}

	@Override
	public void Error(ParseException e) {

	}

	@Override
	public void callBackGetFavData(List<ParseObject> result) {
		
		if(mPullRefreshListView!=null)
		mPullRefreshListView.onRefreshComplete();
		if (state == 0)
			return;

		QHApp app = (QHApp) getActivity().getApplication();

		ArrayList<BranchListData> branch_data = new ArrayList<BranchListData>();

		for (ParseObject obj : result) {
			BranchListData queue_data = new BranchListData(obj);
			branch_data.add(queue_data);
		}

		app.model.fav_data = branch_data;
		
		if(branch_data.size() ==0)
			no_fav.setVisibility(View.VISIBLE);
		else
			no_fav.setVisibility(View.INVISIBLE);
		
		Log.d("Fav" ,"size"+branch_data.size());


//		if(actualListView == null)
		actualListView = mPullRefreshListView.getRefreshableView();

		mAdapter = new FavAdepter(getActivity(), branch_data,this);

		actualListView.setAdapter(mAdapter);
		this.setListShown(true);

		actualListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

			}
		});

		
	}
	
	public void removeFav(String bid)
	{
		final ProgressDialog ringProgressDialog = ProgressDialog.show(getActivity(),
				"Please Wait..", "Loading...");
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid",bid);
		params.put("device_token", ParseInstallation.getCurrentInstallation().getString("deviceToken"));
		
		ParseCloud.callFunctionInBackground("unSetFav", params,
				new FunctionCallback<String>() {
					public void done(String result, ParseException e) {
						if (e == null) {
							loadData();
						} else {

						}
						ringProgressDialog.dismiss();
						
					}
				});
	}

	@Override
	public void callBackGetBranchMetaData(List<ParseObject> result) {

	}


	@Override
	public void callBackGetMyQData(List<ParseObject> result) {

	}

	@Override
	public void callBackGetMyQDataQueue(List<QueueData> result) {
		// TODO Auto-generated method stub
		
	}
}
