package net.pirsquare.qhappy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pirsquare.qhappy.dataAdepter.BranchAdapter;
import net.pirsquare.qhappy.dataAdepter.BrandAdapter;
import net.pirsquare.qhappy.model.BranchData;
import net.pirsquare.qhappy.model.BrandData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.service.ServiceCallBack;
import net.pirsquare.qhappy.util.ImageLoader;
import android.animation.LayoutTransition;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.handmark.pulltorefresh.extras.listfragment.PullToRefreshListFragment;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;

public class BranchFragment extends PullToRefreshListFragment implements ServiceCallBack,OnRefreshListener<ListView>{

	private QHApp app;
	private BrandData brand;
	
	private LinkedList<BranchData> mListItems;
	private BranchAdapter mAdapter;
	private PullToRefreshListView mPullRefreshListView;
	private ListView actualListView;
	private View staticView;
	private Boolean isStart = false;
	
	public void init(BrandData brand){
		this.brand = brand;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		app = (QHApp)getActivity().getApplication();
		app.service.addListener(this);
		
		isStart = true;
	}
	
	@Override
	public void onStart() {
		
		super.onStart();
		
		mPullRefreshListView = this.getPullToRefreshListView();
		mPullRefreshListView.setOnRefreshListener(this);
		mPullRefreshListView.setPadding(0, 0, 0, 50);;
		
//		ImageLoader imageLoader = new ImageLoader(mPullRefreshListView.getContext());
		staticView = LayoutInflater.from(mPullRefreshListView.getContext()).inflate(R.layout.place_header_row, null);
		TextView name = (TextView) staticView.findViewById(R.id.brandName);
		TextView info = (TextView) staticView.findViewById(R.id.brandInfo);
		TextView branchText = (TextView) staticView.findViewById(R.id.branchText);
         // Locate the ImageView in list_row.xml
		ParseImageView logo = (ParseImageView) staticView.findViewById(R.id.brandImage);
		logo.setParseFile(brand.image_file);
		logo.loadInBackground();
        
        name.setText(brand.getBrandName());
        info.setText(brand.getBrandInfo());
        // Set the results into ImageView
//        imageLoader.DisplayImage(brand.getBrandLogo(),logo);
        
        name.setTypeface(app.model.DB_OZONEX_BOLD);
        info.setTypeface(app.model.DB_OZONEX);
        branchText.setTypeface(app.model.DB_OZONEX_BOLD);
        
		loadData();
		
	}
	
	public void loadData() {
		
		this.setListShown(false); 
		
		app.service.getBranchMetaData(app.model.currentBrandData.getOBID(),app.model.lat,app.model.lng);
		
	}
//	Listener
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		loadData();
	}
	
	@Override
	public void callBackGetMyQData(List<ParseObject> result) {
		
	}
	
	@Override
	public void callBackGetFavData(List<ParseObject> result) {
		
	}
	
	@Override
	public void callBackGetBrandMetaData(List<ParseObject> result) {
	}
	public void callBackGetBranchMetaData(List<ParseObject> result) {
		mListItems = new LinkedList<BranchData>();
		
		for (int i =result.size()-1; i >=0 ; i--) {
			BranchData branch = new BranchData((ParseObject)result.get(i));
			mListItems.push(branch);
		}
		
		actualListView = mPullRefreshListView.getRefreshableView();
		
		if(isStart){
			actualListView.addHeaderView(staticView);
			isStart = false;
		}
		
		if(mAdapter == null){
			mAdapter = new BranchAdapter(getActivity(),app,brand, mListItems);
		}else{
			mAdapter.setData(mListItems,brand);
			mAdapter.notifyDataSetChanged();
		}
		
		
		if(mAdapter!=null){
			actualListView.setAdapter(mAdapter);    
		}
		
		this.setListShown(true);
		 
		 mPullRefreshListView.onRefreshComplete();
		 
		 actualListView.setOnItemClickListener(new OnItemClickListener() {
			 
             @Override
             public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
               
             }
		 });
	}
	
	
	@Override
	public void Error(ParseException e) {
		
	}

	@Override
	public void onDestroy() {
//		Toast.makeText(getActivity(),"DESTROY", Toast.LENGTH_LONG).show();
		app.service.removeListener(this);
		super.onDestroy();
	}

	@Override
	public void callBackGetMyQDataQueue(List<QueueData> result) {
		// TODO Auto-generated method stub
		
	}
}
