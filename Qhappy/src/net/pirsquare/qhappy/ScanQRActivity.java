package net.pirsquare.qhappy;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

interface IScanQRActivity
{
	public void onDetectCode(String code);
}
//@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class ScanQRActivity extends Activity   {
	
	private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    TextView scanText;
    Button scanButton;

    ImageScanner scanner;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    static {
        System.loadLibrary("iconv");
    } 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_scan_qr);

		final ActionBar actionBar = getActionBar();
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("QHappy");
		
		QHApp app =(QHApp)getApplication();
		app.current_activity = this;
		
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

	        autoFocusHandler = new Handler();
	        mCamera = getCameraInstance();

	        /* Instance barcode scanner */
	        scanner = new ImageScanner();
	        scanner.setConfig(0, Config.X_DENSITY, 3);
	        scanner.setConfig(0, Config.Y_DENSITY, 3);

	        mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
	        FrameLayout preview = (FrameLayout)findViewById(R.id.cameraPreview);
	        preview.addView(mPreview);
    
	        startScan();
	        
	        TextView footer_textview = (TextView)findViewById(R.id.textView1);
	        footer_textview.setTypeface(app.model.DB_OZONEX);
	}
	
	private void onstop() {
		
		this.finish();
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		Log.d("QHAppy", "Destroy");
		releaseCamera();
		super.onDestroy();
	}
	
	public void startScan() {
        if (barcodeScanned) {
            barcodeScanned = false;
//            scanText.setText("Scanning...");
            mCamera.setPreviewCallback(previewCb);
            mCamera.startPreview();
            previewing = true;
            mCamera.autoFocus(autoFocusCB);
        }
    }
	
	@Override
	public void onBackPressed() {
		
		releaseCamera();
		super.onBackPressed();
	}
	
	public void onGetCode(String code)
	{
	//   create voice 
		   Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		   if(notification != null)
		   {
			   MediaPlayer mp = MediaPlayer.create( getApplicationContext(), notification);
			   if(mp != null)
				   mp.start();
		   }
		   
		  
		//   create vibrate.
		   Vibrator v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		   if(v!=null)
			   v.vibrate(500);
		//
		   
		QHApp app = (QHApp)getApplication();
 		app.model.detatcQRCode = code;
 		this.finish();
 		
 		releaseCamera();
	}
	
	  private Runnable doAutoFocus = new Runnable() {
          public void run() {
              if (previewing)
                  mCamera.autoFocus(autoFocusCB);
          }
      };
	PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);
            
            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();
                
                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
//                    scanText.setText("barcode result " + sym.getData());
                	
                    barcodeScanned = true;
                    onGetCode(sym.getData());
                   
                }
            }
        }
    };

// Mimic continuous auto-focusing
AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };
	
	/** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }
}
