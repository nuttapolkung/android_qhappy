package net.pirsquare.qhappy;

import net.pirsquare.qhappy.receiver.ResponseReceiver;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;

public class NotConnectInternetActivity extends ActionBarActivity {
	
	QHApp app;
	NotConnectInternetActivity ctx;
	private ResponseReceiver receiver;
	private ProgressDialog ringProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = this;
		app = (QHApp) getApplication();
		app.model.notConnectInternetActivity = this;
		
//		getActionBar().hide();
		
		setContentView(R.layout.activity_not_connect_internet);
		
		Intent intent = getIntent();
		Boolean want_refresh = intent.getBooleanExtra("want_refresh", false);
		final ActionBar actionBar = getSupportActionBar();
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("QHappy");
		
		if(want_refresh == false)
		{
			ImageButton refresh_btn = (ImageButton)findViewById(R.id.refresh_btn);
			ViewGroup layout = (ViewGroup) refresh_btn.getParent();
			if(null!=layout) 
			  layout.removeView(refresh_btn);
		}
		
		receiver = new ResponseReceiver(new ResponseReceiver.ResponseReceiverCallBack() {
			
			@Override
			public void error(Context context, Intent intent) {
				
			}
			
			@Override
			public void done(Context context, Intent intent) {
				ctx.finish();
			}
		});
		
		this.registerReceiver(receiver, new IntentFilter("net.pirsquare.haveNet"));
	}

	public void tappedRefresh(View view)
	{
		ringProgressDialog = ProgressDialog.show(this,"Please Wait..", "Loading...");
		
		final Activity ctx = this;
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		    @Override
		    public void run() {
		    	ringProgressDialog.dismiss();
				
				if(!app.getInternetConnection() )
					
					return;
				
				ctx.finish();
				ringProgressDialog = null;
		    }
		}, 3000);
	}
	
	@Override
	protected void onDestroy() {
		
		if(ringProgressDialog!=null)
		{
			ringProgressDialog.dismiss();
			ringProgressDialog = null;
		}
		try {
			app.model.notConnectInternetActivity = null;
		} catch (Exception e) {
		}
		
		this.unregisterReceiver(receiver);
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
