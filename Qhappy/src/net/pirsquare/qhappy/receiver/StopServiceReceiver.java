package net.pirsquare.qhappy.receiver;

import java.util.Iterator;
import java.util.List;

import com.parse.ParseInstallation;

import net.pirsquare.qhappy.service.IntervalGetNotificationService;
import net.pirsquare.qhappy.service.MyQFetchDataBGService;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class StopServiceReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
//		Recheck
		Log.d("QHappy", "StopService is receive broadcast.");
		if(!isAppRunning(context))
		{
			try {
				
			
			Log.d("QHappy", "Stop background when no queue.");
			forceStopService(context.getPackageName()+":fetch_notification",context);
			forceStopService(context.getPackageName()+":fetch_data",context);
			
			Intent fetchintent = new Intent(context.getApplicationContext(), MyQFetchDataBGService.class);
			fetchintent.setData(Uri.parse(ParseInstallation
					.getCurrentInstallation().getString("deviceToken")));
			PendingIntent fetchintentintent = PendingIntent.getService(context.getApplicationContext(), 0, fetchintent, 0);
			
			Intent notiintent = new Intent(context.getApplicationContext(), IntervalGetNotificationService.class);
			notiintent.setData(Uri.parse(ParseInstallation
					.getCurrentInstallation().getString("deviceToken")));
			PendingIntent notiintentintent = PendingIntent.getService(context.getApplicationContext(), 0, notiintent, 0);
			
			AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			
			try {
				alarm.cancel(fetchintentintent);
			} catch (Exception e) {
				Log.d("QHappy", "Can't stop fetch data in background service.");
			}
			
			try {
				alarm.cancel(notiintentintent);
			} catch (Exception e) {
				Log.d("QHappy", "Can't stop fetch noti in background service.");
			}
			} catch (Exception e) {
			}
		}else{
			Intent stop_intent = new Intent("net.pirsquare.service_order");
			stop_intent.addCategory("net.pirsquare.qhappy");
			stop_intent.putExtra("start", false);
			context.sendBroadcast(stop_intent);
		}
	}
	
	private void forceStopService(String service_name,Context context)
	{
		try {
			
		
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> runningAppProcesses = am.getRunningAppProcesses();

		Iterator<RunningAppProcessInfo> iter = runningAppProcesses.iterator();

		while(iter.hasNext()){
		    RunningAppProcessInfo next = iter.next();

		    Log.d("PROCESS", next.processName);

		    if(next.processName.equals(service_name)){
		    	Log.d("QHappy", "Kill process name >> "+ next.processName);
		        android.os.Process.killProcess(next.pid);
		    }
		}
		} catch (Exception e) {
			Log.d("QHappy", "can't stop background service.");
		}
	}
	
	
	private boolean isAppRunning(Context context)
	{
		Boolean result = false;
		ActivityManager activityManager = (ActivityManager) context.getSystemService( Context.ACTIVITY_SERVICE );
		 List<RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
		 
		 for(int i = 0; i < procInfos.size(); i++){
			  if(procInfos.get(i).processName.equals("net.pirsquare.qhappy"))
		   {
			   result = true;
//		    Log.e("Result", "App is running - Doesn't need to reload "+procInfos.get(i).processName);
		    break;
		   }
		   else
		   {
//		     Log.e("Result", "App is not running - Needs to reload");
		  }
		}
		 
		 return result;
	}

}
