package net.pirsquare.qhappy.receiver;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import net.pirsquare.qhappy.QHApp;
import net.pirsquare.qhappy.R;
import net.pirsquare.qhappy.SplashScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class ResponseReceiver extends BroadcastReceiver {

	public static final String ACTION_FINISH = "net.pirsquare.FINISH";
	public static final String ACTION_CONNECTION_RESPONSE = "net.pirsquare.ACTION_CONNECTION_RESPONSE";
	public static final String ACTION_RESP = "net.pirsquare.FETCHCOMPLETE";
	public static final String ACTION_INTERVAL_NOTIFICATION = "net.pirsquare.intervalnotification";
	public static final String ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND = "net.pirsquare.ACTION_RECEIVE_NOTIFICATION_ONFOREGROUND";
	public static final String ACTION_RECEIVE_NOTIFICATION_ON_BACKGROUND = "net.pirsquare.ACTION_RECEIVE_NOTIFICATION_ON_BACKGROUND";
	public static final String ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND_LOCKSCREEN = "net.pirsquare.ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND_LOCKSCREEN";

	public ResponseReceiver() {
		super();
	}

	public ResponseReceiver(ResponseReceiverCallBack callback) {
		super();

		_callback = callback;
	}

	public static interface ResponseReceiverCallBack {
		public void done(Context context, Intent intent);

		public void error(Context context, Intent intent);
	}

	private boolean isAppRunning(Context context) {
		Boolean result = false;
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> procInfos = activityManager
				.getRunningAppProcesses();

		for (int i = 0; i < procInfos.size(); i++) {
			if (procInfos.get(i).processName.equals("net.pirsquare.qhappy")) {
				result = true;
				// Log.e("Result",
				// "App is running - Doesn't need to reload "+procInfos.get(i).processName);
				break;
			} else {
				// Log.e("Result", "App is not running - Needs to reload");
			}
		}

		return result;
	}

	protected boolean isRunningInForeground(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> tasks = manager
				.getRunningTasks(1);
		if (tasks.isEmpty()) {
			return false;
		}

		String topActivityName = tasks.get(0).topActivity.getPackageName();

		return topActivityName.equalsIgnoreCase(context.getPackageName());
	}

	private Boolean isLock(Context context) {
		KeyguardManager myKM = (KeyguardManager) context
				.getSystemService(Context.KEYGUARD_SERVICE);

		return myKM.inKeyguardRestrictedInputMode();
	}

	private Boolean isScreenOn(Context context) {
		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);

		return pm.isScreenOn();
	}

	private void fireNoInternetNotification(Context context) {
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Log.d("QHappy", "Fire Notification No internet on forground.");
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle("QHappy")
				.setContentText("ไม่สามารถติดต่อเซฟเวอร์ได้์");

		Intent demoIntent = null;

		demoIntent = new Intent(context, SplashScreen.class);

		demoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				demoIntent, 0);

		mBuilder.setContentIntent(contentIntent);
		mBuilder.setAutoCancel(true);
		mNotificationManager.cancel(1020);
		mNotificationManager.notify(1020, mBuilder.build());

		alert(context);
	}

	private ResponseReceiverCallBack _callback;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (_callback != null)
			_callback.done(context, intent);
		else {

			NotificationManager mNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

			Boolean push = !isRunningInForeground(context);
			Boolean new_data = false;
			Boolean focus = false;

			QHApp app = (QHApp) context.getApplicationContext();

			if (intent.getAction().equals(
					ResponseReceiver.ACTION_CONNECTION_RESPONSE)
					&& isAppRunning(context)) {
				Boolean response = intent.getBooleanExtra("connection", false);
				SharedPreferences share = context.getSharedPreferences(
						"timeout", Context.MODE_PRIVATE);
				Editor editor = share.edit();
				int time = share.getInt("time", 0);

				if (response == false) {

					if (isRunningInForeground(context)) {
						int show = share.getInt("show", 0);
						if (isLock(context)) {
							time++;

							Log.d("QHappy", "can't connect internet " + time);
							if (time >= 5) {
								editor.putInt("time", 0);
								editor.commit();

								Log.d("QHappy",
										"Fire Notification No internet on forground.");
								if (isAppRunning(context))
									fireNoInternetNotification(context);
							} else {
								editor.putInt("time", time);
								editor.commit();
							}
						} else {
							editor.putInt("time", 0);
							editor.commit();
							Intent noNet = new Intent("net.pirsquare.noNet");
							context.sendBroadcast(noNet);
						}
					} else {
						if (app.model.homeActivity != null)
							Log.d("QHappy", "Running");

						time++;

						Log.d("QHappy", "can't connect internet " + time);
						if (time >= 6) {
							editor.putInt("time", 0);
							editor.commit();

							Log.d("QHappy",
									"Fire Notification No internet on forground.");
							fireNoInternetNotification(context);
						} else
							editor.putInt("time", time);
						editor.commit();
					}
				} else {
					if (isRunningInForeground(context)) {
						Log.d("QHappy", "Internet is normal in forground.");
						Intent haveNet = new Intent("net.pirsquare.haveNet");
						context.sendBroadcast(haveNet);
					} else {
						Log.d("QHappy", "Internet is normal in background.");
						mNotificationManager.cancel(1020);
					}

					editor.putInt("time", 0);
					editor.putInt("show", 0);
					editor.commit();
				}
				return;
			}

			if (intent.getExtras() != null) {
				String json = intent.getExtras().getString("json");

				Log.d("QHappy", "Response json " + json);
				if (json == null)
					return;

				JSONArray jsons;
				try {
					jsons = new JSONArray(json);
					if (jsons != null)
						try {
							if (jsons.length() != 0)
								new_data = true;
						} catch (Exception e) {
						}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			if (new_data == false)
				return;

			try {
				SharedPreferences share = app.getSharedPreferences("noti_time",
						Context.MODE_PRIVATE);
				long old_time = share.getLong("noti_time", 0);

				String json_string = intent.getExtras().getString("json");
				JSONArray ja;
				try {
					ja = new JSONArray(json_string);
					if (ja.length() > 0) {
						JSONObject jo = ja.getJSONObject(ja.length() - 1);
						long noti_time = jo.getLong("time");

						if (noti_time <= old_time) {
							Log.d("QHappy", "older notification. current time "
									+ old_time + " noti time " + noti_time);
							writeToFile(String.valueOf(jo.get("id")),
									"lastid.txt", context);
							writeToFile(String.valueOf(jo.get("time")),
									"lasttime.txt", context);

							return;
						} else {
							Editor editor = share.edit();
							editor.putLong("noti_time", noti_time);
							editor.commit();
						}
					}
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			} catch (Exception e) {

			}

			Intent newIntent = null;

			if (isRunningInForeground(context))
				if (isLock(context)) {
					newIntent = new Intent(
							ResponseReceiver.ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND_LOCKSCREEN);
					push = true;
					newIntent.putExtra("json",
							intent.getExtras().getString("json"));
					context.sendBroadcast(newIntent);
					Log.d("QHappy", "app is running on foreground lock screen.");
				} else {
					if (isScreenOn(context)) {
						push = false;
						Log.d("QHappy", "app is running on foreground.");
						newIntent = new Intent(
								ResponseReceiver.ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND);
					} else if (!isScreenOn(context)) {
						push = true;
						Log.d("QHappy",
								"app is running on foreground. screen off");
						newIntent = new Intent(
								ResponseReceiver.ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND);
						newIntent.putExtra("json", intent.getExtras()
								.getString("json"));
						context.sendBroadcast(newIntent);
					} else {
						Log.d("QHappy",
								"app is running on foreground unlock screen.");
						newIntent = new Intent(
								ResponseReceiver.ACTION_RECEIVE_NOTIFICATION_ON_FOREGROUND_LOCKSCREEN);
						push = false;

					}
				}
			else {

				push = true;
				newIntent = new Intent(
						ResponseReceiver.ACTION_RECEIVE_NOTIFICATION_ON_BACKGROUND);
				Log.d("QHappy", "app is running on backgroud.");

			}

			if (push == false) {
				newIntent
						.putExtra("json", intent.getExtras().getString("json"));
				context.sendBroadcast(newIntent);
				// Vibrite ring and wakelock.
				alert(context);
			}

			if (push
					&& intent.getAction().equals(
							ResponseReceiver.ACTION_INTERVAL_NOTIFICATION)
					&& new_data) {

				Log.d("QHappy", "push local notification");

				Intent demoIntent = null;

				demoIntent = new Intent(context, SplashScreen.class);

				demoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP);

				String json = intent.getExtras().getString("json");
				demoIntent.putExtra("json", json);

				PendingIntent contentIntent = PendingIntent.getActivity(
						context, 0, demoIntent, 0);

				String msg = "";
				String data = "0";

				try {
					JSONArray noti_list = new JSONArray(json);

					if (noti_list.length() > 0) {
						msg = noti_list.getJSONObject(0).getString("alert");
					}

					data = String.valueOf(noti_list.getJSONObject(0).get("id"));

					// JSONArray.
				} catch (Exception e) {

				}

				Log.d("QHappy", "msg " + msg + "data " + data);

				if (msg.equals(""))
					return;

				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle("QHappy").setContentText(msg);

				mBuilder.setAutoCancel(true);
				mBuilder.setContentIntent(contentIntent);

				mNotificationManager.cancelAll();
				mNotificationManager.notify(1009, mBuilder.build());

				alert(context);

				try {
					OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
							context.openFileOutput("temp_queue.txt",
									Context.MODE_PRIVATE));
					outputStreamWriter.write(json);
					outputStreamWriter.close();
					Log.d("QHappy", "write log ID " + json);
				} catch (IOException e) {
					Log.e("Exception", "File write failed: " + e.toString());
				}

				Log.d("QHappy", "save log ID " + data);

				try {
					OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
							context.openFileOutput("lastid.txt",
									Context.MODE_PRIVATE));
					outputStreamWriter.write(data);
					outputStreamWriter.close();
				} catch (IOException e) {
					Log.e("Exception", "File write failed: " + e.toString());
				}
			}
		}
	}

	public void writeToFile(String data, String filename, Context context) {

		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					context.openFileOutput(filename, Context.MODE_PRIVATE));
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public void alert(Context context) {
		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);

		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		KeyguardManager myKM = (KeyguardManager) context
				.getSystemService(Context.KEYGUARD_SERVICE);

		if (!pm.isScreenOn() || myKM.inKeyguardRestrictedInputMode()) {
			PowerManager.WakeLock wl = pm.newWakeLock(
					PowerManager.SCREEN_BRIGHT_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP, "QHappy");
			wl.acquire();
			wl.release();

		} else {

			// mNotificationManager.cancelAll();
		}

		// create voice
		Uri notification = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		if (notification != null) {
			MediaPlayer mp = MediaPlayer.create(
					context.getApplicationContext(), notification);
			if (mp != null)
				mp.start();
		}

		// create vibrate.
		Vibrator v = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		if (v != null)
			v.vibrate(500);
		//

	}

}
