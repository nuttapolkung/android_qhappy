package net.pirsquare.qhappy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.pirsquare.qhappy.dataAdepter.MyQAdepter;
import net.pirsquare.qhappy.model.CancelQueueData;
import net.pirsquare.qhappy.model.QueueData;
import net.pirsquare.qhappy.receiver.ResponseReceiver;
import net.pirsquare.qhappy.service.QHappyService;
import net.pirsquare.qhappy.service.ServiceCallBack;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;

import com.handmark.pulltorefresh.extras.listfragment.PullToRefreshListFragment;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.TextHttpResponseHandler;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

public class MyQFragment extends PullToRefreshListFragment implements
		ServiceCallBack, OnRefreshListener<ListView> {

	private QHApp app;
	Context context;

	public Boolean isFristLoad;
	public int state = 0;
	private ImageView no_q;
	private JSONObject kios_status;
	private Boolean noq_visible = true;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		app = (QHApp) getActivity().getApplication();
		// app.service.addListener(this);

		Log.d("MyQ", "onCreate");

		// add filter for receive data in babckground.

		receiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {
						if (MyQFragment.this != null)
							MyQFragment.this.refreshData();
					}
				});

	}

	// notify when operation in Service finished
	private ResponseReceiver receiver;

	public void refreshData() {
		QHApp app = (QHApp) getActivity().getApplication();
		mListItems = new ArrayList<QueueData>();
		mListItems.addAll(app.model.cancel_queues);
		for (int i = 0; i < app.model.myq_data.size(); i++) {
			QueueData q = app.model.myq_data.get(i);
			if (app.model.getCancelQueueData(q.queue_object_id) == null)
				mListItems.add(q);
		}

		if (no_q != null)
			if (mListItems.size() > 0)
				noq_visible = false;
			else
				noq_visible = true;

		if (noq_visible)
			no_q.setVisibility(View.VISIBLE);
		else
			no_q.setVisibility(View.INVISIBLE);

		showData();
	}

	@Override
	public void onStart() {
		Log.d("MyQ", "onStart");
		app.service.addListener(this);

		IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_FINISH);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		getActivity().registerReceiver(receiver, filter);

		mPullRefreshListView = this.getPullToRefreshListView();
		mPullRefreshListView.setOnRefreshListener(this);

		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
				Gravity.CENTER);

		params.setMargins(20, 20, 30, 20);
		ViewGroup parent = (ViewGroup) mPullRefreshListView.getParent()
				.getParent();

		no_q = new ImageView(getActivity());
		no_q.setScaleType(ScaleType.CENTER_INSIDE);
		no_q.setImageDrawable(getResources().getDrawable(
				R.drawable.empty_no_queue));

		try {
			parent.addView(no_q, params);
		} catch (Exception e) {

		}

		no_q.bringToFront();

		no_q.setVisibility(View.INVISIBLE);

		state = 1;

		QHApp app = (QHApp) getActivity().getApplication();
		// get localstorage
		SharedPreferences s = app.getSharedPreferences("temp_q",
				Context.MODE_PRIVATE);
		String resultsss = s.getString("temp", "");

		ArrayList<QueueData> test_read = new ArrayList<QueueData>();
		try {
			JSONArray temps_read = new JSONArray(resultsss);
			for (int i = 0; i < temps_read.length(); i++) {
				QueueData q = new QueueData(null);
				q.setJSONObject(temps_read.getJSONObject(i));
				test_read.add(q);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// end
		mListItems = new ArrayList<QueueData>();
		mListItems.addAll(app.model.cancel_queues);
		mListItems.addAll(app.model.claim_queues);
		mListItems.addAll(test_read);

		if (mListItems.size() > 0) {
			showData();
			noq_visible = false;
		} else
			noq_visible = true;

		if (noq_visible)
			no_q.setVisibility(View.VISIBLE);
		else
			no_q.setVisibility(View.INVISIBLE);

		mPullRefreshListView.onRefreshComplete();
		actualListView = mPullRefreshListView.getRefreshableView();

		mAdapter = new MyQAdepter(getActivity(), mListItems, new JSONObject());
		actualListView.setAdapter(mAdapter);

		try {
			this.setListShown(true);
		} catch (Exception e) {
			Log.d("callBackGetMyQData", "Error this.setListShown(true)");
			return;
		}
		//

		super.onStart();
	}

	@Override
	public void onResume() {
		Log.d("MyQ", "onResume");
		super.onResume();
	}

	@Override
	public void onPause() {
		state = 0;
		Log.d("MyQ", "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.d("MyQ", "onStop");

		if (no_q != null) {
			no_q.setVisibility(View.GONE);
			no_q = null;
		}

		app.service.removeListener(this);

		getActivity().unregisterReceiver(receiver);

		if (mPullRefreshListView != null) {
			mPullRefreshListView.onRefreshComplete();
		}
		super.onStop();
	}

	public void loadData() {

		// check internet connection.
		QHApp app = (QHApp) getActivity().getApplication();
		if (!app.getInternetConnection()) {
			Intent term_activity = new Intent(getActivity(),
					NotConnectInternetActivity.class);
			term_activity.putExtra("want_refresh", true);
			startActivity(term_activity);
			return;
		}

		if (no_q != null)
			no_q.setVisibility(View.INVISIBLE);

		this.setListShown(false);

		app.service.getMyQ();
	}

	@Override
	public void onDestroy() {
		no_q = null;
		// app.service.removeListener(this);
		Log.d("MyQ", "onDestroy");
		super.onDestroy();
	}

	// Listener
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		loadData();
	}

	private ArrayList<QueueData> mListItems = new ArrayList<QueueData>();
	private MyQAdepter mAdapter;
	private PullToRefreshListView mPullRefreshListView;
	private ListView actualListView;

	@Override
	public void callBackGetBrandMetaData(List<ParseObject> result) {

	}

	@Override
	public void Error(ParseException e) {

	}

	@Override
	public void callBackGetFavData(List<ParseObject> result) {

	}

	@Override
	public void callBackGetBranchMetaData(List<ParseObject> result) {

	}

	@Override
	public void callBackGetMyQData(List<ParseObject> result) {

		if (mPullRefreshListView == null)
			return;

		mPullRefreshListView.onRefreshComplete();

		if (state == 0)
			return;

		final QHApp app = (QHApp) getActivity().getApplication();

		// save in localstorage
		String bids = "";

		ArrayList<QueueData> alive_queue = new ArrayList<QueueData>();
		for (ParseObject obj : result) {
			QueueData queue_data = new QueueData(obj);
			alive_queue.add(queue_data);

			if (bids == "")
				bids = queue_data.branch_id;
			else
				bids = bids + "," + queue_data.branch_id;
		}

		JSONArray temps = new JSONArray();

		for (int i = 0; i < alive_queue.size(); i++) {
			temps.put(alive_queue.get(i).getJSONObject());
		}

		String resultsss = temps.toString();

		Editor e = app.getSharedPreferences("temp_q", Context.MODE_PRIVATE)
				.edit();
		e.putString("temp", resultsss);
		e.commit();
		// end

		ArrayList<QueueData> myQ_data = new ArrayList<QueueData>();

		app.model.checkCancelQueueData();

		myQ_data.addAll(app.model.claim_queues);
		myQ_data.addAll(app.model.cancel_queues);

		app.model.myq_data = alive_queue;

		for (int i = 0; i < app.model.myq_data.size(); i++) {
			QueueData q = app.model.myq_data.get(i);
			if (app.model.getCancelQueueData(q.queue_object_id) == null)
				myQ_data.add(q);
		}

		if (myQ_data.size() == 0)
			noq_visible = true;
		else
			noq_visible = false;

		mListItems = myQ_data;

		app.mListItemsTemp = myQ_data;

		if (app.mListItemsTemp.size() == 0)
			app.mListItemsTemp = null;

		try {
			SharedPreferences sharedPref = app.model.mainActivity
					.getPreferences(Context.MODE_PRIVATE);

			SharedPreferences.Editor editor = sharedPref.edit();

			if (myQ_data.size() == 0)
				editor.putString("haveQ", "");
			else
				editor.putString("haveQ", "" + myQ_data.size());

			Boolean resultCommit = editor.commit();

			if (resultCommit)
				Log.d("QHappy", "commit complete");
			else
				Log.d("QHappy", "commit failed");

			Log.d("QHappy", "size" + myQ_data.size());
		} catch (Exception error) {

		}

		if (myQ_data.size() == 0)
			this.setListShown(false);
		else
			checkKiosStatus(bids);

	}

	private void checkKiosStatus(String bids) {
		final String f_bids = bids;
		// Check kios status
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("bid", bids);

		if (Config.useParseServer == false)
			QHappyService.request(
					QHappyService.REQUEST_ACTION_CHECK_KIOS_WITH_ID, params,
					new TextHttpResponseHandler() {

						@Override
						public void onSuccess(int arg0, Header[] arg1,
								String arg2) {
							kios_status = null;
							try {
								kios_status = new JSONObject(arg2);
							} catch (JSONException e) {
								e.printStackTrace();
							}

							showData();

							if (noq_visible)
								no_q.setVisibility(View.VISIBLE);
							else
								no_q.setVisibility(View.INVISIBLE);

							Log.d("QHappy", arg2);
						}

						@Override
						public void onFailure(int arg0, Header[] arg1,
								String arg2, Throwable arg3) {
							checkKiosStatus(f_bids);
						}
					});
		else
			ParseCloud.callFunctionInBackground("checkKiosStatusWithBranchIDs",
					params, new FunctionCallback<String>() {

						@Override
						public void done(String arg0, ParseException arg1) {
							if (arg1 == null) {
								kios_status = null;
								try {
									kios_status = new JSONObject(arg0);
								} catch (JSONException e) {
									e.printStackTrace();
								}

								showData();

								if (noq_visible)
									no_q.setVisibility(View.VISIBLE);
								else
									no_q.setVisibility(View.INVISIBLE);

								Log.d("QHappy", arg0);
							} else {
								checkKiosStatus(f_bids);
							}
						};
					});
	}

	private void showData() {
		actualListView = mPullRefreshListView.getRefreshableView();

		mAdapter = new MyQAdepter(getActivity(), mListItems, kios_status);

		actualListView.setAdapter(mAdapter);

		try {
			this.setListShown(true);
		} catch (Exception e) {
			Log.d("callBackGetMyQData", "Error this.setListShown(true)");
			return;
		}

		// Click event for single list row
		actualListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				int pos = position - 1;

				QueueData queueData = null;
				try {
					queueData = mListItems.get(pos);
				} catch (Exception e) {
				}

				if (queueData == null)
					return;

				if (queueData.getClass() == CancelQueueData.class) {
					CancelQueueData cancelQueueData = app.model
							.getCancelQueueData(((CancelQueueData) queueData).queue_object_id);
					if (cancelQueueData != null) {
						app.model.cancel_queues.remove(cancelQueueData);
						app.model.saveFile();

						mListItems.remove(pos);

						mAdapter.notifyDataSetChanged();
						// show noq image landing.
						if (mListItems.size() == 0)
							noq_visible = true;

						if (noq_visible)
							no_q.setVisibility(View.VISIBLE);
						else
							no_q.setVisibility(View.INVISIBLE);
					}

				} else {

					if (mAdapter.kiosIsOffline(pos)) {
						Intent contact_shop = new Intent(getActivity(),
								ContactShopActivity.class);
						getActivity().startActivity(contact_shop);
					} else {

						QHApp app = (QHApp) getActivity().getApplication();
						app.model.currentQueueData = queueData;

						Intent queue_detail = new Intent(getActivity(),
								QueueDetailActivity.class);
						startActivityForResult(queue_detail, 12);
					}
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 12) {
			if (resultCode == 1) {
				this.loadData();
			}
		}
	}

	@Override
	public void callBackGetMyQDataQueue(List<QueueData> result) {
		if (mPullRefreshListView == null)
			return;

		mPullRefreshListView.onRefreshComplete();

		if (state == 0)
			return;

		final QHApp app = (QHApp) getActivity().getApplication();

		// save in localstorage
		String bids = "";

		ArrayList<QueueData> alive_queue = new ArrayList<QueueData>();

		if (result != null)
			for (QueueData queue_data : result) {
				alive_queue.add(queue_data);

				if (bids == "")
					bids = queue_data.branch_id;
				else
					bids = bids + "," + queue_data.branch_id;
			}

		JSONArray temps = new JSONArray();

		for (int i = 0; i < alive_queue.size(); i++) {
			temps.put(alive_queue.get(i).getJSONObject());
		}

		String resultsss = temps.toString();

		Editor e = app.getSharedPreferences("temp_q", Context.MODE_PRIVATE)
				.edit();
		e.putString("temp", resultsss);
		e.commit();
		// end

		ArrayList<QueueData> myQ_data = new ArrayList<QueueData>();

		app.model.checkCancelQueueData();

		myQ_data.addAll(app.model.claim_queues);
		myQ_data.addAll(app.model.cancel_queues);

		app.model.myq_data = alive_queue;

		for (int i = 0; i < app.model.myq_data.size(); i++) {
			QueueData q = app.model.myq_data.get(i);
			if (app.model.getCancelQueueData(q.queue_object_id) == null)
				myQ_data.add(q);
		}

		if (myQ_data.size() == 0)
			noq_visible = true;
		else
			noq_visible = false;

		mListItems = myQ_data;

		app.mListItemsTemp = myQ_data;

		if (app.mListItemsTemp.size() == 0)
			app.mListItemsTemp = null;

		try {
			SharedPreferences sharedPref = app.model.mainActivity
					.getPreferences(Context.MODE_PRIVATE);

			SharedPreferences.Editor editor = sharedPref.edit();

			if (myQ_data.size() == 0)
				editor.putString("haveQ", "");
			else
				editor.putString("haveQ", "" + myQ_data.size());

			Boolean resultCommit = editor.commit();

			if (resultCommit)
				Log.d("QHappy", "commit complete");
			else
				Log.d("QHappy", "commit failed");

			Log.d("QHappy", "size" + myQ_data.size());
		} catch (Exception error) {

		}

		if (!bids.equals(""))
			checkKiosStatus(bids);
		else {
			this.setListShown(true);
			if (noq_visible)
				no_q.setVisibility(View.VISIBLE);
			else
				no_q.setVisibility(View.INVISIBLE);
		}

	}
}
