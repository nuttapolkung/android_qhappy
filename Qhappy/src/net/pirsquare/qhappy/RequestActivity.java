package net.pirsquare.qhappy;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import net.pirsquare.qhappy.service.QHappyService;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class RequestActivity extends ActionBarActivity implements AppListener{

	private Activity ctx ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		ActionBar actionBar = getActionBar();
		actionBar.setTitle("QHappy");
		actionBar.setIcon(R.drawable.ic_tab_scan);
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		
		QHApp app =(QHApp)getApplication();
		 app.current_activity = this;
		 app.addListener(this);
		 
		 ctx = this;
		 
		 app.model.request_activity = this;
		 
		 receiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				removeTimer();
				if(isFinishing()==false)
				try{
					ctx.finish();
				}catch(Exception e)
				{
					Log.d("QHappy","appCallBack in  home activity can't finish.");
				}
			}
		};
		
		filterAccepted = new IntentFilter("net.pirsquare.accepted");
		filterAccepted.addCategory(Intent.CATEGORY_DEFAULT);
		filterRejected = new IntentFilter("net.pirsquare.rejected");
		filterRejected.addCategory(Intent.CATEGORY_DEFAULT);
		
		this.registerReceiver(receiver, filterAccepted);
		this.registerReceiver(receiver, filterRejected);
	}
	
	BroadcastReceiver receiver;
	
	IntentFilter filterAccepted;
	IntentFilter filterRejected;
	
	public void onClickChangeName(View v)
	{
		final QHApp app = (QHApp)getApplication();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("เปลี่ยนชื่อ");

		// Set up the input
		final EditText input = new EditText(this);
		
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setText(app.model.login_name);
		builder.setView(input);
		
		
		// Set up the buttons
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		    	
		    	TextView login_name = (TextView)findViewById(R.id.textView4);
		    	String user_name  = input.getText().toString();
		    	login_name.setText(user_name); 
		    	
		    	
		    	
		    	app.model.login_name = user_name;
		    	
				SharedPreferences sharedPref = app.model.mainActivity
						.getPreferences(Context.MODE_PRIVATE);

				SharedPreferences.Editor editor = sharedPref
						.edit();
				editor.putString("fb_name", user_name);
				editor.commit();
				
				final ProgressDialog  progress =ProgressDialog.show(ctx, "Change name", "Saving");

				ParseUser user = ParseUser.getCurrentUser();
				user.put("title", user_name);
				
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("title",user_name);
				params.put("device_token", ParseInstallation.getCurrentInstallation().getString("deviceToken"));
				
			QHappyService.request("userUpdate", params, new TextHttpResponseHandler() {
				
				@Override
				public void onSuccess(int arg0, Header[] arg1, String arg2) {
					Log.d("QHappy", "update user complete.");
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
					Log.d("QHappy", "update user failed.");
				}
			});


				user.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException arg0) {
						
						if(arg0!=null)
						{
							Log.d("QHappy", "Change name is failed "+arg0.toString());
						}
						progress.dismiss();
					}
				});
		    	
		    	
		    }
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        dialog.cancel();
		    }
		});
		builder.show();
	}

	@Override
	protected void onStart() {
		
		super.onStart();
	}
	
	@Override
	protected void onStop() {
		
		super.onStop();
	}
	
	@Override
	public void appCallBack(JSONObject jsonObj) {
		
		QHApp app = (QHApp)getApplicationContext();
		
		int showAlert = 1;
		String queue_status = "";
		try {
			queue_status = jsonObj.getString("qs");

		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		if (queue_status.equals(QHApp.QUEUE_STATUS_ACCEPT)) {
			
			app.model.isReserve = false;
			
			removeTimer();
			
			if(isFinishing()==false)
			try{
				this.finish();
			}catch(Exception e)
			{
				Log.d("QHappy","appCallBack in  home activity can't finish.");
			}
		} else if (queue_status.equals(QHApp.QUEUE_STATUS_REJECT)) {
			
			app.model.isReserve = false;
			
			removeTimer();
			if(isFinishing()==false)
			try{
				this.finish();
			}catch(Exception e)
			{
				Log.d("QHappy","appCallBack in  home activity can't finish.");
			}
		} 
		
	}
	
	private void startTimer()
	{
		if(timeout == null)
		{
			timeout = new Handler();
			timeout.postDelayed(timoutRunable, Config.request_reserve_timeout *1000);
		}
	}
	
	public void removeTimer()
	{
		if(timeout!=null)
			timeout.removeCallbacks(timoutRunable);
		
			timeout = null;
			
			QHApp app =(QHApp)getApplication();
			if(app!=null)
			{
				 app.removeListener(this);
				
			}
			
			this.finish();
	}
	
	private Handler timeout;
	
	Runnable timoutRunable = new Runnable() {
		
		@Override
		public void run() {
			
			QHApp app = (QHApp)getApplication();
			
			app.model.homeActivity.stopCheckReserve();
			app.model.homeActivity.delaySetCancelReserve();
			
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("queue_object_id", app.model.request_reserve_queue_id);
			params.put("bid", app.model.branch_detact.branch_id());
			params.put("queue_status", "timeout");
			
			if(Config.useParseServer==false){
//			Request to www.qhappy.com
			QHappyService.request(QHappyService.REQUEST_ACTION_RESERV_TIMEOUT,params, new TextHttpResponseHandler() {
				
				@Override
				public void onSuccess(int arg0, Header[] arg1, String arg2) {
					Log.d("QHappy", "reserv timeout done.");
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
					Log.d("QHappy", "reserv timeout failed.");
				}
			});
			}else{
//			TODO change
			ParseCloud.callFunctionInBackground("reserve_timeout", params, new FunctionCallback<String>() {
				
				public void done(String arg0, ParseException arg1) {
					if(arg1== null)
					{
						
					}
				};
			});
			}
			
//			CHANGE Is set null for ignore accept queue notification after time out worked.
//			app.model.request_reserve_queue_id = null;
			
			if(app.current_activity!=null)
				app.current_activity.finish();
			
			callContactShop();
		}
	};

	protected void onDestroy() {
		QHApp app = (QHApp)getApplication();
		app.model.request_activity = null;
		
		this.unregisterReceiver(receiver);
		
		this.removeTimer();
		super.onDestroy();
	};
	
	public void callWaitAcceptQueue() {
		Intent waitAccept = new Intent(this, WaitAcceptQueueActivity.class);
		startActivity(waitAccept);
	}

	public void callContactShop() {
		Intent contactShop = new Intent(this, ContactShopActivity.class);
		startActivity(contactShop);

		this.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_request,
					container, false);

			return rootView;
		}

		public TextView band_name;
		public TextView branch_name;
		public TextView branch_info;
		public TextView login_name;
		public TextView queue_remain_text;
		public int queue_remain = 2;

		@Override
		public void onStart() {

			getActivity().setRequestedOrientation(
					ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

			QHApp app = (QHApp) getActivity().getApplication();

			ParseImageView imageView = (ParseImageView) getActivity()
					.findViewById(R.id.band_image);
			// The placeholder will be used before and during the fetch, to be
			// replaced by the fetched image
			// data.
			imageView.setPlaceholder(getResources().getDrawable(
					R.drawable.logo_qhappy));
			ParseFile image_file = app.model.branch_detact.branch_image_file();
			imageView.setParseFile(image_file);
			imageView.loadInBackground();

//			ParseImageView user_image = (ParseImageView) getActivity()
//					.findViewById(R.id.reserve_profile_image);

			CircularImageView circularImageView = (CircularImageView)getActivity().findViewById(R.id.reserve_profile_image);;
//			circularImageView.setBorderWidth(10);
//			circularImageView.addShadow();
			
			if(app.model.islogin)
				app.model.setProfileImage((ImageView)circularImageView);
			else
				circularImageView.setImageDrawable(getResources().getDrawable(R.drawable.no_profile_man_medium));

			
			band_name = (TextView) getActivity().findViewById(R.id.version_text);
			branch_name = (TextView) getActivity().findViewById(R.id.textView2);
			branch_info = (TextView) getActivity().findViewById(R.id.textView3);
			login_name = (TextView) getActivity().findViewById(R.id.textView4);
			queue_remain_text = (TextView) getActivity().findViewById(
					R.id.textView5);
			
			band_name.setTypeface(app.model.DB_OZONEX_BOLD);
			band_name.setTextSize(36);
			branch_name.setTypeface(app.model.DB_OZONEX);
			branch_name.setTextSize(24);
			
			login_name.setTypeface(app.model.DB_OZONEX_BOLD);
			
			branch_info.setTypeface(app.model.DB_OZONEX);
			branch_info.setTextSize(24);

			branch_name.setText(app.model.branch_detact.branch_name());
			branch_info.setText(app.model.branch_detact.other_info());

			login_name.setText("Guest");
			band_name.setText("");
			queue_remain_text.setText("" + queue_remain);
			
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int densityDpi = (int)(metrics.density * 160f);
			if(densityDpi>240)
				queue_remain_text.setTextSize(110);
			else
				queue_remain_text.setTextSize(75);
			
			if(app.model.islogin)
				login_name.setText(app.model.login_name);

			ParseObject brandObject = app.model.branch_detact.brand();
			if (brandObject.isDataAvailable()) {
				band_name.setText(brandObject.getString("brand_name"));
			} else {
				brandObject.fetchInBackground(new GetCallback<ParseObject>() {
					public void done(ParseObject object, ParseException e) {
						if (e == null) {
							band_name.setText(object.getString("brand_name"));
						} else {
						}
					}
				});
			}

			ImageButton removeRemainButton = (ImageButton) getActivity()
					.findViewById(R.id.imageButton3);
			ImageButton addRemainButton = (ImageButton) getActivity()
					.findViewById(R.id.imageButton4);
			ImageButton reserveButton = (ImageButton) getActivity()
					.findViewById(R.id.imageButton1);

			removeRemainButton
					.setOnClickListener(new ImageButton.OnClickListener() {
						public void onClick(View v) {
							if (queue_remain - 1 > 0) {
								queue_remain--;
								queue_remain_text.setText("" + queue_remain);
							}
						}
					});

			addRemainButton
					.setOnClickListener(new ImageButton.OnClickListener() {
						public void onClick(View v) {
							if (queue_remain < 99) {
								queue_remain++;
								queue_remain_text.setText("" + queue_remain);
							}
						}
					});

			reserveButton.setOnClickListener(new ImageButton.OnClickListener() {
				public void onClick(View v) {
					
					final RequestActivity act = (RequestActivity) getActivity();
			
				
					final QHApp app = (QHApp) getActivity().getApplication();

					final ProgressDialog ringProgressDialog = ProgressDialog
							.show(getActivity(), "Please Wait..", "Loading...");
					HashMap<String, Object> params = new HashMap<String, Object>();
					params.put("queue_status", "reserve");
					params.put("device_token", ParseInstallation.getCurrentInstallation().getString("deviceToken"));
					params.put("branch_id", app.model.branch_detact.branch_id());
					params.put("capacity_reserve", queue_remain);
					params.put("device_type", "android");
					params.put("time_reserve",System.currentTimeMillis());
					
					act.startTimer();
					
					
					if(Config.useParseServer==false){
//					Reserv queue trought www.qhappy.com
					QHappyService.request(QHappyService.REQUEST_ACTION_RESERVE,params, new JsonHttpResponseHandler(){
						@Override
						public void onSuccess(int arg0, Header[] arg1, JSONObject arg2) {
							
							
							try {
								if(arg2.getString("error")!=null)
								{
									Log.d("QHappy", "Reserve error "+arg2.toString());
									act.callContactShop();
									return;
								}
							} catch (JSONException e1) {
								e1.printStackTrace();
							}
							
									if (arg2.equals("Internal server error.")) {
										act.callContactShop();
									} else if (arg2
											.equals("Door host not found.")) {
										act.callContactShop();
									} else {
										QHApp app = (QHApp) getActivity()
												.getApplication();
										String id ="";
										try {
											id = arg2.getString("result");
										} catch (JSONException e) {
											e.printStackTrace();
										}

										app.model.request_reserve_queue_id =id;
										act.callWaitAcceptQueue();
										
										app.model.homeActivity.cancelDelaySetCancelReserve();
//										start background service 
										app.model.isReserve =true;
										Intent stop_intent = new Intent("net.pirsquare.service_order");
										stop_intent.addCategory("net.pirsquare.qhappy");
										stop_intent.putExtra("start", true);
										act.sendBroadcast(stop_intent);
									}
						}
						@Override
						public void onFailure(int statusCode, Header[] headers,
								String responseString, Throwable throwable) {
							act.removeTimer();
							act.callContactShop();
							
							super.onFailure(statusCode, headers, responseString, throwable);
						}
						
						@Override
						public void onFinish() {
							ringProgressDialog.dismiss();
							super.onFinish();
						}
					});
					}else{
					
//					TODO change
					ParseCloud.callFunctionInBackground("reserveQueue", params,
							new FunctionCallback<String>() {
								public void done(String queue_object_id,
										ParseException e) {
									
									if (e == null) {
										
										if(queue_object_id.equals("Internal server error."))
										{
											act.callContactShop();
										}else if(queue_object_id.equals("Door host not found."))
										{
											act.callContactShop();
										}
										else{
										QHApp app = (QHApp) getActivity()
												.getApplication();
										
										app.model.request_reserve_queue_id = queue_object_id;
										act.callWaitAcceptQueue();
										}
									} else {
										
										act.removeTimer();
										act.callContactShop();
									}
									
									ringProgressDialog.dismiss();
								}
							});
					}
				}
			});

			super.onStart();
		}
		
		public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
			ImageView bmImage;

			public DownloadImageTask(ImageView bmImage) {
				this.bmImage = bmImage;
			}

			protected Bitmap doInBackground(String... urls) {
				String urldisplay = urls[0];
				Bitmap mIcon11 = null;
				try {
					InputStream in = new java.net.URL(urldisplay).openStream();
					mIcon11 = BitmapFactory.decodeStream(in);
				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
				return mIcon11;
			}

			protected void onPostExecute(Bitmap result) {
				bmImage.setImageBitmap(result);
			}
		}
	}

	

}
