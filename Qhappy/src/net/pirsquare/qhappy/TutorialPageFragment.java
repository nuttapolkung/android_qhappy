package net.pirsquare.qhappy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.parse.FunctionCallback;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;

public class TutorialPageFragment extends Fragment {

	public Activity tutorial_activity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
//		ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.fragment_tutorial_page, container,false);
		
		background_view = (ImageView) inflater.inflate(R.layout.fragment_tutorial_page, container,false);
		
		if(backgroundDrawable!=null)
			background_view.setImageDrawable(backgroundDrawable);
		
		return background_view;
	}
	
	public AnimationDrawable p2_animation;
	public ImageButton close_btn;
	public ImageView title_image_view;
	public ImageView background_view;
	private Drawable backgroundDrawable;
	public Drawable titledrawable;
	public int animation_resource;
	
	@Override
	public void onStart() {
		
		super.onStart();
	}
	
	public void setBackgroundImageWithDrawable(Drawable drawable) 
	{
		backgroundDrawable = drawable; 
		
	}
}
