package net.pirsquare.qhappy.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.parse.ParseObject;


public class CancelQueueData extends QueueData{

	
	public CancelQueueData(ParseObject obj) {
		super(obj);
	}

	public String branch_id;
	public String queue_object_id;
	public String group_id;
	public String queue_id;
	public int queue_remain;
	public String time_estimate;
	public String queue_estimate;
	public long time_start;
	public int reserved_capacity;
	public String message;
	
	
	
	public void readObject(QueueData obj)
	{
		if(obj!=null)
		{
		
		branch_id = obj.branch_id;
		queue_object_id =obj.queue_object_id;
		group_id = obj.group_id;
		queue_id = obj.queue_id;
		queue_remain =obj.queue_remain;
		time_estimate = obj.time_estimate;
		queue_estimate = obj.queue_estimate;
		time_start = obj.time_start;
		reserved_capacity = obj.reserved_capacity;
		
		}
	}
	
	public JSONObject getJSONObject()
	{
		JSONObject json = new JSONObject();
		
		try {
			json.put("branch_id", branch_id);
			json.put("queue_object_id", queue_object_id);
			json.put("group_id", group_id);
			json.put("queue_id", queue_id);
			json.put("queue_remain", queue_remain);
			json.put("time_estimate", time_estimate);
			json.put("queue_estimate", queue_estimate);
			json.put("time_start", time_start);
			json.put("reserved_capacity", reserved_capacity);
			json.put("message", message);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public void setJSONObject(JSONObject json)
	{
		try {
			branch_id = json.getString("branch_id");
			queue_object_id =json.getString("queue_object_id");
			group_id = json.getString("group_id");
			queue_id = json.getString("queue_id");
			queue_remain =json.getInt("queue_remain");
			message =json.getString("message");
			time_estimate = json.getString("time_estimate");
			queue_estimate =json.getString("queue_estimate");
			
			time_start =json.getInt("time_start");
			reserved_capacity = json.getInt("reserved_capacity");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
}
