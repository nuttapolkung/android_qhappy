package net.pirsquare.qhappy.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseRelation;

public class BranchListData {
	
		private ParseObject _obj;
		private JSONObject _json;
		
		
//		{
//		id: "1",
//		branch_id: "sl1_ct_ldpw",
//		branch_name: "เซ็นทรัลลาดพร้าว",
//		branch_image: "sizzler-logo01.png",
//		brand_id: "3",
//		other_info: "ชั้น 5 ติด B2S",
//		share_msg: "อิ่มอร่อยกับเรา",
//		queue_estimate: "40",
//		time_estimate: "01:20:00",
//		order: "8",
//		create_date: "2014-04-28 04:25:25",
//		update_date: "2014-10-28 14:31:02"
//		}
		
		public String branch_id()
		{
			if(_obj!=null)
			return _obj.getString("branch_id");
			else
				try {
					return _json.getString("brand_id");
				} catch (JSONException e) {
					e.printStackTrace();
					return "";
				}
		}
		
		public String branch_name()
		{
			if(_obj!=null)
			return _obj.getString("branch_name");
			else
				try {
					return _json.getString("branch_name");
				} catch (JSONException e) {
					e.printStackTrace();
					return "";
				}
		}
		
		public ParseFile branch_image_file()
		{
			if(_obj!=null)
			return (ParseFile)_obj.get("branch_image");
			else
				return null;
		}
		
		public ParseFile share_image_file()
		{
			try{
				return (ParseFile)_obj.get("share_image");
			}catch(Error e){
				return null;
			}
			
		}
		
		public String provider_name()
		{
			if(_obj!=null)
				return _obj.getString("provider_name");
			else
				return "";
				
		}
		
		public String estimate_time()
		{
			if(_obj!=null)
			return _obj.getString("estimate_time");
			else
				try {
					return _json.getString("time_estimate");
				} catch (JSONException e) {
					e.printStackTrace();
					return "";
				}
		}
		
		public ParseObject brand()
		{
			if(_obj!=null)
				return (ParseObject)_obj.get("brand");
			else
				return null;
		}
		
		public String other_info()
		{
			if(_obj!=null)
				return _obj.getString("other_info");
			else
				try {
					return _json.getString("share_msg");
				} catch (JSONException e) {
					e.printStackTrace();
					return "";
				}
		}
		
		public Integer queue_remain()
		{
			if(_obj!=null)
			return _obj.getInt("queue_remain");
			else
				try {
					return _json.getInt("queue_estimate");
				} catch (JSONException e) {
					e.printStackTrace();
					return 0;
				}
		}
		
		public String share_msg()
		{
			if(_obj!=null)
			return _obj.getString("share_msg");
			else
				try {
					return _json.getString("share_msg");
				} catch (JSONException e) {
					e.printStackTrace();
					return "";
				}
		}
		
		public ParseRelation<ParseObject> menus()
		{
			return _obj.getRelation("menus");
		}
		

		
		public void setJson(JSONObject json)
		{
			this._json = json;
		}
		
		public BranchListData(ParseObject parseObj)
		{
			this._obj = parseObj;
		}
}


