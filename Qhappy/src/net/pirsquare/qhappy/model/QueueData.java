package net.pirsquare.qhappy.model;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.parse.ParseObject;

public class QueueData {

	private ParseObject _obj;

	public String branch_id;
	public String queue_object_id;
	public String group_id;
	public String queue_id;
	public int queue_remain;
	public String time_estimate;
	public String queue_estimate;
	public long time_start;
	public int reserved_capacity;
	public int counter_number = 0;
	public String current_stage = null;
	public long claim_time;

	public QueueData(ParseObject obj) {
		if (obj != null) {
			_obj = obj;

			branch_id = _obj.getString("branch_id");

			try {
				queue_object_id = _obj.getString("queue_object_id");
			} catch (Exception error) {

			}
			if (queue_object_id == null)
				try {
					queue_object_id = _obj.getObjectId();
				} catch (Exception error) {

				}

			// queue_object_id = _obj.getString("queue_object_id");
			group_id = _obj.getString("group_id");
			queue_id = "" + _obj.getInt("queue_id");
			queue_remain = _obj.getInt("queue_remain");
			time_estimate = _obj.getString("time_estimate");
			queue_estimate = _obj.getString("queue_estimate");
			time_start = _obj.getLong("time_start");
			reserved_capacity = _obj.getInt("reserved_capacity");

			try {
				current_stage = _obj.getString("cs");
				counter_number = _obj.getInt("cn");
			} catch (Exception error) {

			}
			try {
				current_stage = _obj.getString("current_stage");
				counter_number = _obj.getInt("cn");
			} catch (Exception error) {

			}
		}
	}

	public JSONObject getJSONObject() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("claim_time", claim_time);
		} catch (Exception e) {
		}

		try {
			obj.put("branch_id", branch_id);
			obj.put("queue_object_id", queue_object_id);
			obj.put("group_id", group_id);
			obj.put("queue_id", queue_id);
			obj.put("queue_remain", queue_remain);
			obj.put("time_estimate", time_estimate);
			obj.put("queue_estimate", queue_estimate);
			obj.put("time_start", time_start);
			obj.put("reserved_capacity", reserved_capacity);
			try {
				obj.put("current_stage", current_stage);
				obj.put("counter_number", counter_number);
			} catch (Error e) {
			}
			;

		} catch (JSONException e) {
			Log.d("getJSONObject", "DefaultListItem.toString JSONException: "
					+ e.getMessage());
		}

		return obj;
	}

	public void setJSONObject(JSONObject json) {

		try {
			claim_time = json.getLong("claim_time");
		} catch (Exception e) {
		}
		try {

			branch_id = json.getString("branch_id");
			queue_object_id = json.getString("queue_object_id");
			group_id = json.getString("group_id");
			queue_id = json.getString("queue_id");

			queue_remain = json.getInt("queue_remain");

			time_start = json.getLong("time_start");
			reserved_capacity = json.getInt("reserved_capacity");

			queue_estimate = json.getString("queue_estimate");
			time_estimate = json.getString("time_estimate");

		} catch (JSONException e) {
			Log.d("setJSONObject", "DefaultListItem.toString JSONException: "
					+ e.getMessage());
		}

		try {
			counter_number = json.getInt("cn");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			current_stage = json.getString("cs");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			counter_number = json.getInt("counter_number");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			current_stage = json.getString("current_stage");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void setData(HashMap<String, Object> obj) {
		if (obj != null) {

			branch_id = _obj.getString("branch_id");
			queue_object_id = _obj.getString("queue_object_id");
			group_id = _obj.getString("group_id");
			queue_id = "" + _obj.getInt("queue_id");
			queue_remain = _obj.getInt("queue_remain");
			time_estimate = _obj.getString("time_estimate");
			queue_estimate = _obj.getString("queue_estimate");
			time_start = _obj.getLong("time_start");
			reserved_capacity = _obj.getInt("reserved_capacity");

			try {
				current_stage = _obj.getString("current_stage");
				counter_number = _obj.getInt("cn");
			} catch (Exception error) {

			}

			try {
				claim_time = _obj.getLong("claim_time");
			} catch (Exception e) {
				Log.d("QHappy", "can't get claimed time.");
			}
		}
	}
}
