package net.pirsquare.qhappy.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.pirsquare.qhappy.Config;
import net.pirsquare.qhappy.HomeActivity;
import net.pirsquare.qhappy.MainActivity;
import net.pirsquare.qhappy.MenuGalleryActivity;
import net.pirsquare.qhappy.NotConnectInternetActivity;
import net.pirsquare.qhappy.RequestActivity;
import android.R.bool;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.entity.mime.MinimalField;

public class Model {
	public String detatcQRCode = "";
	public BranchListData branch_detact;
	public String request_reserve_queue_id;
	public String cancel_reserve_queue_id;
	
	public List<BranchListData> branch_metadata;
	
	public String FBID;
	
	public Boolean isReserve = false;
	public Boolean islogin = false;
	public String login_name ="";
	public String gender ="";
	public String birthday = "";
	
	public BrandData currentBrandData;
	
	public MainActivity mainActivity;
	public HomeActivity homeActivity;
	public MenuGalleryActivity menuGalleryActivity;
	public NotConnectInternetActivity notConnectInternetActivity;
	
	public QueueData currentQueueData;
	public List<ParseObject>  currentMenusParseObject;
	public ArrayList<BranchListData> fav_data = new ArrayList<BranchListData>();
	public ArrayList<QueueData> myq_data = new ArrayList<QueueData>();
	public List<ParseFile> menu_image_file = new Vector<ParseFile>();
	
	public Typeface DB_OZONEX;
	public Typeface DB_OZONEX_BOLD;
	
	public int unread_promotion = 0;
	public ArrayList<PromotionData> promotion_lists = new ArrayList<PromotionData>();
	
	public ArrayList<CancelQueueData> cancel_queues = new ArrayList<CancelQueueData>();
	public ArrayList<WaitQueueData> wait_queues = new ArrayList<WaitQueueData>();
	public ArrayList<QueueData> claim_queues = new ArrayList<QueueData>();
	
	public String lng ="0.0";
	public String lat ="0.0";
	
	public String notification_temp ="";
	
	public RequestActivity request_activity;
	
	public CancelQueueData getCancelQueueData(String id)
	{
		CancelQueueData return_date = null;
		for (CancelQueueData cancelQueueData : cancel_queues) {
			if(cancelQueueData.queue_object_id!=null)
			if(cancelQueueData.queue_object_id.equals(id))
			{
				return_date = cancelQueueData;
				break;
			}
		}
		
		return return_date;
	}
	
	public void checkCancelQueueData()
	{
		for (CancelQueueData cancelQueueData : cancel_queues) {
			if(cancelQueueData.queue_object_id==null)
			{
				cancel_queues.remove(cancelQueueData);
				checkCancelQueueData();
				break;
			}
		}
	}
	
	public void removeTempDataWithBrachID(String id)
	{
		String queue_object_id ="";
		for (CancelQueueData cancelQueueData : cancel_queues) {
			if(cancelQueueData.branch_id!=null)
			if(cancelQueueData.branch_id.equals(id))
			{
				queue_object_id =cancelQueueData.queue_object_id;
				cancel_queues.remove(cancelQueueData);
				
			}
		}
		
		if(queue_object_id.equals(""))
			queue_object_id = id;
		for (WaitQueueData waitQueueData : wait_queues) {
			if(waitQueueData.queue_object_id!=null)
			if(waitQueueData.queue_object_id.equals(queue_object_id))
			{
				wait_queues.remove(waitQueueData);
			}
		}
		
		saveFile();
	}
	
	public void removeWaitDataWithBrachID(String id)
	{
		for (WaitQueueData waitQueueData : wait_queues) {
			if(waitQueueData.queue_object_id!=null)
			if(waitQueueData.queue_object_id.equals(id))
			{
				wait_queues.remove(waitQueueData);
			}
		}
		
		saveFile();
	}
	
	public WaitQueueData getWaitQueueData(String id)
	{
		WaitQueueData return_date = null;
		for (WaitQueueData waitQueueData : wait_queues) {
			if(waitQueueData.queue_object_id.equals(id))
			{
				return_date = waitQueueData;
				break;
			}
		}
		
		return return_date;
	}
	
	public void loadLocalData() 
	{
		SharedPreferences share =  homeActivity.getSharedPreferences("queue_file_4",Context.MODE_PRIVATE);
		String cancel_string = share.getString("cancel_queue", "");
		String wait_queue_string = share.getString("wait_queue", "");
		String claim_queue_string = share.getString("claim_queue", "");
		
		claim_queues = new ArrayList<QueueData>();
		
		cancel_queues = new ArrayList<CancelQueueData>();
		wait_queues = new ArrayList<WaitQueueData>();
		
		try {
			
			JSONArray json_arr = new JSONArray(claim_queue_string);
			
			for(int i = 0 ; i < json_arr.length(); i++){
				JSONObject claim_json = json_arr.getJSONObject(i);
				QueueData c = new QueueData(null);
				c.setJSONObject(claim_json);
				
				claim_queues.add(c);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			
			JSONArray json_arr = new JSONArray(cancel_string);
			
			for(int i = 0 ; i < json_arr.length(); i++){
				JSONObject cancel_json = json_arr.getJSONObject(i);
				CancelQueueData c = new CancelQueueData(null);
				c.setJSONObject(cancel_json);
				
				cancel_queues.add(c);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			JSONArray json_arr = new JSONArray(wait_queue_string);
			
			for(int i = 0 ; i < json_arr.length(); i++){
				JSONObject wait_json = json_arr.getJSONObject(i);
				WaitQueueData w = new WaitQueueData();
				w.setJSONObject(wait_json);
				
				wait_queues.add(w);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public void checkExpireclaimQueue()
	{
		for (QueueData claim_queue : claim_queues) {
			long claim_diff_time =System.currentTimeMillis() -  claim_queue.claim_time;
			long  minuntes = TimeUnit.MINUTES.convert(claim_diff_time, TimeUnit.MILLISECONDS);
			if(minuntes>Config.claim_time_expired)
			{
				claim_queues.remove(claim_queue);
				saveFile();
				checkExpireclaimQueue();
				break;
			}
		}
	}
	
	public void saveFile()
	{
		JSONArray cancel_json = new JSONArray();
		
		for (CancelQueueData cancel_queue : cancel_queues) {
			cancel_json.put(cancel_queue.getJSONObject());
		}
		
		String cancel_str = cancel_json.toString();
		
		JSONArray wait_json = new JSONArray();
		
		for (WaitQueueData wait_queue : wait_queues) {
			wait_json.put(wait_queue.getJSONObject());
		}
		
		String wait_str = wait_json.toString();
		
		JSONArray claim_json = new JSONArray();
		
		for (QueueData claim_queue : claim_queues) {
			claim_json.put(claim_queue.getJSONObject());
		}
		
		String claim_str = claim_json.toString();

		SharedPreferences share =  homeActivity.getSharedPreferences("queue_file_4",Context.MODE_PRIVATE);
		Editor edit = share.edit();
		edit.putString("cancel_queue", cancel_str);
		edit.putString("wait_queue", wait_str);
		edit.putString("claim_queue", claim_str);
		edit.commit();
	}
	
	public void checkUnreadpromotion()
	{
		unread_promotion = 0;
		for (PromotionData promotion_data : promotion_lists) {
			if(promotion_data.isRead!=null)
			if(promotion_data.isRead==false)
			{
				unread_promotion++;
			}
		}
	}
	
	public void readPromotion() 
	{
		SharedPreferences share =  homeActivity.getSharedPreferences("promotion_file",Context.MODE_PRIVATE);
		String promotion_str = share.getString("promotion_list", "");
		
		promotion_lists = new ArrayList<PromotionData>();
		
		try {
			
			JSONArray json_arr = new JSONArray(promotion_str);
			
			for(int i = 0 ; i < json_arr.length(); i++){
				JSONObject promotion_json = json_arr.getJSONObject(i);
				PromotionData p = new PromotionData();
				p.setJSONObject(promotion_json);
				
				promotion_lists.add(p);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
//		checkUnreadpromotion();
	}
	
	public void writePromotion()
	{
		JSONArray promotion_json = new JSONArray();
		
		for (PromotionData promotion_data : promotion_lists) {
			promotion_json.put(promotion_data.getJSONObject());
		}
		
		String promotion_str = promotion_json.toString();
		
		SharedPreferences share =  homeActivity.getSharedPreferences("promotion_file",Context.MODE_PRIVATE);
		Editor edit = share.edit();
		edit.putString("promotion_list", promotion_str);
		edit.commit();
	}
	
	public void setProfileImage(ImageView image)
	{
		new DownloadImageTask(image)
		.execute("https://graph.facebook.com/" + FBID
				+ "/picture?type=large");
	}
	
	public BranchListData getBranchDataWithID(String id)
	{
		BranchListData data = null;
		if(branch_metadata!=null)
		for (BranchListData branch_data : branch_metadata) {
			if(branch_data.branch_id().equals(id))
			{
				data = branch_data;
				break;
			}
		}
		
		return data;
	}
	
	public QueueData getQueueDataWithID(String id){
		QueueData data = null;
		for (QueueData queue_data : myq_data) {
			if(queue_data.queue_object_id.equals(id))
			{
				data = queue_data;
				break;
			}
		}
		
		return data;
	}
	
	public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setScaleType(ScaleType.CENTER_CROP);
		}
	}
	
}
