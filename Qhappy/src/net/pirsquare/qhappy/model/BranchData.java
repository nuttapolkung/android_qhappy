package net.pirsquare.qhappy.model;

import com.parse.ParseFile;
import com.parse.ParseObject;

public class BranchData {

	private ParseObject _obj;
	
	public String branch_id()
	{
		return _obj.getString("branch_id");
	}
	
	public String branch_name()
	{
		return _obj.getString("branch_name");
	}
	
	public ParseFile branch_image_file()
	{
		return (ParseFile)_obj.get("branch_image_file");
	}
	
	public String provider_name()
	{
		return _obj.getString("provider_name");
	}
	
	public String time_estimate()
	{
		return _obj.getString("time_estimate");
	}
	
	public ParseObject brand()
	{
		return (ParseObject)_obj.get("brand");
	}
	
	public String other_info()
	{
		return _obj.getString("other_info");
	}
	
	public Integer queue_remain()
	{
		return _obj.getInt("queue_remain");
	}
	
	public String share_msg()
	{
		return _obj.getString("share_msg");
	}
	

	public BranchData(ParseObject parseObj)
	{
		this._obj = parseObj;
	}
}

