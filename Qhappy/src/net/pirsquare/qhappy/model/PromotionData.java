package net.pirsquare.qhappy.model;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class PromotionData {
	public String pid;
	public String bid;
	public String msg;
	public Date start_date;
	public Boolean isRead;
	
	public JSONObject getJSONObject()
	{
		JSONObject json = new JSONObject();
		
		try {
			json.put("pid", pid);
			json.put("bid", bid);
			json.put("msg", msg);
//			convert date to String
			SimpleDateFormat  dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");  
			String date_string  = dateformat.format(start_date);
			json.put("start_date", date_string);
			
			json.put("isRead", isRead);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public void setJSONObject(JSONObject json)
	{
		try {
			pid = json.getString("pid");
			bid =json.getString("bid");
			msg = json.getString("msg");
//			convert String to date
			String dateString = json.getString("start_date");
			SimpleDateFormat  dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");  
			
			try {
				start_date =dateformat.parse(dateString);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			isRead =json.getBoolean("isRead");
			if(isRead == null)
				isRead = false;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
}
