package net.pirsquare.qhappy.model;

import com.parse.ParseFile;

public class BrandData {

	private String name;
	private String info;
	private String logo;
	public ParseFile image_file;
	private String obid;
	public Double distance = 0.0;

	public String getBrandName() {
		return name;
	}

	public void setBrandName(String name) {
		this.name = name;
	}

	public String getBrandInfo() {
		return info;
	}

	public void setBrandInfo(String info) {
		this.info = info;
	}

	public String getBrandLogo() {
		return logo;
	}

	public void setBrandLogo(String logo) {
		this.logo = logo;
	}

	public void setOBID(String obid) {
		this.obid = obid;
	}

	public String getOBID() {
		return obid;
	}
}
