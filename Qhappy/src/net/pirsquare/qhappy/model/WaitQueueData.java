package net.pirsquare.qhappy.model;

import org.json.JSONException;
import org.json.JSONObject;

public class WaitQueueData {
	public String queue_object_id;
	public String message;
	public Boolean isTimeOut = false;
	
	public JSONObject getJSONObject()
	{
		JSONObject json = new JSONObject();
		
		try {
			json.put("queue_object_id", queue_object_id);
			json.put("message", message);
			json.put("isTimeOut", isTimeOut);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public void setJSONObject(JSONObject json)
	{
		try {
			queue_object_id = json.getString("queue_object_id");
			message =json.getString("message");
			isTimeOut = json.getBoolean("isTimeOut");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
}
