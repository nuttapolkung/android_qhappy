package net.pirsquare.qhappy;

import java.util.HashMap;
import java.util.List;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;

import net.pirsquare.qhappy.model.PromotionData;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.Build;

public class PromotionDetail extends Activity {

	private QHApp app;
	PromotionData promotion_data;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_promotion_detail);

		final ActionBar actionBar = getActionBar();
//		actionBar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
		actionBar.setIcon(R.drawable.ic_tab_scan);
		actionBar.setTitle("Promotion");
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		app  = (QHApp)getApplication();
		
		int index = this.getIntent().getIntExtra("index", -1);
		if(index!=-1 &&app.model.promotion_lists.size()>0 )
		{
			
			promotion_data =  app.model.promotion_lists.get(index-1);
			if(promotion_data.isRead == false)
			{
				promotion_data.isRead = true;
				app.model.writePromotion();
				app.model.checkUnreadpromotion();
			}
			
		}
	}
	
	private void loadData()
	{
		if(promotion_data!=null)
		{
			HashMap<String, Object>params = new HashMap<String, Object>();
			params.put("pid", promotion_data.pid);
			
			final ProgressBar preload = (ProgressBar)findViewById(R.id.progressBar1);
			preload.setIndeterminate(true);
			preload.setVisibility(View.VISIBLE);
			
			ParseCloud.callFunctionInBackground("get_promotion_detail", params, new FunctionCallback<List<ParseObject>>() {
				public void done(List<ParseObject> arg0, com.parse.ParseException arg1) {
					if(arg1 == null)
					{
						if(arg0.size()>0)
						{
							preload.setVisibility(View.INVISIBLE);
							
							ParseObject promotion_detail = arg0.get(0);
							
							TextView dsc_text = (TextView)findViewById(R.id.textView1);
							dsc_text.setText((String)promotion_detail.get("detail"));
							
							ParseImageView imageView = (ParseImageView)findViewById(R.id.imageView1);
							imageView.setParseFile((ParseFile)promotion_detail.getParseFile("picture"));
							imageView.loadInBackground();
						}
					}
				};
				
			});
		}
	}
	
	@Override
	protected void onStart() {
		this.loadData();
		super.onStart();
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_promotion_detail, container, false);
			return rootView;
		}
	}

}
