package net.pirsquare.qhappy;


import java.util.ArrayList;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.TitlePageIndicator;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

public class TutorialActivity extends FragmentActivity {

	public static final int NUM_PAGES=5;
	
	private ViewPager mPager;
	private TutorialSlidePageAdapter mPagerAdepter;
	private CirclePageIndicator titlePageIndicator;
	
	public Activity ctx;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		requestWindowFeature(Window.FEATURE_NO_TITLE); 
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		ctx  = this;
		
		setContentView(R.layout.activity_tutorial);
		
//	getActionBar().setBackgroundDrawable(new ColorDrawable(0xffff0000));
	getActionBar().setIcon(R.drawable.ic_tab_scan);
	getActionBar().setTitle("QHappy");
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		mPager = (ViewPager)findViewById(R.id.pager);
		mPagerAdepter = new TutorialSlidePageAdapter(getSupportFragmentManager());
		
		mPager.setAdapter(mPagerAdepter);
		mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				TutorialPageFragment page  = (TutorialPageFragment)mPagerAdepter.getItem(arg0);
				
			}
		});
		
		titlePageIndicator = (CirclePageIndicator)findViewById(R.id.titles);
		titlePageIndicator.setViewPager(mPager);
		titlePageIndicator.setSnap(true);
		titlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				TutorialPageFragment page  = (TutorialPageFragment)mPagerAdepter.getItem(arg0);
				
				
			}
		});
		
		ImageButton close_button = (ImageButton)findViewById(R.id.imageButton1);
		close_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ctx.finish();
			}
		});
		
	}
	
	private class TutorialSlidePageAdapter extends FragmentStatePagerAdapter
	{
		
		private ArrayList<TutorialPageFragment> pages =  new ArrayList<TutorialPageFragment>();
		public TutorialSlidePageAdapter(FragmentManager fm) {
			super(fm);
			
			for (int i = 0; i < 5; i++) {
				TutorialPageFragment page   = new TutorialPageFragment();
				page.tutorial_activity = ctx;
				if(i == 0)
					page.setBackgroundImageWithDrawable(getResources().getDrawable(R.drawable.tu_p1));
				
				if(i ==1)
				{
					page.setBackgroundImageWithDrawable(getResources().getDrawable(R.drawable.tu_p2));
				}else if(i ==2)
				{
					page.setBackgroundImageWithDrawable(getResources().getDrawable(R.drawable.tu_p3));
				}else if(i ==3)
				{
					page.setBackgroundImageWithDrawable(getResources().getDrawable(R.drawable.tu_p4));
				}else if(i ==4)
				{
					page.setBackgroundImageWithDrawable(getResources().getDrawable(R.drawable.tu_p5));
				}
				
				pages.add(page);
			}
		}
		

		@Override
		public Fragment getItem(int arg0) {
			
			return pages.get(arg0);
		}

		@Override
		public int getCount() {
			
			return NUM_PAGES;
		}
		
	}
}
