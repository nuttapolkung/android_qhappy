package net.pirsquare.qhappy;

import org.json.JSONException;
import org.json.JSONObject;

import com.parse.ParseException;
import com.parse.ParsePush;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class Receiver extends BroadcastReceiver {

	private static final String TAG = "Receiver";

	  @Override
	  public void onReceive(Context context, Intent intent) {
		  
		 String json_string = "";
		  
	      String action = intent.getAction();
	      String channel = intent.getExtras().getString("com.parse.Channel");
	      
	      json_string = intent.getExtras().getString("com.parse.Data");
	      
	     JSONObject json = null;
		try {
			json = new JSONObject(json_string);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	      String ms = "";
		try {
			ms = json.getString("alert");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	    Log.d(TAG, "got action " + action + " on channel " + channel + " with:");
	    
	    QHApp app = (QHApp)context.getApplicationContext();
	    
	    // Send the name of the connected device back to the UI Activity
        Message msg = app.mHandler.obtainMessage(0);
        Bundle bundle = new Bundle();
        bundle.putString("json", json_string);
        msg.setData(bundle);
        
        app.mHandler.sendMessage(msg);
        
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        
        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

       if(!pm.isScreenOn() || myKM.inKeyguardRestrictedInputMode())
       {
    	   PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "QHappy");
    	   wl.acquire();
    	   wl.release();
       }else{
    	   
//    	   mNotificationManager.cancelAll();
       }
 	  }

}
