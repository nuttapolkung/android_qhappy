package net.pirsquare.qhappy;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.parse.Parse;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class DeveloperModeActivity extends ListActivity{

	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		loadData();
		
	}
	
	private ArrayList<JSONObject> jsons = new ArrayList<JSONObject>();
	
	private ProgressDialog proDialog;
	
	private void loadData()
	{
//		create proload.
		 proDialog = new ProgressDialog(this);
		 proDialog.setTitle("QHappy");
	     proDialog.setMessage("Loding...");
	     proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	     proDialog.setCancelable(true);
	     proDialog.show();
	    
	     getdata();
	}
	
	private void getdata() {
	    try {
	        StrictMode.ThreadPolicy policy = new StrictMode.
	          ThreadPolicy.Builder().permitAll().build();
	        StrictMode.setThreadPolicy(policy); 
	        URL url = new URL("http://www.pirsquare.net/client/qhappy/server_data.html");
	        HttpURLConnection con = (HttpURLConnection) url
	          .openConnection();
	      String result =   readStream(con.getInputStream());
	      
	     JSONObject jsonObj =  new JSONObject(result);
	     JSONArray jsonArr = jsonObj.getJSONArray("data");
	     
	    int lenght =  jsonArr.length();
	    
	    
	    
	    ArrayList<String> server_names = new ArrayList<String>();
	    
	    jsons = new ArrayList<JSONObject>();
	    
	    for (int i = 0; i < lenght; i++) {
			JSONObject json = jsonArr.getJSONObject(i);
			jsons.add(json);
			
			server_names.add((String)json.get("server_name"));
		}
	    
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		        android.R.layout.simple_list_item_1,server_names);
	    
		setListAdapter(adapter);
	    } catch (Exception e) {
	    	
	        e.printStackTrace();
	    }
	    
	    proDialog.dismiss();
	     proDialog = null;
	}     

	private String readStream(InputStream in) {
	  BufferedReader reader = null;
	  String result ="";
	  try {
	    reader = new BufferedReader(new InputStreamReader(in));
	    String line = "";
	    while ((line = reader.readLine()) != null) {
	      System.out.println(line);
	      result +=line;
	    }
	  } catch (IOException e) {
	    e.printStackTrace();
	  } finally {
	    if (reader != null) {
	      try {
	        reader.close();
	      } catch (IOException e) {
	        e.printStackTrace();
	      }
	    }
	  }
	  
	  return result;
	} 
			  @Override
			  protected void onListItemClick(ListView l, View v, int position, long id) {
			    String item = (String) getListAdapter().getItem(position);
			    JSONObject json  = jsons.get(position);
			    
			    QHApp app = (QHApp) getApplication();
			    
			   String appID = "";
			   String client ="";
			try {
				appID = json.getString("app_id");
				client  = json.getString("client");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			 
			    app.reInitialzeParse(appID,client);
			    
			    Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();
			    this.finish();
			    
			    
			  }


}
