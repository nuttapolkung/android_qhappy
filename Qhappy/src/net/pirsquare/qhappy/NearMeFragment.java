package net.pirsquare.qhappy;

import java.util.LinkedList;
import java.util.List;

import net.pirsquare.qhappy.dataAdepter.BrandAdapter;
import net.pirsquare.qhappy.model.BrandData;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.handmark.pulltorefresh.extras.listfragment.PullToRefreshListFragment;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnPullEventListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.State;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class NearMeFragment extends PullToRefreshListFragment implements
		OnRefreshListener<ListView>, OnPullEventListener<ListView>,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener,
		com.google.android.gms.location.LocationListener {

	private QHApp app;
	Context context;
	private int state = 0;
	private Activity act;

	private Button near_btn;
	private TextView not_found_provider_textView;

	private LinkedList<BrandData> mListItems;
	private BrandAdapter mAdapter;
	private PullToRefreshListView mPullRefreshListView;
	private ListView actualListView;

	@Override
	protected PullToRefreshListView onCreatePullToRefreshListView(
			LayoutInflater inflater, Bundle savedInstanceState) {
		PullToRefreshListView view = new PullToRefreshListView(getActivity());
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		act = getActivity();
		app = (QHApp) getActivity().getApplication();

		buildGoogleApiClient();

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {

		if (!mGoogleApiClient.isConnected())
			mGoogleApiClient.connect();

		if (mLocationRequest != null)
			startLocationUpdates();

		mPullRefreshListView = this.getPullToRefreshListView();
		mPullRefreshListView.setOnRefreshListener(this);

		this.setListShown(true);

		super.onStart();
	}

	@Override
	public void onStop() {

		if (mLocationRequest != null)
			stopLocationUpdates();

		mLocationRequest = null;

		if (mPullRefreshListView != null)
			mPullRefreshListView.setOnPullEventListener(null);

		if (mGoogleApiClient.isConnected())
			mGoogleApiClient.disconnect();

		super.onStop();
	}

	private GoogleApiClient mGoogleApiClient;

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(act)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	private void refreshListView() {

		mPullRefreshListView.onRefreshComplete();
		actualListView = mPullRefreshListView.getRefreshableView();
		mAdapter = new BrandAdapter(getActivity(), app, mListItems);

		actualListView.setAdapter(mAdapter);
		this.setListShown(true);

		actualListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				BrandData itemData = mListItems.get(position - 1);

				Intent reserve_intent = new Intent(
						StandbyFragment.QUICK_RESERVE_ACTION);
				reserve_intent.addCategory("net.pirsquare.qhappy");
				reserve_intent.putExtra("branchID", itemData.getOBID());

				getActivity().sendBroadcast(reserve_intent);

			}
		});
	}

	private void findNearShop() {
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery("t_branch_metadata");

		final ParseGeoPoint userLocation = new ParseGeoPoint(
				Double.parseDouble(app.model.lat),
				Double.parseDouble(app.model.lng));
		query.whereWithinKilometers("location", userLocation,
				Config.near_me_distance_meter);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> result, ParseException e) {

				// Create result intent.
				Intent result_intent = new Intent(
						StandbyFragment.SEARCH_NEAR_ME_FINISH_ACTION);
				result_intent.addCategory("net.pirsquare.qhappy");
				if (e == null) {
					if (!result.isEmpty()) {
						mListItems = new LinkedList<BrandData>();

						for (ParseObject parseObject : result) {

							// Locate images in brand_image column
							ParseFile image = (ParseFile) parseObject
									.get("branch_image");

							BrandData brand = new BrandData();
							brand.setBrandName((String) parseObject
									.getString("branch_name"));
							brand.setBrandInfo((String) parseObject
									.getString("other_info"));
							brand.setBrandLogo(image.getUrl());
							brand.setOBID((String) parseObject
									.getString("branch_id"));
							brand.image_file = image;
							mListItems.push(brand);

							brand.distance = parseObject.getParseGeoPoint(
									"location").distanceInKilometersTo(
									userLocation);

						}

						// refresh view
						refreshListView();

						result_intent.putExtra(
								StandbyFragment.SEARCH_RESULT_PARAMETER, true);
					} else {
						result_intent.putExtra(
								StandbyFragment.SEARCH_RESULT_PARAMETER, false);
					}
				} else {
					result_intent.putExtra(
							StandbyFragment.SEARCH_RESULT_PARAMETER, false);
				}

				getActivity().sendBroadcast(result_intent);
			}

		});
	}

	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(
				mGoogleApiClient, this);
	}

	protected void startLocationUpdates() {
		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);
	}

	private LocationRequest mLocationRequest;

	@Override
	public void onConnected(Bundle connectionHint) {

		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(10000);
		mLocationRequest.setFastestInterval(5000);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		Location mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null) {
			app.model.lat = (String.valueOf(mLastLocation.getLatitude()));
			app.model.lng = (String.valueOf(mLastLocation.getLongitude()));

			findNearShop();
		}
	}

	@Override
	public void onConnectionSuspended(int cause) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {

	}

	@Override
	public void onLocationChanged(Location location) {
		app.model.lat = (String.valueOf(location.getLatitude()));
		app.model.lng = (String.valueOf(location.getLongitude()));

		findNearShop();

	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		this.setListShown(false);
		findNearShop();

	}

	@Override
	public void onPullEvent(PullToRefreshBase<ListView> refreshView,
			State state, Mode direction) {

	}
}
