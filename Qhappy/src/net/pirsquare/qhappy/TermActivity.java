package net.pirsquare.qhappy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;

public class TermActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().hide();
		
		setContentView(R.layout.activity_term);

		WebView myWebView = (WebView) findViewById(R.id.webView1);
		myWebView.loadUrl("http://www.pirsquare.net/project/qhappy/privacy/android/terms.html");
		
		Intent intent = getIntent();
		Boolean want_accept = intent.getBooleanExtra("want_accept", false);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		 QHApp app =(QHApp)getApplication();
		 app.current_activity = this;
		
		if(want_accept == false)
		{
			ImageButton accept_btn = (ImageButton)findViewById(R.id.accept_btn);
			ViewGroup layout = (ViewGroup) accept_btn.getParent();
			if(null!=layout) 
			  layout.removeView(accept_btn);
		}
	}
	

	public void tappedAccept(View view)
	{
		this.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
