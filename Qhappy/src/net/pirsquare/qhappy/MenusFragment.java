package net.pirsquare.qhappy;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import net.pirsquare.qhappy.model.BranchListData;
import net.pirsquare.qhappy.util.ImageLoader;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.TableLayout.LayoutParams;

public final class MenusFragment extends Fragment {
	
	int imageResourceId;
	float alpha = 0.5f;
	ImageLoader imageLoader;
	 QHApp app;
	private List<ParseFile> menus_file = new Vector<ParseFile>();
	
	public MenusFragment setCurrentItem(int i){
		imageResourceId = i;
		return this;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (QHApp)getActivity().getApplication();
        menus_file = app.model.menu_image_file;
        imageLoader = new ImageLoader(getActivity());
    }
    
    

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_menu_image, container, false);
    	
    	final ProgressBar circle_progress = (ProgressBar)rootView.findViewById(R.id.progressBar);
    	circle_progress.animate();
    	
    	final ParseImageView image = (ParseImageView) rootView.findViewById(R.id.menu_image);
    	image.setVisibility(View.INVISIBLE);
    	 
    	image.setPlaceholder(getResources().getDrawable(R.drawable.logo_qhappy));
    	
		ParseFile image_file = menus_file.get(imageResourceId);
//		String brand_image_uri = image_file.getUrl();
		
		image.setParseFile(image_file);
		image.loadInBackground(new GetDataCallback() {
			
			@Override
			public void done(byte[] arg0, ParseException arg1) {
				image.setVisibility(View.VISIBLE);
				circle_progress.clearAnimation();
				circle_progress.setVisibility(View.INVISIBLE);
			}
		});
        return rootView;
    }
}
