package net.pirsquare.qhappy;

import net.pirsquare.qhappy.receiver.ResponseReceiver;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class StandbyFragment extends Fragment {

	// CONST GLOBAL
	public static final String QUICK_RESERVE_ACTION = "quick_reserve_action";
	public static final String SEARCH_NEAR_ME_FINISH_ACTION = "search_near_me_finish_action";
	public static final String SEARCH_RESULT_PARAMETER = "search_result_parameter";

	// Private
	private QHApp app;
	Context context;
	private int state = 0;
	private Activity act;

	private TextView not_found_provider_textView;
	private ImageButton scan_qr_imageButton;
	private ImageButton touch_to_scan_imageButton;
	private NearMeFragment near_me_fragment;

	private IntentFilter quickReserveIntentFilter;
	private ResponseReceiver quickReserveReceiver;
	private LinearLayout preload_layout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_standby, container,
				false);

		not_found_provider_textView = (TextView) rootView
				.findViewById(R.id.textView1);
		not_found_provider_textView.setVisibility(View.INVISIBLE);

		scan_qr_imageButton = (ImageButton) rootView
				.findViewById(R.id.imageButton1);
		touch_to_scan_imageButton = (ImageButton) rootView
				.findViewById(R.id.imageButton2);
		touch_to_scan_imageButton.setVisibility(View.INVISIBLE);

		preload_layout = (LinearLayout) rootView
				.findViewById(R.id.preload_layout);

		// act = getActivity();
		// app = (QHApp)getActivity().getApplication();

		// create receive broadcast
		quickReserveReceiver = new ResponseReceiver(
				new ResponseReceiver.ResponseReceiverCallBack() {

					@Override
					public void error(Context context, Intent intent) {

					}

					@Override
					public void done(Context context, Intent intent) {
						Boolean result = intent.getBooleanExtra(
								SEARCH_RESULT_PARAMETER, false);

						setViewWithNearMeResult(result);
					}
				});

		// Create filter.
		quickReserveIntentFilter = new IntentFilter(
				StandbyFragment.SEARCH_NEAR_ME_FINISH_ACTION);
		quickReserveIntentFilter.addCategory("net.pirsquare.qhappy");

		return rootView;
	}

	private void setViewWithNearMeResult(Boolean value) {

		// value is true mean it have near shop in range.
		if (value) {
			// scan_qr_imageButton.setVisibility(View.INVISIBLE);
			// not_found_provider_textView.setVisibility(View.INVISIBLE);

			if (touch_to_scan_imageButton.getVisibility() == View.INVISIBLE) {
				touch_to_scan_imageButton.setVisibility(View.VISIBLE);

				YoYo.with(Techniques.FadeOutUp).duration(700)
						.playOn(not_found_provider_textView);
				YoYo.with(Techniques.ZoomIn).duration(700).delay(0)
						.playOn(touch_to_scan_imageButton);

				YoYo.with(Techniques.FadeOutDown).duration(400).delay(0)
						.playOn(scan_qr_imageButton);
			}

		} else {
			scan_qr_imageButton.setVisibility(View.VISIBLE);
			not_found_provider_textView.setVisibility(View.VISIBLE);
			touch_to_scan_imageButton.setVisibility(View.INVISIBLE);

		}

		preload_layout.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onStart() {

		// register receiver.
		getActivity().registerReceiver(quickReserveReceiver,
				quickReserveIntentFilter);
		super.onStart();
	}

	@Override
	public void onStop() {

		// unregister receiver.
		getActivity().unregisterReceiver(quickReserveReceiver);

		super.onStop();
	}

}
